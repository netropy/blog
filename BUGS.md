
ISSUE: Gitlab Hugo template fails CI/CD build with "latest" docker version

various version issues reported:
  https://gitlab.com/pages/hugo/-/issues/69
  https://gitlab.com/pages/hugo/-/issues
  https://github.com/gohugoio/hugo/issues

```
$ hugo serve
  hugo v0.92.0-B3549403 linux/amd64 BuildDate=2022-01-12T08:23:18Z VendorInfo=gohugoio
  ERROR 2022/08/04 19:01:40 Page.URL is deprecated and will be removed in Hugo 0.93.0. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
  ERROR 2022/08/04 19:01:40 Page.Hugo is deprecated and will be removed in Hugo 0.93.0. Use the global hugo function.

$ hugo serve
hugo v0.91.0-D1DC0E9A linux/amd64 BuildDate=2021-12-17T09:50:20Z VendorInfo=gohugoio
  WARN 2022/08/04 19:04:00 Page.URL is deprecated and will be removed in a future release. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
  WARN 2022/08/04 19:04:00 Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.
```

workaround: fixate hugo version in .gitlab-ci.yml to:
```
#image: registry.gitlab.com/pages/hugo:latest
image: registry.gitlab.com/pages/hugo:0.91.0
#image: registry.gitlab.com/pages/hugo:0.55.6
```
