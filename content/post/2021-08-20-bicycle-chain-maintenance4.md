---
title: About bicycle chains (4)
subtitle: How to size a new chain and check for correct tension
date: 2021-08-20
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain", "bicycle tools"]
---

The prior post [(1)][0a] recapped how a chain is split with a [chain
tool][10i].  What can be a bit challenging is to determine _where_ to break
the chain, how long it must be for a given bike.

Interestingly, chainsizing can be done by equation [(PT2)][50e].  The correct
length of the chain is a function of 1) the [frame][10o], namely the length of
the [chainstay][10g] and the shape of the rear [forkend][10b], and of 2) the
features of the [drivetrain][10j]: the sizes of the front and rear
[sprockets][10n] (i.e., [chainrings][10e] and [cogs][10f]), and the presence
of a [derailleur][10k] or a [chain tensioner][10d].  Practically, though, it's
much easier to simply take measure of a chain on the bike.

_The challenge:_ One must arrive at an _even_ number of chain links (\*),
since inner plates can only join outer plates.  So, normally, chain lengths
are whole inch multiples (**).  One must round up to the nearest even number
of links and then have a mechanism to take up the chain slack. (\* actually,
_half-links_; ** there are exceptions.)

Still, sizing a chain is perfectly [DIY][20]:
1. One might get lucky and the chain length with just enough slack comes out
   even. :smile:
1. A derailleur, chain tensioner, or a horizontal dropout take up any slack --
   easy! :smile::smile:
1. Sizing is one-time only, we then cut new chains to the length of an old
   one. :smile::smile::smile:

This post shows how to check for the correct chain size or chain slack, and
how to size a new chain.  We show photos how this works out for an
internal-gear hub drivetrain
... <!--more-->

Topics: \
[How to check the chain size on a derailleur drivetrain?](#how-to-check-the-chain-size-on-a-derailleur-drivetrain) \
[How to adjust the chain slack on a non-derailleur drivetrain?](#how-to-adjust-the-chain-slack-on-a-non-derailleur-drivetrain) \
[How to size a new chain to an original chain?](#how-to-size-a-new-chain-to-an-original-chain) \
[How to size a new chain for a derailleur drivetrain?](#how-to-size-a-new-chain-for-a-derailleur-drivetrain) \
[How to size a new chain for a non-derailleur drivetrain?](#how-to-size-a-new-chain-for-a-non-derailleur-drivetrain) \
[What if the non-derailleur bike has vertical dropouts? ](#what-if-the-non-derailleur-bike-has-vertical-dropouts) \
[What if the nearest even number of links isn't a good fit?](#what-if-the-nearest-even-number-of-links-isn't-a-good-fit) \
[Resources](#resource)

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### How to check the chain size on a derailleur drivetrain?

__The test__, [(PT2)][50a]: Inspect the chain when the [derailleur's][10k] in
its most forward and back position.  Shift to the...
- _largests_ front and rear sprockets: The chain should have _two slight
  bends_, one at each [pulley][10c].
- _smallest_ front and rear sprockets: The chain must not _slack_ or _contact
  itself_ at the derailleur.

For details, see this excellent video [(PT1)][51b].

### How to adjust the chain slack on a non-derailleur drivetrain?

For [internal-gear][10a] or a [single-speed][10l] hub drivetrains, the _chain
slack_ is typically adjusted by moving the rear axle back and forth in the
[forkend][10b] or [horizontal dropout][10m].

__The goal__, [(SB2)][12d], [(PT3)][53b]: The chain should have just enough
slack to run freely and _not bind_ the rear wheel -- but not too much slack to
skip teeth or _jump off_ the chainring.

__Test #1__, [(PT3)][53b], [(PT4)][52b]: Pull the chain up and push it down
between chainring and sprocket  -- the chain should move about _~0.5 inch_ or
_~1.25 cm_ around the centerline.

__Test #2__, [(PT3)][53b], [(PT4)][52b]: While pedalling forward, apply a
left/right side load against the chain between the front and rear sprockets --
it should not derail.

Here's a photo of the test #1 on my [IGH touring bike][upgrade1]: Still good
enough...

![Chain Tension][chain-tension02]

...same when pushing downward (quiet hard)...

![Chain Tension][chain-tension03]

Test #2 works out as well: When disturbing the chain sidewise, it does not
derail :smile:

### How to size a new chain to an original chain?

Easy, but to avoid simple mistakes and cutting a new chain at the wrong
location, see this nice video chapter or article by
[(PT1)][51c], [(PT2)][50b]: Line up the chains _correctly_, notice the
chain elongation, mark the rivet, cut the chain with a [chain breaker][10i].

### How to size a new chain for a derailleur drivetrain?

Two methods.  The 1st is quicker but relies a bit on eyeballing; the 2nd is
always precise.

__Method #1:__ Smallest cog to smallest chainring

Nice video segment by [(OZC)][30a] (just 60s, watch closely):
- Route the chain through the rear derailleur,
- put it on the _smallest_ cog and _smallest_ chainring,
- attach 1 plate of the master link (if using any) to a chain end,
- gradually pull the chain ends and overlap until
  - an outer and inner plates link meet _and_
  - the derailleur has come forward a bit (chain not contacting itself);
- cut at the overlapping rivet.

__Method #2:__ Largest cog to largest chainring

Detailed video and articles by [(PT1)][51d], [(PT2)][50c], [(SB1)][11a]:
- Wrap the chain around the _largest_ cog and _largest_ chainring,
- _bypass_ the rear derailleur (or a chain tensioner),
- if using a master link, attach 1 plate of it to a chain end,
- mesh the chain ends on the chainring as tightly as possible
  - such that an outer and inner plates link meet, then
  - mark the overlapping rivet;
- add 2 more links (technically, 2 half-links or 1 inch), and cut there.

Note some exceptions in [(PT1)][51e], [(PT2)][50d] (when to add 4 links, bikes
with rear suspension).

### How to size a new chain for a non-derailleur drivetrain?

[Non-derailleur][12] systems, such as internal-gear or single-speed hubs,
typically come with a [horizontal dropout][10m] or a [chain tensioner][10d] as
a mechanism to regulate chain slack.

In this case: Fixate the axle all the way forward in the horizontal dropout.
Then follow method #2 (largest cog to largest chainring) like for a derailleur
system.  The extra 2 links (=1 inch) provide the slack for the dropout slot or
the chain tensioner, see video or article [(PT3)][53b], [(PT4)][52a].

### What if the non-derailleur bike has vertical dropouts?

Bike frames with a [vertical dropout][12b] are normally made for a
[derailleur][10k] drivetrain -- unless they are converted to a
[single-speed][10l] or an [internal-gear][10a] hub, like my [IGH touring
bike][upgrade1]. :smile:

In this case, we can try: Perhaps, the chain length with the _right amount of
slack_ comes out as an _even number of links_!

I got lucky with my touring bike and its [8-speed Alfine IGH][90]:  The
[chainring's][10e] _42T_ (teeth), the [sprocket's][10n] _18T_, and the length
of the [chainstay][10g] conspired to an even number of links -- at the
right amount of chain slack, see
[Q&A](#how-to-adjust-the-chain-slack-on-a-non-derailleur-drivetrain).

This is how the mounted chain looks like: Perfect slack, no need for a chain
tensioner!

![Chain Tension][chain-tension01]

_Voilà!_  I've never had the chain skipping teeth or jumping off.

P.S.: Besides the current tooth ratio of 42T : 18T, I was also considering
40T : 20T, which should have the same chain slack.  But then, I already find
myself riding in 6th or 7th gear most of the time, which a smaller ratio would
push even higher.  The current ratio is ideal for commuting or touring.

### What if the nearest even number of links isn't a good fit?

It's possible for a [non-derailleur][12] drivetrain with a [vertical
dropout][12b], that the nearest even number of links makes the chain come
out _too tight_ (binding the rear wheel), or equally bad, _too loose_ (jumping
off).  We have a few options left:
1. Experiment with other gear ratios _front : rear_ teeth.  Possibly, a _39T_
   front or a _20T_ rear sprocket gives or takes up some slack.
1. Install a [chain tensioner][12e], which takes up any amount of slack; for
   details, see [(SB2)][12f].  Shimano offers two versions for the Alfine:
   [CT-S500][91] and [CT-S510][92].
1. Experiment with a _cranked half-link_ (offset link), such as [this][93] or
   [this][94], which extends an _inner plate end_ by 1 link, and so allows for
   a total _odd number_ of links.
1. Install an [eccentric bottom bracket][10h] (if possible), which allows the
   bottom bracket to be moved slightly for fine-tuning the chain tension.
1. See [(SB2)][12f] for details on above solutions and for "Sheldon's
   Kludges". :wink:

At last, some wide chains for single-speed drivetrains consist entirely of
_cranked half-links_ and, thus, do not require an even number of links, for
example, see this video [(PT3)][53c].

### Resources

Recommended videos and articles cited in this post:
- chain sizing for derailleur drivetrains: [(PT1)][51], [(PT2)][50],
  [(SB1)][11a]
- slack adjustment for non-derailleur drivetrains: [(PT3)][53], [(PT4)][52],
  [(SB2)][12]

[chain-tension01]: /img/2021-08-17-bicycle-chain-tension01.jpg
[chain-tension02]: /img/2021-08-17-bicycle-chain-tension02.jpg
[chain-tension03]: /img/2021-08-17-bicycle-chain-tension03.jpg

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[upgrade1]: /post/2021-08-09-bicycle-upgrade1/

[10a]: https://www.sheldonbrown.com/gloss_i-k.html#internal
[10b]: https://www.sheldonbrown.com/gloss_e-f.html#forkend
[10c]: https://www.sheldonbrown.com/gloss_p.html#pulley
[10d]: https://www.sheldonbrown.com/gloss_ch.html#chaintensioner
[10e]: https://www.sheldonbrown.com/gloss_ch.html#chainring
[10f]: https://www.sheldonbrown.com/gloss_cn-z.html#cog
[10g]: https://www.sheldonbrown.com/gloss_ch.html#chainstay
[10h]: https://www.sheldonbrown.com/gloss_e-f.html#eccentric
[10i]: https://www.sheldonbrown.com/gloss_ch.html#chaintool
[10j]: https://www.sheldonbrown.com/gloss_dr-z.html#drivetrain
[10k]: https://www.sheldonbrown.com/gloss_da-o.html#derailer
[10l]: https://www.sheldonbrown.com/gloss_sa-o.html#singlespeed
[10m]: https://www.sheldonbrown.com/gloss_dr-z.html#dropout
[10n]: https://www.sheldonbrown.com/gloss_sp-ss.html#sprocket
[10o]: https://www.sheldonbrown.com/gloss_e-f.html#frame

[11a]: https://www.sheldonbrown.com/derailer-adjustment.html#chain

[12]: https://sheldonbrown.com/no-derailers.html
[12b]: https://sheldonbrown.com/no-derailers.html#vertical
[12d]: https://sheldonbrown.com/no-derailers.html#installation
[12e]: https://sheldonbrown.com/no-derailers.html#tensioners
[12f]: https://sheldonbrown.com/no-derailers.html#workarounds

[20]: https://en.wikipedia.org/wiki/Do_it_yourself

[30a]: https://www.youtube.com/watch?v=DT5p0SXXNYs&t=530s

[50]: https://www.parktool.com/blog/repair-help/chain-length-sizing
[50a]: https://www.parktool.com/blog/repair-help/chain-length-sizing#article-section-2
[50b]: https://www.parktool.com/blog/repair-help/chain-length-sizing#article-section-3
[50c]: https://www.parktool.com/blog/repair-help/chain-length-sizing#article-section-4
[50d]: https://www.parktool.com/blog/repair-help/chain-length-sizing#article-section-5
[50e]: https://www.parktool.com/blog/repair-help/chain-length-sizing#article-section-6

[51]: https://www.youtube.com/watch?v=O0YibMDWBAw
[51b]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=58s
[51c]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=178s
[51d]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=242s
[51e]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=386s

[52]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes
[52a]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes#article-section-3
[52b]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes#article-section-4

[53]: https://www.youtube.com/watch?v=88tDcVvS7mU
[53b]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=114s
[53a]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=251s
[53c]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=466s

[54]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes

[90]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-8.html

[91]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/CT-S500.html

[92]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/CT-S510.html

[93]: https://www.connexchain.com/en/product/connex-cranked-link-half-link-1.html

[94]: https://www.kmcchain.com/en/series/chain-connector-single-speed#cut-6
