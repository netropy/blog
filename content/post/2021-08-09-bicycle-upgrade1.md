---
title: Blending old & new tech (1)
subtitle: Upgrading my touring bicycle from 1993
date: 2021-08-09
tags: ["DIY", "bicycle", "bicycle commute", "bicycle touring", "bicycle renovation", "bicycle conversion"]
---

Ever had a bike that's comfortable as an old shoe and just as well-worn?

So, renovate the old bike?  Make it fit for another 25+ years?  Upgrade
components where has technology has improved?

Or shop for a new bike?  Buy what and where?  What components to add or
customize?

Which option is more economical?  More eco-friendly?  Gives me the better
_commuter and touring_ bicycle?

Here's a 7-part, bicycle technology blog series with photos of a fun and
illustrative [DIY][14] project
... <!--more-->

### A touring / city / commuter bicycle

The bike below is what's generically called a _touring bicycle_, in Germany:
an all-purpose road bike, good for daily city commutes, grocery shopping,
excursions into the country, or serious bike touring (with camping gear).

I bought this bicycle back in 1993, the model's name "Torpedo Stratos" never
made it into the interweb.  The bicycle was manufactured in Italy and combined
a Tange 1000 Chromoly Steel frame with a 400CX (3x7) derailleur gear from
Shimano.  After 25 years of use in Germany and the U.S., most parts on the
bike were worn down (alas, I didn't take photos of them).

After a major upgrade, which I did by myself in 2018, here is what my touring
bicycle looks today:

![Torpedo Stratos][side]

This blog series shows the new parts and tech behind it.  We start with why
we embarked on a major overhaul instead of buying a new bike...

[side]: /img/2021-08-09-bicycle-side.jpg

[14]: https://en.wikipedia.org/wiki/Do_it_yourself
