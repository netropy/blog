---
title: Blending old & new tech (6)
subtitle: New center pull Cantis with "Retro Classic" levers
date: 2021-08-14
tags: ["DIY", "bicycle", "bicycle renovation", "bicycle brakes", "bicycle brake levers"]
---

The _Cantilever_ brakes of my 25+ year-old bike's were worn out and in dire
need of replacing (also the cables and the levers).

Brake systems have evolved over time.

What are the right parts for these brakes?  Modern _V-Brakes_ are also called
_Cantilevers_ but look quite different.  So, can I just pick any levers for
_my_ Cantis?

Again, Sheldon Brown's [website][7] (which lives on!) had a ton of useful
information.  After I knew what I needed, the only problem was: finding some
brakes and levers that looked "ordinary"
... <!--more-->

### Brakes -- When "Retro Classic" dates me...

My 1993 bike gives testimony of a time when _center pull_ Cantilever brakes
ruled the middle-of-the-market, before _direct pull_ Cantilever brakes,
a.k.a. _V-Brakes_ or _linear pull_ brakes, took over.

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

Happily, I found these new, center pull Cantis and levers as replacement for
the old ones -- but I had to chuckle when reading they were branded as:
"Origin8 Retro Classic Levers".

_Retro Classic?_  What do you mean?  They look just like _ordinary_ brakes
and levers :wink:

![Torpedo Stratos][brakes]

I no longer see these _Origin8 Retro Classic Levers_ and _Origin8 Pro Force
Cantilever Brakes_ on the [origin8.bike][8] website.

[brakes]: /img/2021-08-09-bicycle-brakes.jpg

[7]: https://www.sheldonbrown.com/brakes.html

[8]: https://www.origin8.bike/category/cantilever-&-caliper
