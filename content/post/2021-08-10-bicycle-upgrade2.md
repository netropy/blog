---
title: Blending old & new tech (2)
subtitle: Bike-Commuting in the Bay Area
date: 2021-08-10
tags: ["DYI", "bicycle", "bicycle commute", "bicycle touring", "Bay Area"]
---

Some _data points_ on bike-commuting in Europe vs California -- and why
shopping for a commuter or touring bike feels _difficult_ here in CA
... <!--more-->

### Bicycles as means of transport?

For reasons unbeknownst to me, _touring / city / commuter bicycles_ seem to be
a small market segment here in the U.S., compared to Europe.  Looking at
physical and online bike shops, at one end there's a large product range of
high-end road (racing) machines, on the other the more affordable (but heavy)
mountain bikes; plus, nowdays, also a variety of e-bikes.  But where's the
"middle" segment of ordinary, economical touring bikes?

For sure, the Bay Area has an active bike-commuter crowd.  Like in other
metropolitan areas with train services, the combination train + bicycle often
beats other modes of commuting: no traffic, no car parking, daily exercise for
free.  Occasional chats with bike-commuters, about a leather saddle or a
lugged steel frame, reveals that many of them have built their bike for and by
themselves!

It baffles me: To many Californians, bicycling is a serious sport or hobby;
why isn't it _also_ seen by more as a serious means of _transport_ (as in,
say, Northern Europe) -- the climate here so much supports bike-commuting.

### Conditions for bicyclists are improving in the Bay Area!

But then, climate is only one of many factors.  This [Stanford Bicycle
Commuter Access Study \(2017\)][20] contributes some interesting data points!

Not surprisingly, the stated factors where Bicyclists and non-Bicyclists
_diverge_: Health/Lifestyle vs Children/Time.

![stanford1]

Nevertheless, within just 14 years, bike-commuting to Stanford campus has
almost _doubled_ while car-commutes have dropped by a third.

![stanford2]

I can confirm how much safer and nicer many roads have become here for
bicyclists.  Dedicated bike lanes help enormously -- but the _bike boulevards_
(deep purple), which are corridors thorough a city and closed for car traffic,
are simply a treat to ride!

![stanford3]

Road conditions for bicyclists have been improving all over the Bay Area.
This [Google Bike Map][21] shows how accessible the _South Bay_ and
_Peninsula_ have become for bicyclists:

![googlemap]

### Yet, try shopping for a city/commuter/touring bike...

Despite favourable conditions for bike-commuting, it has never felt easy here
to shop for a _nimble, sturdy, and economical_ city and touring bike.

It has not helped that 3 out of 5 nearby bike shops have closed since 2019
(a market opportunity if it weren't for prohibitive rents).

Take the bike shop of a large outdoor store, here: Just 1 out of 97 bikes fits
_urban cycling_ and _size=L_.  But I want: _size=XL_, a classical frame with a
horizontal top tube, and no front suspension.

The other bike store here lists 16 _size=XL, active-fitness_ bikes, 3 of which
for under $800.  But these bikes are rarely suitable for touring with just 2x7
(2x8) derailleur gears (and an aluminum frame and fork).

The _online stores_ carry a large range of bikes and brands.  But how to try
out a bike beforehand?  The store's return policies may require _as-new_
condition and shipping is not effortless and cheap (and has an environmental
impact).

Then there are _boutique bicycle shops_ (within 1..10 hours of driving) with
beautifully composed bikes (vintage, modern, or blended).  But those tend to
be expensive and seem less suited for daily commutes or grocery shopping
(train rides, locking in public places, risk of theft etc).

__In summary:__ Hard to find good commuter & touring bikes in shops, here.

Ok, the above shop sample had just 2 out of a 14 stores within 1 hr driving
range.  Still, not much better elsewhere (especially, finding a bike with
internal-gear hub, see subsequent posts).

![South Bay bike shops][shops]

[shops]: /img/2021-08-09-bicycle-shops-Bay-Area.jpg
[stanford1]: /img/2021-08-09-stanford-bike-commuter-access-study1.jpg
[stanford2]: /img/2021-08-09-stanford-bike-commuter-access-study2.jpg
[stanford3]: /img/2021-08-09-stanford-bike-commuter-access-study3.jpg
[googlemap]: /img/2021-08-09-google-bike-map-South-Bay.jpg

[20]: https://transportation.stanford.edu/sites/default/files/2017-10/Stanford_Bicycle_Commuter_Access_Study_2017.pdf

[21]: https://www.google.com/maps/@37.363626,-122.0096736,11z/data=!5m1!1e3
