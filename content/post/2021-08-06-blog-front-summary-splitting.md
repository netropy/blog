---
title: How to shape a blog's summary view on the front page
date: 2021-08-06
tags: ["blog", "website", "Hugo", "Beautiful Hugo Blog Template"]
---

This website's front page shows a summary view of each recent blog post,
the first few lines of it -- nice!

Then, how does the [Hugo Blog Template][14] determine how much text to show as 
summary?  Can this amount be configured globally for the website?  Can it be
adjusted for each post?  Can a different summary be displayed than the post's
text?

Cool -- The answer to each of these questions is: _yes_
... <!--more-->

_Hugo's_ documentation on [Summary Splitting Options][50] is a quick and clear
read:
1. _Automatic Summary Split_ after _N_ words, customizable as `summaryLength`
   in the [site configuration][51] (here, file _config.toml_).
1. _Manual Summary Split_ using the `<!--more-->` _summary divider_ as
   demarcation; this is not unique to _Hugo_ and also called "more tag" or
   "excerpt separator".
1. _Front Matter Summary_ displaying the value of the variable `summary` of
   the [article front matter][52], as embedded in a post (as _YAML, TOML,
   JSON,_ or _ORG_).

So, what does this post do?  It places the _summary divider_ along with an
ellipsis to help readability:
```code
Cool -- The answer to each of these questions is: _yes_
... <!--more-->
```

[14]: https://pages.gitlab.io/hugo/

[50]: https://gohugo.io/content-management/summaries/

[51]: https://gohugo.io/getting-started/configuration/

[52]: https://gohugo.io/content-management/front-matter/
