---
title: Blending old & new tech (3)
subtitle: Rack and fenders not optional for a touring bicycle
date: 2021-08-11
tags: ["DIY", "bicycle", "bicycle renovation", "bicycle touring", "bicycle commute", "bicycle fenders", "bicycle rack"]
---

New bikes are sold quite "bare" here in the U.S.: no rack, fenders, lights, or
bottle holders.

So, whether upgrading an old bike or buying a new one, one needs to plan for
extra costs and time
... <!--more-->

### Add fenders and a solid rack

For a city or a touring bike, a _rack_ and _fenders_ are a must.  In rain,
fenders are a life-saver (preventing the "skunk stripe").  Yet, full-length
fenders also do a job protecting the bicycle; for example, the mud flaps hold
back dirt so that the chain stays cleaner and lasts longer (see [(SB)][23]).

Most bikes here in U.S. are sold without fenders and a rack.  So, the _pro_ is
that I can pick my own; the _con_ is that they cost extra.

In other words, if the old bike's components are still good, one can mount
them on a new bike; otherwise, one needs to shop for new components anyway,
whether for the old or a new bike.

__In summary:__ The costs for components do not weigh in on the buy-vs-upgrade
question (unlike in Europe where touring bikes are sold fully equipped).

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

Here are the bike's new [rack][24], [fenders][25], and [seatpost][26]:

![Torpedo Stratos Rack][rack]

__Tip:__ When mounting components, always apply a drop of [thread-locker][12]
on screws or bolts to prevent them from coming loose (and loosing them).

[Topeak - Super Tourist DX Rack - Spring][20]: I had to order this rack from a
shop in the U.K. to get the model with a _spring_!  A spring comes quite handy
if one needs to transport stuff but has no basket or a rubber band at hand.

Note how this rack is well-equiped for carrying commuting and bicycle touring
[pannier bags][22].  The extra, horizontal bar on the side not only increases
the rack's weight capacity but also lowers the center of mass, thus, improving
steering and directional stability.  In addition, the lower bar allows to have
mounted both, panniers and a basket, with a little amount of space between
them (depending on the model).

[Portland Design Works - Poncho Fenders - City][21]: These fenders are
light-weight and match the bike's style.

[Thomson Elite InLine Seatpost][19]: The challenge was to find a _quality_
seatpost of _29.4 mm_ diameter, as found on older European bicycles.  One
could adjust a slightly thinner post with a _shim_, but it's not ideal.
Happily, there are _12_ diameters to choose from with this seatpost from
_Thomson_.  It fits my bike perfectly and is well-made (safety-relevant).

![Torpedo Stratos Seatpost][seatpost]

P.S.: I'm not happy with the current saddle -- to be continued.

[rack]: /img/2021-08-09-bicycle-rack.jpg
[seatpost]: /img/2021-08-09-bicycle-seatpost.jpg

[12]: https://en.wikipedia.org/wiki/Thread-locking_fluid

[19]: https://www.bikethomson.com/product/elite-seatpost/

[20]: https://www.chainreactioncycles.com/topeak-super-tourist-dx-rack-spring/rp-prod32818

[21]: https://ridepdw.com/products/poncho-fenders?variant=24745574593

[22]: https://cycloscope.net/best-bike-panniers

[23]: https://www.sheldonbrown.com/fenders.html

[24]: https://www.sheldonbrown.com/gloss_ra-e.html#rack

[25]: https://www.sheldonbrown.com/gloss_e-f.html#fender

[26]: https://www.sheldonbrown.com/gloss_sa-o.html#seatpost
