---
title: Checklists for bicycle maintenance (0)
subtitle: Easy D.I.Y. (do it yourself) bike service
date: 2023-09-02
tags: ["DIY", "bicycle", "bicycle maintenance"]
---

For [D.I.Y.][9a] bicycle mechanics, some maintenance tasks just occur
too infrequently to remember the details of each step.  Memory items like
these:
- To remove both bike pedals, which one unscrews clockwise and which one
  anti-clockwise? \
  _(right/drive side: anti-clockwise, left/non-drive side: clockwise)_
- When adjusting a cantilever brake, what was the trick to adjust together the
  brake shoe's vertical height, face, tangent, and toe-in? \
  _(use a rubber band as shim, press shoe against rim, tighten mounting nut)_
- For bikes with hydraulic disc brakes, what were the things one should not
  do? \
  _(stand bike upside down, pull brake lever while wheel's out of the bike,
  and more)_

Yet, some maintenance is required often enough that one does not want to spend
time on reading on websites or watching how-to videos, every time.  Also,
while there are excellent how-to resources on the web (such as
[SheldonBrown][2] or [ParkTool's RepairHelp][1], see previous [blog][0a]), it is
just impractical to scroll the screen, or switch browser tabs, or start/stop a
video -- while in the middle of working on a bike.

So, I started to write down [_checklists_][9a] for servicing our bicycles: for
each maintenance task, a table listing the basic steps, ideally on 1..2 pages
per task, plus links to resources.

Here a blog series sharing my bicycle maintenance checklists
... <!--more-->

The scope of the checklists is to cover all regular maintenance of touring,
commuter, or "performance" bicycles, not so much of road and mountain bikes.
This should include all component technologies going back 4..5 decades (given
the age and variety of our bikes).  Where the checklists are insufficient, one
can look up the listed web resources.

The blog series covers these areas: \
[(1) wheels, tubes, tires][r1] \
[(2) drivetrain, chains, hubs, gears, cranks, bottom brackets][r2] \
[(3) saddles, seatposts, pedals, shoes, grips, handlebars, stems, headsets][r3] \
[(4) brakes][r4]

[0a]: /post/2023-09-01-bicycle-resources/
[r0]: /post/2023-09-02-bicycle-routine-maintenance0.md
[r1]: /post/2023-09-03-bicycle-routine-maintenance1.md
[r2]: /post/2023-09-04-bicycle-routine-maintenance2.md
[r3]: /post/2023-09-05-bicycle-routine-maintenance3.md
[r4]: /post/2023-09-06-bicycle-routine-maintenance4.md

Each maintenance task also lists _resources_ and _videos_ that link to expert
sources, which are listed as [(SB)][2], [(PT)][1], [(BR)][5], [(BG)][4],
[(CMA)][3], and other, see previous [post][0a].

_Note:_ \
I update these checklists _in-place_ for improvements and clarifications;
also, some tasks still need to be written up.  For comments: martin dot zaun
at gmx dot com

The first checklist starts with some basics...

### 0.1 Check bicycle for basic safety, problems, noises

It's a good idea to run a quick safety check on a bike when borrowing one, or
after a repair, or after some extended time.

| | |
|:-:|:--|
| (1) | the _ABC Quick Check_ for basic safety takes 20..30 seconds: see [video][2a] |
| | Air, Brakes, Crank/Chain/Casette, Quick-release levers, visual Check |
| | |
| (2) | additonal tests: see [(SB)][2b] |
| | _wheels:_ trueness, about equal spoke tension, tire seating/wear/damage, see [(1)][r1] |
| | _brake lever:_ reach, travel to "bite point", springy vs stiff, see [(4)][r4] |
| | _brakes:_ no play in brake mount or brake arms, see [(4)][r4] |
| | _headset:_ front wheel swings freely, no binding, no play, no "indexed" steering, see [(3)][r3] |
| | _hubs:_ no axle play, minimal drag, bearings not too tight/loose, little grease friction, see [(2)][r2] |
| | _alignment:_ axle nut/quick-release engages abruptly, no flexing when closing, no steering to one side when riding no-hands |
| | _gears, shifting:_ smooth into all gears, no binding of chain, no oil leakage, see [(2)][r2] |
| | _pedals, bottom bracket:_ no play, minimal drag, little grease friction, see [(3)][r3], [(2)][r2] |
| | |
| (3) | check for and isolate noises: see [(SB)][2c] |
| | - while braking? |
| | - while coasting? per wheel revolution? |
| | - while pedaling? per pedal revolution? per chain revolution? |
| | - when leaning/pulling on handlebars? |
| | - randomly or road vibrations? |

Resources: \
[(SB) quick-check bike][2b];
[(SB) diagnose creaks, clicks & clunks][2c];

Videos: \
[(SB) ABC Quick Check][2a];

[1]: https://www.parktool.com/blog/repair-help

[2]: https://www.sheldonbrown.com

[2a]: https://www.youtube.com/watch?v=9VziOIkNXsE&t=6s
[2b]: https://www.sheldonbrown.com/bike-tests.html
[2c]: https://www.sheldonbrown.com/creaks.html

[3]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[4]: https://bike.bikegremlin.com/post-list-by-category/

[5]: https://www.bikeride.com/guide/

[9a]: https://en.wikipedia.org/wiki/Do_it_yourself
[9b]: https://en.wikipedia.org/wiki/Checklist
