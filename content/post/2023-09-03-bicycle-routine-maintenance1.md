---
title: Checklists for bicycle maintenance (1)
subtitle: Wheels, tubes, tires
date: 2023-09-03
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle wheels", "bicycle tubes", "bicycle tires", "bicycle axles"]
---

Most bicycle riders have probably some practice in removing/installing a
wheel, or replacing a tire or inner tube.  But I like _checklists_ to remind
me of tips, such as: \
\- Start working the tire levers around the valve. \
\- Drop the tire's bead into the rim's well for more slack. \
\- Inflate the tube just enough to hold its shape before installing it inside
the tire.

Checklists are also useful when looking after bikes with different components: \
\- How again do I quick-release this road brake vs. that cantilever brake? \
\- On which side goes the lever of this quick-release axle? \
\- What is the torque range for those axle nuts?

These checklists cover various maintenance tasks around wheels: \
[1.1 remove wheel][r1.1] \
[1.2 install wheel][r1.2] \
[1.3 remove/install inner tube or clincher tire][r1.3] \
[1.4 inspect/patch inner tube][r1.4] \
[1.5 check/true wheel][r1.5] \
[1.6 build wheel][r1.6] \
[1.7 install rim tape][r1.7] \
[1.8 types and sizes of tires/tubes/valves][r1.8] \
[1.9 resources, playlists][r1.9]
... <!--more-->

[r1.1]: #11-remove-wheel
[r1.2]: #12-install-wheel
[r1.3]: #13-remove-install-inner-tube-or-clincher-tire
[r1.4]: #14-inspect-patch-inner-tube
[r1.5]: #15-check-true-wheel
[r1.6]: #16-build-wheel
[r1.7]: #17-install-rim-tape
[r1.8]: #18-types-and-sizes-of-tires-tubes-valves
[r1.9]: #18-resources-playlists

Links to the _resources/videos_ on [(SB)][2], [(PT)][1], [(BR)][5], [(BG)][4],
[(CMA)][3], see [post][0a], given in each task.

_Note:_ I update these checklists _in-place_ for improvements and
clarifications; also, some tasks still need to be written up.  For comments:
martin dot zaun at gmx dot com

### 1.1 Remove wheel

| | |
|:--:|:--|
| (1) | mount bike in a repair stand, or stand bike upside down, or lay on its left (non-drive) side |
| | _for hydraulic brakes:_ avoid standing bike upside down (may have to bleed brake) |
| | |
| (2) | quick-release/disengage/undo the brake, as needed: |
| | _for disc brakes:_ not needed |
| | _for direct-pull or V-brakes:_ squeeze brake arms together and push down the cable bridge, disconnect |
| | _for cantilever brakes:_ squeeze brake arms together and unhook the link- or straddle-cable from the arm |
| | _for drum brakes:_ disconnect the reaction arm and any brake cable; some brakes have quick-release attachments |
| | _for other brakes:_ check for a button/lever/mechanism at the brake lever, cable hanger, or brake itself |
| | _otherwise:_ align the slotted barrel adjuster with the lock nut and pull out the cable housing |
| | |
| (3) | adjust gears, disconnect any shifter cable or hub dynamo cable, as needed: |
| | _for derailleur gears:_ shift to smallest chainring + sprocket/cog for most chain slack |
| | _for Alfine/Nexus hub gears:_ shift to the lowest/highest gear with the most cable slack |
| | put a pin or 2mm hex key into the cassette joint pulley's hole, turn to increase slack and hold the position |
| | pull out the outer casing holder from the holder section of the cassette joint |
| | unhook the shifter cable's fixing bolt unit from the pulley notch |
| | _for hub dynamos:_ disconnect the cable connector from the hub's connection terminal |
| | |
| (4) | release the wheel: |
| | _for quick-release axle:_ open the quick-release lever |
| | loosen the adjusting acorn nut, as needed (for dropouts with "lawyer/retention lips") |
| | _for axle nuts:_ loosen both nuts outside the dropouts/forkends with a 15mm wrench (or 13mm, 14mm) |
| | take note of any spacers between the hub and the dropout/forkend |
| | _for thru axle:_ if it has a lever, open; if it has a fitting/wedge, engage; if it has a bolt head, use a hex key |
| | turn anticlockwise, pull axle straight out; if other side has an axle nut, do not loose it |
| | |
| (5) | lift/drop/pull the axle out of the dropouts/forkends |
| | _for derailleur gears or chain tensioners:_ pull the derailleur backwards out of the way of the axle |
| | if derailleur has a clutch mechanism, lock the lower pulley forward by button (SRAM) or lever (Shimano) |
| | _for hub gears or single speeds:_ unhook the chain from the sprocket/cog (and chainring, as needed) |
| | |
| (6) | _for hydraulic brakes:_ prevent brake pads from closing together |
| | do not squeeze the brake lever or, better, slide a plastic spacer between pads while the wheel is removed |
| | to spread pads apart: use a spreader tool, a piston press tool, a wooden shim, or a flat screwdriver |
| | using a screwdriver: squeeze between one pad and its piston, press against the other pad, repeat for other side |
| | be careful not to damage the resin/metal/ceramic surface of the brake pads |

Resources: \
[(PT) remove/install wheel][1b];
[(SB) remove/install quick-release axle, mechanism][2b];
[(BG) remove/install wheel][4c];
[Alfine/Nexus Hub Gear][10a];
[Alfine Hub Gear][10b];
[Alfine Hub Dynamo][10c];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) remove/install wheel][1c];
[(SB) disconnect/reconnect quick-release brakes][2c];
[(CMA) remove front wheel][3g];
[(CMA) install front wheel][3e];
[(CMA) remove back wheel][3h];
[(CMA) install back wheel][3f];
[(CMA) disconnect/reconnect cantilever and caliper brake][3b];
[(CMA) disconnect/reconnect V-brake][3a];
[remove/install quick-release front wheel][7b];
[remove/install quick-release rear wheel][7c];
[(CMA) remove/install quick-release wheel][3c];
[(CMA) quick-release axle mechanism][3i];
[remove/install thru axle with lever][7a];
[remove/install thru axle with bolt head][8a];
[remove/install thru axle front wheel][7d];
[(CMA) remove/install thru axle wheel][3d];
[remove/install thru axle wheel: road bike][7e];
[remove/install thru axle wheel: MTB bike][7f];
[install thru axle and quick-release wheel][8b];

Other: \
[(CMA) disconnect roller brake cable][3r];
[(CMA) reconnect roller brake cable][3s];
[(CMA) remove/install roller brake wheel][3t];

Other: \
[(CMA) remove/install Rohloff Speedhub wheel][3u];
[(CMA) remove Shimano and Sturmey Archer hub gear wheel][3v];
[(CMA) install Shimano and Sturmey Archer hub gear wheel][3w];

### 1.2 Install wheel

| | |
|:--:|:--|
| (1) | insert/lift/slide the axle into the dropouts/forkends, check that axle is fully seated |
| | _for directional tires:_ check indicator on tire, tread's direction points backwards |
| | _for disc brakes:_ line up the rotor to fit in the gap between the brake pads |
| |	_for quick-release axle:_ lever goes left (non-drive) side, each side's spring points inward |
| | _for axle nuts:_ if axle has spacers, check that they're positioned between the hub and the dropout/forkend |
| | _for thru axle:_ line up the hub with the dropouts, slide the axle from the left side all the way in |
| | |
| | _for derailleur gears or chain tensioners:_ pull the derailleur backwards out of the way of the axle |
| | put the wheel into the loop of the chain, aligh chain with the smallest sprocket/cog, guide axle into the dropouts |
| | if derailleur has a clutch mechanism, release the lower pulley by button (SRAM) or lever (Shimano) |
| | _for hub gears or single speeds:_ adjust the chain tension and align the wheel |
| | hook the chain onto the sprocket/cog (and chainring, as needed) |
| | alternatingly loosen/shift/tighten the left/right side ("jockeying technique") ... |
| | ... until the wheel is aligned with the frame and the chain moves ±1.25cm (±0.5") when pulled up/down |
| | |
| (2) | tighten the wheel: |
| |	_for quick-release axle:_ align lever with the fork or between the chainstay and seat stay, don't close yet |
| | for exposed-cam levers (visible curved plastic washer), check that the lever is properly seated in the indent |
| | tighten the adjusting acorn nut enough that the lever meets resistance at a right angle from the frame |
| | close the quick-release lever upwards, check that lever is unobstructed, tight, and flat |
| | |
| | _for axle nuts:_ tighten alternatingly with 15mm wrench (13mm, 14mm), tighten firmly to 25..40 Nm or recommended torque |
| | _for drum brakes:_ tighten axle nuts and reaction arm in steps, so they don't cause each other to bind |
| | _for thru axle:_ turn clockwise |
| | if axle has a lever, turn; if it has a fitting/wedge, engage and turn; close like a quick-release lever |
| | if axle has a bolt head, use a hex key, tighten to 10 Nm or recommended torque |
| | |
| (3) | reconnect any shifter cable or hub dynamo cable, as needed: |
| | _for Alfine/Nexus hub gears:_ push the outer casing holder into the holder section of the cassette joint |
| | put a pin or 2mm hex key into the cassette joint pulley's hole, turn to increase slack and hold the position |
| | slide the shifter cable's fixing bolt unit into the pulley notch with the mounting nut facing outwards |
| | check that the cable is correctly seated inside the pulley guide |
| | _for hub dynamos:_ reconnect the cable connector to the hub's connection terminal |
| | |
| (4) | reconnect/rejoin/close the brake, as needed: |
| | _for disc brakes:_ not needed |
| | _for direct-pull or V-brakes:_ squeeze brake arms together and push up the cable bridge, connect |
| | _for cantilever brakes:_ squeeze brake arms together and hook the link- or straddle-cable into the arm |
| | _for other brakes:_ check for a button/lever/mechanism at the brake lever, cable hanger, or brake itself |
| | _fallback:_ align the slotted barrel adjuster with the lock nut and slide in the cable housing |
| | |
| (5) | check axle, gears, brake performance: |
| | no axle play, no wheel hub friction |
| | smooth gear shifting |
| | _for Alfine/Nexus hub gears:_ aligned yellow setting lines at cassette joint (see below) |
| | no brake rubbing, adequate brake lever pull and reach, see [1.5][r1.5] below |

Resources, videos: see [1.1][r1.1] above

### 1.3 Remove/install inner tube or clincher tire

| | |
|:--:|:--|
| | for types and sizes of tires/tubes/valves, see [1.8][r1.8] below |
| | |
| (1) |	remove wheel from bike, see [1.1][r1.1] above |
| | |
| (2) | deflate tire completely: |
| | unscrew the valve cap and any stem nut (valve collar), set aside |
| | valves: _Presta:_ unthread locknut, press; _Schrader:_ depress inside plunger; _Dunlop:_ unthread cap, pull |
| | _for thick tires:_ press downward on wheel while depressing the valve |
| | |
| (3) | loosen beads (tire edges) from rim: |
| | squeeze tire sidewalls together going around wheel |
| | if bead is stuck on the rim, push/pull tire sidewall with force toward rim center |
| | |
| (4) | pry one tire bead up and over the rim sidewall: |
| | recommended: use 2..3 tire levers that have hooks for spokes |
| | at the nearest spoke left + right of the valve, hook 1st and 2nd tire lever under the bead |
| | at once, pull both levers toward spokes and lift the bead off the rim, hook each lever onto its spoke |
| | repeat at spoke next to the right, use a 3rd lever (or disengage the 1st lever) to lift bead off rim |
| | repeat engaging the lever until bead loosens and easily lifts over rim sidewall |
| | if tire needs more slack, drop the bead into the rim's well (center channel) at opposite end |
| | be careful not to pinch the tube between beads/rim/levers |
| | |
| (5) | pull tube from tire: start opposite the valve, lift valve from valve hole, remove tube |
| | |
| (6) | recommended: inspect tube, tire, and rim |
| | for easier inspection, pull the remaining tire sidewall over the rim and remove tire completely |
| | check for punctures, debris, or other damage, see [1.4][r1.4] below |
| | |
| (7) | as needed, install one side of tire onto rim in correct orientation: |
| | place wheel flat on a surface right side (drive side) up, wheel rotation will be clockwise |
| | check if tire has directional indicators, flip as needed to match wheel rotation |
| | recommended: line up the air pressure specs on tire's sidewall with the rim's valve hole |
| | pull bead over the rim's sidewall using both hands, use tire lever as needed |
| | |
| (8) | install tube inside tire: |
| | _for very long valve stems_ (60+mm): install tube before step (7) |
| | inflate tube just a little bit, place on the wheel and align valve with rim's valve hole |
| | fit valve stem through valve hole, bend away tire as needed |
| | loosely secure valve with a valve cap or stem nut (valve collar), don't tighten yet |
| | adjust tire and tube such that valve stem is not crooked but points straight toward hub |
| | |
| (9) | install other side of tire onto rim, starting at valve: |
| | _for very tight tire/rim combinations:_ start opposite valve, push bead into rim's well/channel |
| | again, pull bead over the rim's sidewall using both hands, use tire lever as needed |
| | be careful not to pinch the tube between beads/rim/levers |
| | |
| (10) | check that beads nowhere pinch tube against rim: |
| | speeze togther sidewalls to check that bead is moves freely (if needed, lift beads and reinstall) |
| | as needed, realign valve stem to point to hub by slightly shifting tire sideways |
| | |
| (11) | inflate tire to low pressure (~0.7 Bar/~10 psi) and inspect bead on both sides: |
| | check that the small molding line above bead runs evenly above rim, pull/push tire as needed |
| | if bead is stuck below rim, apply extra air pressure or use soapy water as lubricant |
| | |
| (12) | inflate tire to full pressure, check with gauge: |
| | valves: _Presta:_ tighten locknut; _Schrader, Dunlop:_ - |
| | reinstall any stem nut (valve collar) and valve cap (finger tight) |

Resources: \
[(SB) fix flat tires][2g];
[(PT) remove/install tire and tube][1g];
[(BG) remove tire and tube][4e];
[(BG) install tire and tube][4f];
[(SB) which tire pressure](https://www.sheldonbrown.com/tires.html#pressure);
[(BG) which tire pressure][4h];
[(SB) when (not) rotate tires][2l];

Videos: \
[(PT) remove/install tire and tube][1a];
[(CMA) straighten a bent tube valve][3j];
[(CMA) bicycle inner tubes][3k];
[(CMA) tire pressure][7g];

### 1.4 Inspect/patch inner tube

| | |
|:--:|:--|
| | for types and sizes of tires/tubes/valves, see [1.8][r1.8] below |
| | |
| (1) |	remove tire from wheel, see [1.3][r1.3] above |
| | optional: an inner tube can be patched on the bike without removing the wheel, see [(SB)][2h] |
| | |
| (2) | recommended: inspect the tube for punctures or a leaking valve |
| | inflate tube to twice its normal width, find air leaks by holding it close to ears or sensitive skin |
| | check what caused the flat, and where:
| | - single puncture: likely from sharp debris (thorn, wire, nail etc.) |
| | - double slits from a rim pinch ("snake bite"): hitting an object at low tire pressure |
| | - cut or hole on tube's inside edge: protruding spoke, damaged or displaced rim strip |
| | - large hole or rip: tire blowout (not repairable) |
| | - cut or hole at valve core: misaligned tube or riding at low tire pressure (not repairable) |
| | - leaky valve core: cause? (can be tighten with a valve core tool) |
| | |
| | if puncture cannot be found, submerge tube in water (bubbles) or wet the valve with soapy water |
| | if repair is planned, mark hole with marker pen (away from hole, as the mark may be sanded off) |
| | |
| | as stopgap: substitute a Presta valve tube for a Schrader tube using/improvising an adapter sleeve |
| | |
| (3) | recommended: inspect the tire for debris and damage |
| | wipe inside of the casing (liner) with a cloth while examining the sidewall and tread |
| | look for and carefully feel with fingers for embedded debris, holes, cuts, or other tire damage |
| | remove any debris with a plier, squeeze any cut to look for objects inside, such as slivers of glass |
| | |
| | as stopgap: patch up a bad tire cut by placing some non-stretchy material (folded bills) inside tire |
| | |
| (4) | recommended: inspect the rim and rim strip (tape) for damage |
| | inspect the rim strip for tears and rips, make sure rim strip is centered over the nipple holes |
| | the rim strip should cover the full bottom of the rim but not interfere with the seating of the bead |
| | |
| | as stopgap: improvise a rim tape from ripped duct tape or slice and vulcanize together an inner tube |
| | |
| (5) | fix tube with patch kit: |
| | check kit to have thin patches, sandpaper, glue (rubber cement, cow gum, vulcanizing agent) |
| | select a patch overlapping by 1cm (0.5") or more on every side of the hole, mark hole with pen |
| | abrade tube with sandpaper for an area larger than patch; area should look matte, dry, and clean |
| | sand down a bit any molding line but avoid excessive sanding or pressure |
| | |
| | apply a thin coat of glue, spread evenly with the back of the patch or tube nozzle, work quickly |
| | allow glue to dry completely (3..5 minutes), tap outer area with knuckle (outside patch area) |
| | peel the aluminum foil off patch, leave on the clear plastic cover, handle patch by the edges |
| | press patch onto tube, avoid trapping air, squeeze the entire patch firmly onto tube |
| | check that all edges are glued, leave the clear plastic cover on patch |
| | |
| | as stopgap: patch a tube with a "glueless" patch (unreliable) or duct tape (unflexible) |
| | |
| (6) | install tube inside tire, see [1.3][r1.3] above |

Resources: \
[(SB) fix flat tires][2g];
[(PT) patch tire and inner tube][1e];
[(SB) patch inner tube without removing the wheel][2h];
[(BG) patch inner tube][4d];
[(SB) select tire tools and supplies][2m];
[(SB) patch inner tube (Jobst Brandt)][2q];
[(SB) which tire pressure](https://www.sheldonbrown.com/tires.html#pressure);
[(BG) which tire pressure][4h];

Videos: \
[(SB) patch inner tube][2f];
[(PT) patch inner tube][1f];
[(SB) expose tube without removing the wheel][2d];
[(SB) reassemble tube and tire with wheel on bike][2e];
[(CMA) inspect tire, tube, rim][3m];
[(CMA) patch inner tube][3n];
[(CMA) fix Schrader valve air leak][3p];

Long videos: \
[(PT) fix a flat tire (combined edit)][1j];
[(CMA) change bike tyre][3x];
[(CMA) replace tube][3y];
[(BG) quick-release axle mechanism][4i];
[(BG) fix a flat tyre][4j];
[(BG) patch a punctured tube][4k];

### 1.5 Check/true wheel

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) true wheel][2o];
[(PT) true wheel #1][1k];
[(PT) true wheel #2+#3][1m];
[(PT) true wheel #4][1p];
[(PT) true wheel #5][1r];
[(PT) true wheel #6][1t];
[(PT) select spoke wrench][1v];

Videos: \
[(PT) true wheel #1][1l];
[(PT) true wheel #2][1n];
[(PT) true wheel #3][1o];
[(PT) true wheel #4][1q];
[(PT) true wheel #5][1s];
[(PT) true wheel #6][1u];
[(BG) check wheel][4n];

Long videos: \
[(BG) true wheel #1][4p];
[(BG) true wheel #2][4q];

### 1.6 Build wheel

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) build wheel][2p];
[(SB) determine spoke length][2t];
[(PT) determine spoke length][1w];
[(PT) build wheel][1y];
[(BG) build wheel][4l];

Videos: \
[(PT) determine spoke length][1x];
[(PT) build wheel][1z];
[(BG) lace spokes for Rohloff hub][4o];

### 1.7 Install rim tape

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) install rim tape #1][2p];
[(SB) inspect rim tape #2][2g];
[(BG) install rim tape][4m];
[install rim tape][12a];

### 1.8 Types and sizes of tires/tubes/valves

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) tires and inner tubes][2i];
[(SB) inner tubes][2j];
[(SB) tire sizing systems][2k];
[(SB) tire crib sheet][2n];
[(SB) 26 and 27.5 inch tires][2a];
[(SB) measure rim for tire fit][2s];
[(PT) tire, wheel and inner tube fit standards][1d];
[(BG) valve types and tube sizes][4b];

Videos: \
[(CMA) valves][3o];
[(CMA) bicycle tire sizes][3l];

Other: tubeless and tubular tires \
[(SB) why (not) tubeless tire][2r];
[(PT) remove/install tubeless tire][1h];
[(PT) convert to tubeless][1aa];
[(PT) convert to tubeless][1ab];
[(PT) convert to tubeless][1ab];
[(PT) setup tubeless][1ac];
[(PT) glue tubular tire][1i];

### 1.8 Resources, playlists

Lists: \
[(PT) tires, inner tubes, tubeless][1ba];
[(PT) tires, inner tubes, tubeless][1bb];
[(PT) wheels][1bc];
[(PT) wheels][1bd];
[(PT) wheels][1be];
[(BG) tires, inner tubes][4aa];
[(BG) wheels][4ab];
[(BG) wheels, tires][4ac];
[(BG) build wheels][4ad];

[0a]: /post/2023-09-01-bicycle-resources/
[r0]: /post/2023-09-02-bicycle-routine-maintenance0.md
[r1]: /post/2023-09-03-bicycle-routine-maintenance1.md
[r2]: /post/2023-09-04-bicycle-routine-maintenance2.md
[r3]: /post/2023-09-05-bicycle-routine-maintenance3.md
[r4]: /post/2023-09-06-bicycle-routine-maintenance4.md

[1]: https://www.parktool.com/blog/repair-help

[1a]: https://www.youtube.com/watch?v=eqR6nlZNeU8
[1b]: https://www.parktool.com/en-us/blog/repair-help/wheel-removal-and-installation
[1c]: https://www.youtube.com/watch?v=hdjB_wHW0-Q
[1d]: https://www.parktool.com/en-us/blog/repair-help/tire-wheel-and-inner-tube-fit-standards
[1e]: https://www.parktool.com/en-us/blog/repair-help/inner-tube-repair
[1f]: https://www.youtube.com/watch?v=T0F_hibWHlU
[1g]: https://www.parktool.com/en-us/blog/repair-help/tire-and-tube-removal-and-installation
[1h]: https://www.parktool.com/en-us/blog/repair-help/tubeless-tire-mounting-and-repair
[1i]: https://www.parktool.com/en-us/blog/repair-help/tubular-tire-gluing-sew-up
[1j]: https://www.youtube.com/watch?v=58STtUM-Wow
[1k]: https://www.parktool.com/en-us/blog/repair-help/how-wheel-truing-works
[1l]: https://www.youtube.com/watch?v=MFOng1UXn-g
[1m]: https://www.parktool.com/en-us/blog/repair-help/wheel-and-rim-truing
[1n]: https://www.youtube.com/watch?v=xz6nM6SY-aY
[1o]: https://www.youtube.com/watch?v=7R8dncAbhoM
[1p]: https://www.parktool.com/en-us/blog/repair-help/wheel-dishing-centering
[1q]: https://www.youtube.com/watch?v=FqeEBih8kx0
[1r]: https://www.parktool.com/en-us/blog/repair-help/wheel-tension-measurement
[1s]: https://www.youtube.com/watch?v=aYfL2wzkV4M
[1t]: https://www.parktool.com/en-us/blog/repair-help/wheel-tension-balance-app-instructions
[1u]: https://www.youtube.com/watch?v=mb32h4PK_aU
[1v]: https://www.parktool.com/en-us/blog/repair-help/spoke-wrench-tool-selection
[1w]: https://www.parktool.com/en-us/blog/repair-help/determining-spoke-length-for-wheel-building
[1x]: https://www.youtube.com/watch?v=kSFmvV3J99c
[1y]: https://www.parktool.com/en-us/blog/repair-help/how-to-build-a-wheel
[1z]: https://www.youtube.com/watch?v=X5gs00ttvdg

[1aa]: https://www.parktool.com/en-us/blog/repair-help/tubeless-tire-conversion
[1ab]: https://www.youtube.com/watch?v=MuEiBSAKWLI
[1ac]: https://www.youtube.com/watch?v=-0p5pE4sRJM

[1ba]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=60
[1bb]: https://www.youtube.com/playlist?list=PLGCTGpvdT04SCKR3pm1OsC5mUF9dapUuz
[1bc]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=62
[1bd]: https://www.youtube.com/playlist?list=PLGCTGpvdT04STuv86bSdOxtSvXGIC-eh1
[1be]: https://www.youtube.com/playlist?list=PLGCTGpvdT04QXRvU4M3o6wUp2yOgIAbIG

[2]: https://www.sheldonbrown.com

[2a]: https://www.sheldonbrown.com/26.html
[2b]: https://www.sheldonbrown.com/skewers.html
[2c]: https://www.youtube.com/watch?v=kJ3K1vt0Ocw
[2d]: https://www.youtube.com/watch?v=Hyq38Mma3AQ
[2e]: https://www.youtube.com/watch?v=kzfSfRqRS8c
[2f]: https://www.youtube.com/watch?v=Veigm-n7D00
[2g]: https://www.sheldonbrown.com/flats.html
[2h]: https://www.sheldonbrown.com/remove-tube.html
[2i]: https://www.sheldonbrown.com/tires.html
[2j]: https://www.sheldonbrown.com/inner-tubes.html
[2k]: https://www.sheldonbrown.com/tire-sizing.html
[2l]: https://www.sheldonbrown.com/tire-rotation.html
[2m]: https://www.sheldonbrown.com/tire-tools.html
[2n]: https://www.sheldonbrown.com/cribsheet-tires.html
[2o]: https://www.sheldonbrown.com/tooltips/truing.html
[2p]: https://www.sheldonbrown.com/wheelbuild.html
[2q]: https://www.sheldonbrown.com/brandt/patching.html
[2r]: https://www.sheldonbrown.com/tubeless.html
[2s]: https://www.sheldonbrown.com/rim-sizing.html
[2t]: https://www.sheldonbrown.com/spoke-length.html

[3]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[3a]: https://www.youtube.com/watch?v=Ss6Yq9Hu8m0
[3b]: https://www.youtube.com/watch?v=QU575Xpy8kA
[3c]: https://www.youtube.com/watch?v=UqDB3f7Uiuw
[3d]: https://www.youtube.com/watch?v=7N234t9hZg0
[3e]: https://www.youtube.com/watch?v=bfl8bC9ywNI
[3f]: https://www.youtube.com/watch?v=2kSFGHOUiNo
[3g]: https://www.youtube.com/watch?v=p6Ri6S_I6io
[3h]: https://www.youtube.com/watch?v=aqJqeuakiuk
[3i]: https://www.youtube.com/watch?v=rJ1iey8cksE
[3j]: https://www.youtube.com/watch?v=T8qyjpxLMkQ
[3k]: https://www.youtube.com/watch?v=PeMIGswqgD0
[3l]: https://www.youtube.com/watch?v=y7_ZnVmPhO4
[3m]: https://www.youtube.com/watch?v=H1BEyVJ_12k
[3n]: https://www.youtube.com/watch?v=-31uf0_wAv8
[3o]: https://www.youtube.com/watch?v=2aKvBMYXp3Q
[3p]: https://www.youtube.com/watch?v=91w2ZkHY5Vc
[3r]: https://www.youtube.com/watch?v=uEoCPfKpurg
[3s]: https://www.youtube.com/watch?v=exq5pZSYuC0
[3t]: https://www.youtube.com/watch?v=bh_FIhlgYUo
[3u]: https://www.youtube.com/watch?v=sVWuNfzT8Mg
[3v]: https://www.youtube.com/watch?v=3kuWgeWrlIY
[3w]: https://www.youtube.com/watch?v=NSO_qd1RFlc
[3x]: https://www.youtube.com/watch?v=jLXhjgkspGc
[3y]: https://www.youtube.com/watch?v=Zes7Ud6RT4w

[4]: https://bike.bikegremlin.com/post-list-by-category/

[4a]: https://bike.bikegremlin.com/14694/bicycle-tightening-torques/
[4b]: https://bike.bikegremlin.com/56/bicycle-tyre-tubes
[4c]: https://bike.bikegremlin.com/889/how-to-remove-a-wheel/
[4d]: https://bike.bikegremlin.com/1032/patching-a-tube/
[4e]: https://bike.bikegremlin.com/1017/removing-a-tyre-and-tube/
[4f]: https://bike.bikegremlin.com/1054/mounting-bicycle-tube-and-tyre/
[4h]: https://bike.bikegremlin.com/799/pressure-i-inflate-bicycle-tyres/
[4i]: https://www.youtube.com/watch?v=wwhh0BmF4sQ
[4j]: https://www.youtube.com/watch?v=aX11CmCOdsU
[4k]: https://www.youtube.com/watch?v=i1IiH5ihGFg
[4l]: https://bike.bikegremlin.com/380/how-many-spokes/
[4m]: https://bike.bikegremlin.com/245/bicycle-rim-tape-explained/
[4n]: https://www.youtube.com/watch?v=WnARbR8nO4M
[4o]: https://www.youtube.com/watch?v=MT9dvN928Jo
[4p]: https://www.youtube.com/watch?v=cvcrU3TKAC4
[4q]: https://www.youtube.com/watch?v=ETnEOCWaGCo

[4aa]: https://bike.bikegremlin.com/category/bearings-brakes-wheels/tyres-and-tubes/
[4ab]: https://bike.bikegremlin.com/category/bearings-brakes-wheels/wheels/
[4ac]: https://www.youtube.com/playlist?list=PLlbCLBTvuIstsl8a9sC02j06nriuCqVRO
[4ad]: https://www.youtube.com/playlist?list=PLlbCLBTvuIssCHYrqh9BM8IrtEV2SaD_3

[5]: https://www.bikeride.com/guide/

[5a]: https://www.bikeride.com/torque-specifications/

[7a]: https://www.youtube.com/watch?v=tX6VpXTT-qg
[7b]: https://www.youtube.com/watch?v=i_2NTHjwSzk
[7c]: https://www.youtube.com/watch?v=95RLSzB4GH0
[7d]: https://www.youtube.com/watch?v=A1vRAk1VJe0
[7e]: https://www.youtube.com/watch?v=FZ44ekC-xus
[7f]: https://www.youtube.com/watch?v=1U674WmrgUo
[7g]: https://www.youtube.com/watch?v=W1ikuRf9aOo

[8a]: https://www.youtube.com/watch?v=hP-tRvXaYKE
[8b]: https://www.youtube.com/watch?v=_CkLUaL4Xzo

[9a]: https://www.dedhambike.com/articles/torque-table-pg186.htm

[10a]: https://si.shimano.com/en/pdfs/dm/CASG004/DM-CASG004-03-ENG.pdf
[10b]: https://si.shimano.com/en/pdfs/dm/SG0004/DM-SG0004-09-ENG.pdf
[10c]: https://si.shimano.com/en/pdfs/si/2ZS0C/SI-2ZS0C-001-ENG.pdf

[12a]: https://road.cc/content/feature/how-fit-rim-tape-get-it-right-expert-guide-170363
