---
title: More templates and themes for GitLab Pages
date: 2021-08-05
tags: ["website", "GitLab Pages", "Static Site Generator", "Jekyll"]
---

The previous blog post showed the [builtin website templates][builtin] for
[GitLab Pages][1] as unpersuasive, except for the [Hugo Blog Theme][14].

What other templates or themes are available for _GitLab Pages_?

Given the numerous website templates for [Static Site Generators (SSGs)][6],
what does it take to deploy an _external_ theme in _GitLab Pages_?
... <!--more-->

### More templates for _GitLab Pages_

The _GitLab_ website lists a variety of projects with support for _Pages_,
which can be dowloaded, cloned, or forked:
- a couple of [miscellaneous themes & templates][60],
- a large number of [example websites][61] using various SSGs.

### External templates or themes for _SSGs_

There are plenty of sources of free or premium website templates, also called
_themes_.

For example, just for [Jekyll][11], the _galleries_ listed there add up to
hundreds of themes: \
https://github.com/topics/jekyll-theme \
https://jamstackthemes.dev \
https://jekyllthemes.io \
https://jekyll-themes.com \
http://jekyllthemes.org

Incidentally, the current count of SSGs out there stands at [333][62].

### Integrate an external theme with _GitLab Pages_?

To deploy an external template in _GitLab Pages_ one needs to define a
[CI/CD][8] pipeline to create the _Pages_ site.  This is done in form of the
file `.gitlab-ci.yml` having a `pages` section.

Since GitLab provides code examples for many SSGs, one can either
- fork and adapt a [sample project][63] and add the theme and configuration
  files, or
- copy a suitable [.gitlab-ci.yml template][64] to an existing project and
  adapt it there.
  
So far, I have not tried either approach.

[builtin]: /img/2021-08-04-gitlab-pages-builtin-templates.jpg

[1]: https://docs.gitlab.com/ee/user/project/pages/

[6]: https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/

[8]: https://docs.gitlab.com/ee/ci/

[11]: https://jekyllrb.com/

[12]: https://www.ruby-lang.org/en/

[14]: https://pages.gitlab.io/hugo/

[60]: https://gitlab.com/themes-templates

[61]: https://gitlab.com/pages

[62]: https://jamstack.org/generators/

[63]: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html

[64]: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html
