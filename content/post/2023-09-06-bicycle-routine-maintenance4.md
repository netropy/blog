---
title: Checklists for bicycle maintenance (4)
subtitle: Brakes
date: 2023-09-06
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle brakes", "bicycle brake levers", "bicycle brake pads"]
---

Servicing the mechanical brakes of our bicycles used to feel a bit tricky, for example:
- This mechanical disc brake has either excessive brake lever travel or pad rub.  Huh? \
  _(rotor is flexing, first recenter the brake by pulling cable tight, then adjust pad spacing)_
- Having just recentered that caliper brake, why is there now play in the brake arms? \
  _(rotating the pivot stud loosened the nuts, use 2 wrenches to rotate front+back at once)_
- The cantilever's spring tension screws are each at their limit.  What is the problem? \
  _(possible friction difference on bosses from dirt, clean with isopropyl alcohol, regrease)_

Occasionally, my prior ad-hoc approach ended in circles of adjusting brake
settings, for example: first adding cable slack, then removing it.  Happily,
my go-to expert resources [(PT)][1], [(SB)][2], [(BG)][4], [(CMA)][3], and
others (see [post][0a]), set me straight!

There's a challenge, though: The many _brake-servicing guides_ vary so much by
type and subtype of brake -- and by expert preference.  For example, [(PT)][1]
offers separate how-to guides for _side-pull vs. center-pull caliper_ brakes,
_smooth post vs. threaded post cantilever_ brakes etc., which not only differ
in details but also order of steps.  And these are just subtypes of mechanical
rim brakes -- not to speak of the scores of hydraulic disc brakes.

Hence, these checklists summarize my own practice, based on other guides: \
[4.1 identify brakes][r4.1] \
[4.2 check brakes][r4.2] \
[4.3 check and adjust brake levers][r4.3] \
[4.4 adjust rim brakes][r4.4] \
[4.5 adjust disc brakes][r4.5] \
[4.6 replace brake pads/shoes][r4.6] \
[4.7 replace/true disc brake rotor][r4.7] \
[4.8 service drum brakes][r4.8] \
[4.9 replace brake cables][r4.9] \
[4.10 bleed hydraulic brakes][r4.10] \
[4.11 resources, playlists][r4.11]
... <!--more-->

[r4.1]: #41-identify-brakes
[r4.2]: #42-check-brakes
[r4.3]: #43-check-and-adjust-brake-levers
[r4.4]: #44-adjust-rim-brakes
[r4.5]: #45-adjust-disc-brakes
[r4.6]: #46-replace-brake-pads
[r4.7]: #47-replace-true-disc-brake-rotor
[r4.8]: #48-service-drum-brakes
[r4.9]: #49-replace-brake-cables
[r4.10]: #410-bleed-hydraulic-brakes
[r4.11]: #411-resources-playlists

For _hydraulic_ disc brakes, the checklists below only cover their maintenance
on a generic level (no model- or make-specific details).

Hydraulics not just come with a list of _dos_ but also _don'ts_ -- so far, I
have: \
\- Don't stand bike upside down. \
\- Don't pull brake lever while wheel's out of the bike. \
\- Don't reset the pistons without prior venting of the cylinder. \
\- Don't mix or contaminate hydraulic fluids. \
\- Never skip reading the manufacturer's instructions for the specific brake
model...

Links to the _resources/videos_ on [(SB)][2], [(PT)][1], [(BR)][5], [(BG)][4],
[(CMA)][3], see [post][0a], given in each task.

_Note:_ I update these checklists _in-place_ for improvements and
clarifications; also, some tasks still need to be written up.  For comments:
martin dot zaun at gmx dot com

### 4.1 Identify brakes

Summary: The main types of brakes and actuation mechanisms \
What is used as braking surface? \
\- _rim brakes:_ rubber brake shoes press against the wheel's rim \
\- _disc brakes:_ brake pads of various compounds press against a rotor \
\- _drum brakes:_ metal brake shoes press against the inside of a drum \
For rim brakes, how are they mounted? \
\- _caliper brakes:_ on a centerbolt above wheel \
\- _cantilever brakes:_ on fittings (brazed-on pivots) below rim \
\- _other direct mount brakes:_ on fittings (brazed-on pivots) above rim \
How is the brake actuated? \
\- _mechanical brakes:_ by lever via cables, a wire inside a cable housing \
\- _hydraulic brakes:_ by lever via hydraulic lines, a fluid-filled tubing \
\- _coaster brakes:_ by back-pedaling via the chain

Details (with pros & cons):
| | |
|:--:|:--|
| (1) | _rim brakes:_ braking force is applied by friction pads to the rim of the rotating wheel, ideal for road bikes |
| | pros: lightweight, large heat-dissipating area, low stress on frame/fork/wheel, mechanically simple, easy to maintain |
| | cons: performs poorly when wet, affected by rim damage or out-of-true, rim wear from sand or mud, risk of tire damage |
| | - _side-pull brakes:_ the cable housing attaches to one brake arm (push), the inner wire to the other arm (pull) |
| | - _center-pull brakes:_ the housing ends in a cable stop, the inner wire pulls on saddle/stradle/link cable joining both arms |
| | - _1st-class lever design:_ the arm pivots above the rim, brake shoe is below pivot, opposite side as cable attachment |
| | - _2nd-class lever design:_ the arm pivots below the rim, brake shoe is above pivot, same side as cable attachment |
| | |
| (1.1) | _caliper brakes:_ a single assembly attached to the fork/frame by a centerbolt above the wheel (= 1st-class lever) |
| | - _single-pivot side-pull calipers:_ both arms pivot around the centerbolt |
| | - _dual-pivot side-pull calipers:_ each arm has a separate pivot, one arm at the centre, the other at the side |
| | - _dual-pivot center-pull calipers:_ each arm rotates around a pivot on its side of the wheel above the rim |
| | mounting: hole in fork crown or brake bridge on seat stays, centerbolt with hex nut or cylindrical nut (recessed mounting) |
| | name origin: a caliper is an instrument for measuring external or internal dimensions with moving parts that come together |
| | |
| (1.2) | _cantilever brakes:_ each arm pivots on a stud/boss brazed onto the fork/frame below rim (= dual-pivot, 2nd-class lever) |
| | - _traditional cantilevers:_ center-pull, cable: straddle carrier vs link unit, arms angled 90° vs 45° (high vs low profile) |
| | - _V-brakes_ &reg; or _direct/linear-pull brakes:_ side-pull, a tube with a bend ("noodle") is seated in a stirrup/cable bridge |
| | mounting: V-brakes and traditional cantilevers mount on the same fork/frame bosses (clearance and cable stop may differ) |
| | name origin: a cantilever is a rigid structural element (beam) that is fixed at one end and free (unsupported) at the other |
| | |
| (1.3) | _other (dual-pivot) direct-mount brakes:_ arms pivot on individual studs/bosses located above rim (= 1st-class lever) |
| | - _U-brakes:_ side-pull or center-pull, like a caliper brake only with the pivots mounted directly to the fork/frame |
| | - _roller-cam brakes:_ center-pull, the cable pulls a triangular sliding cam pushing on rollers attached to the arms |
| | - _direct-mount brakes:_ side-pull, fork/frame has threaded holes/bosses for the pivot bolts plus cut-outs for pad clearance |
| | mounting: U-brakes and roller-cam brakes use the same type and placement of studs (clearance and cable stop may differ) |
| | mounting: for direct mount brakes, spacing and threading of bosses differ across manufacturers (frame and brake must agree) |
| | |
| (2) | _hub brakes:_ braking force is applied to the wheel's hub, either incorporated into the hub or externally attached to it |
| | pros: unaffected by rain/snow/mud, unaffected by rim damage or out-of-true, no rim wear, no risk of overheating tire |
| | cons: can get very hot, brake effectiveness varies, heavier than rim brakes, high stress/torque on fork |
| | cons: incompatible with quick-release axle, front disc brake can loosen QR lever, most drum brakes don't support QR axle |
| | |
| (2.1) | _drum brakes:_ built into or attached to the wheel hub, shoes press against the inside of a cylindrical drum |
| | - _coaster_ or _back-pedal brakes:_ foot-operated drum brake integrated into rear hub, can overheat quickly |
| | - _roller brake_ &reg;: cable-operated drum brake, braking surfaces are metallic, prone to overheating |
| | |
| (2.2) | _disc brakes:_ a frame/fork-attached caliper squeezes on a hub-attached disc rotor, ideal for mountain bikes |
| | pros vs drum brakes: highly effective, good heat dissipation, easy wheel removal, no wheel replacement when drum worn out |
| | cons vs drum brakes: risk of damage to rotor, risk of injury from rotor, higher maintenance frequency |
| | subtypes:
| | - actuation: _mechanical_ vs _hydraulic_, see below
| | - number of pistons: _single piston/actuation_ (1 pad moves) vs _dual actuation_ (2 pads move) vs 4-piston calipers (2/side) |
| | - caliper mounting standards: _IS_ mount (International Standard, tabs) vs _post-mount_ (PM, threaded posts) vs other |
| | - disc mounting standards: _six-bolt mount_ (as with _IS_) vs _Center Lock_ &reg; (splined interface with lockring) vs other |
| | - disc sizes: _160mm, 185mm, 203mm_ diameter vs other, can have larger/smaller rotor on front/rear wheel |
| | mounting: various adapters available for caliper and disc mounts, some incompatible frame/hub/brake combinations |
| | |
| (3) | _actuation mechanisms and brake levers:_ |
| | |
| (3.1) | _mechanical, cable:_ brake levers transmit force via cable to brake, the cable housing/inner wire exert push/pull force |
| | pros: low maintenance, reliable, easy to service/repair/replace, inexpensive, durable (years) |
| | cons: cable friction can make the brake feel "sticky", depends on length, number/angles of bends, age of cables |
| | - brake lever pull types: _long pull_ for (most) V-brakes and mechanical disc brakes vs _short/standard pull_ for all other |
| | |
| (3.2) | _hydraulic:_ brake levers push a fluid down the hose to calipers where pistons move brake pads to grip the disc rotor |
| | pros: reliable, frictionless, smooth modulation of braking force, self-adjusts pad clearance, long-lasting (years) |
| | cons: air bubbles can make the brake feel "spongy", must periodically bleed brake to remove air or replace hydraulic fluid |
| | cons: non-trivial to service/repair, brake bleeding requires model-specific instructions and special tools (bleed kits) |
| | cons: limited compatibility between brake levers/lines/calipers of different models/manufacturers (piston diameter etc) |
| | cons: the self-adjustment of pad clearance lacks any feedback on pad wear, must regularly inspect to avoid rotor/piston damage |
| | cons: must avoid contamination of hydraulic fluid, cannot mix different types, recommended to use separate bleed kits |
| | cons: hydraulic fluids are toxic and must be disposed as hazardous waste (according to local regulation) |
| | - hydraulic fluid: _DOT brake fluid_ (SRAM, Formula, Giant et al) vs _mineral oil_ (Shimano, Tektro/TRP, Margura, Giant et al) |
| | |
| (3.3) | other actuation mechanisms: _hybrid_ cable-hydraulic, mechanical _back-pedal_ |
| | |
| (3.4) | brake levers: |
| | main types: _flat bar levers_ vs _drop bar levers;_ also _dual control levers,_ which provide for both braking and shifting |

Resources: \
[(SB) brake choices][2d];
[(SB) cables][2e];
[(WP) bicycle brake][6a];
[(BG) mechanical brakes/levers][4g];

Resources: \
[(SB) rim brakes][2f];
[(SB) caliper brakes][2g];
[(SB) cantilever brakes][2k];
[(SB) cantilevers: geometry][2h];
[(SB) cantilevers: center-pull][2i];
[(SB) V-brakes][2j];
[(SB) U-brakes][2a];
[(SB) roller-cam brakes][2b];
[(PT) identify rim brakes][1aa];
[(BG) mechanical brakes/levers][4h];
[direct mount brakes][14];
[direct mount brakes][15a];

Resources: \
[(SB) disc brakes][2l];
[(BG) disc/rim brakes][4i];
[mounting standards][16];
[mounting standards][17];
[mounting adapters][18a];

Resources: \
[(SB) drum brakes][2m];
[(SB) coaster brakes][2n];
[(SB) roller brakes][2o];
[(BG) drum brakes][4k];

Videos: \
[(PT) identify rim brakes][1ab];
[(CMA) identify brakes][3u];
[(CMA) bleed hydraulic brakes][3f];
[(BG) modern caliper brakes are unsafe][4f];
[compatibility disc brake mount types][11a];

### 4.2 Check brakes

Goals _for rim/disc brakes:_ \
\- For short brake lever travel, the pads sit as near as possible to the
rim/rotor without rubbing. \
\- For ample stopping power, both pads make full contact with the rim/rotor
with roughly equal force and at the same time. \
\- For good fine control and smooth operation, the pads are not worn down, the
rim/rotor is true, the brake has no play and retracts freely, and the brake
cable has almost no slack or friction.

| | |
|:-:|:--|
| (1) | check for proper installation and basic safety: |
| | wheel is centered in fork/frame and axle is properly tightened, see [1.2][r1.2] |
| | wheel has no hub play, see [r2.6][2.6] |
| | inspect levers, cables/lines, mounts, brakes, rims/rotors for damage or loose parts |
| | squeeze lever with max force, hold, check for cable slippage/hydraulic leakage |
| | _for center-pull brakes:_ |
| | - cable carrier or link unit is centered, has 2cm (0.8") or more clearance to cable stop |
| | - _for straddle carrier:_ cable cannot catch on the tire (stopped by fender etc) |
| | - _for link unit:_ cable ligns up with guide line on button, flexible pipe touches brake arm |
| | _for drum brakes:_ |
| | reaction arm is properly attached to fork/frame by metal strap or braze-on |
| | |
| (2) | optional: inspect shoe/pad/rim/rotor wear |
| | _for rim brakes:_ |
| | - replace pads when worn to the bottom of the deepest grooves, see [4.6][r4.6] |
| | - replace aluminum rims when thickness reaches 1mm or wear indicator disappears (groove, hole) |
| | - replace carbon fibre rims when braking surface discolored or fibrous (resin layer worn away) |
| | - true or replace wheel if warped/bent |
| | _for disc brakes:_ |
| | - remove wheel, see [1.1][r1.1], remove pads from caliper and inspect, see [4.6][r4.6] |
| | - measure pad/rotor thickness with a micrometer or a vernier caliper tool (imprecise) |
| | - replace pads when thickness reaches 0.5mm (or -1.5mm from new) or as recommended, see [4.6][r4.6] |
| | - replace rotor when thickness reaches 1.5mm (or -0.3mm from new) or as recommended, see [4.7][r4.7] |
| | - true or replace rotor if warped/bent |
| | _for drum brakes:_ |
| | - must disassemble unit/hub to inspect/grease the internal brake shoes, see [4.8][r4.8] |
| | - must replace unit/hub when performance drops or brake cable can no longer be adjusted |
| | |
| (3) | check shoe/pad alignment: |
| | _for rim brakes:_ pull brake, inspect... |
| | - vertical height: pad makes full contact, stays within 1mm of rim's braking surface |
| | - vertical face/roll angle: pad makes full contact with rim's braking surface |
| | - tangent/pitch angle: level to rim, curved pads follow rim's curvature |
| | - toe/yaw angle: either parallel to rim or slight toe-in (pad's front edge touches rim first) |
| | - extension: pads contact rim when brake arms are roughly vertical (for mechanical advantage) |
| | - centering: symmetrical with exception, see (4) |
| | - orientation: longer side of asymmetrical brake shoes/pads points rearwards |
| | _for disc brakes:_ pull brake, inspect against white background (paper held behind brake)... |
| | - face/toe: parallel to rotor, pads make full contact |
| | - centering: symmetrical with exception, see (4) |
| | |
| (4) | check shoe/pad centering and clearance: |
| | pull & release brake, inspect...
| | brake lever travel at "bite point": is as preferred? has clearance to grip? see [4.3][r4.3] |
| | |
| | shoe/pad clearance: |
| | _for rim brakes:_ 1 .. 2mm pad clearance from rim, see [4.4][r4.4] |
| | _for disc brakes:_ 0.2 .. 0.5mm pad clearance from rotor, see [4.5][r4.5] |
| | |
| | centering: do both pads contact the rim/rotor at the same time? |
| | _for rim brakes:_ important except for _side-pull caliper brakes,_ see [4.4][r4.4] |
| | _for disc brakes:_ important except for _single piston/actuation brakes,_ see [4.5][r4.5] |
| | |
| (5) | check brake operation, noises, feel: |
| | spin wheel, pull brake, check for... |
| | trueness: |
| | - wheel is true, see [1.5][r1] |
| | - rotor is true, see [4.7][r4.7] |
| | noises: chattering/scraping while not braking |
| | - pad rubbing or brake not centered, see [4.4][r4.4], [4.5][r4.5] |
| | - wheel/rotor out of true, see [1.5][r1], [4.7][r4.7] |
| | noises: squeaking/squealing/scraping while braking |
| | - worn/dirty pads/shoes/rim/rotor/drum, see step (2) |
| | - misaligned pads (adjust toe-in, recenter), see [4.4][r4.4], [4.5][r4.5] |
| | noises: rattling/clunking |
| | - loose parts such as cables, spokes, pads/shoes, rotor, brake mounts, axle, hub, fork |
| | |
| | brake feel: |
| | - stiff, sticky: cable friction, tight caliper mount, fatigued spring, see [4.9][r4.9], [4.4][r4.4] |
| | - soft, spongy: cable slack, brake play, hydraulic needs bleeding, see [4.4][r4.4], [4.10][r4.10] |
| | |
| | brake play: |
| | rock the wheel back/forth while braking, check for play in brake (vs. fork/headset) |
| | push brake/arms back/forth, tighten caliper mounts as needed, see [4.4][r4.4], [4.5][r4.5] |
| |
| (6) | optional: ride bike, check each brake's performance |
| | stopping power as expected? pad clearance too large? worn shoes/pads/rim/rotor/cables? |
| | modulation, control? brake under-/over-responsive? mechanical advantage too large/small? |

Resources: \
[(SB) rim brakes][2f];
[(SB) caliper brakes][2g];
[(SB) cantilever brakes][2k];
[(SB) cantilevers: center-pull][2i];
[(SB) disc brakes][2l];
[(SB) creaks, clicks & clunks][2p];
[(BG) check disc brake pads][4l];
[(BG) check disc brake rotor][4m];
[check/adjust disc brakes][10a];
[check rim wear][12a];

### 4.3 Check and adjust brake levers

| | |
|:--:|:--|
| (1) | check brake levers: |
| | - best check from rider's preferred downhill/descent position (sit/stand) to avoid hand fatique |
| | - rider can lean against a wall or somebody holds the bike |
| | - _for upright bar levers:_ position is lateral (left/right) and angle is rotational (up/down) |
| | - _for drop bar levers:_ position is vertical (up/down) and angle is rotational (left/right) |
| | position: index/middle/ring/pinky fingers grip the curve of 1/2/3/4-finger blade in a straight line |
| | angle: lower arms, wrists, and stretched fingers form a straight line; wrist doesn't move to brake |
| | reach: first (outer) joint of index finger lies on the lever |
| | travel, bite point: by personal preference, but lever never touches grip or other fingers |
| | symmetry: both levers are adjusted to same settings |
| | |
| (2) | adjust brake levers position/angle: |
| | _for carbon bars or plastic body levers:_ tighten just enough so that lever doesn't easily move |
| | _for upright bar levers:_ tighten to 3..7 Nm or recommended torque |
| | _for drop bar levers:_ tighten to 6..10 Nm or recommended torque |
| | |
| (3) | assignment left/right brake lever to front/rear brake: typically... |
| | _for E.U. + countries with left-hand traffic:_ right lever actuates the front brake |
| | _for U.S. + countries with right-hand traffic:_ right lever actuates the rear brake |
| | _by preference:_ the front brake is most effective and should be used alone in most situations |

Resources: \
[(PT) adjust brake lever: upright bar][1e];
[(PT) adjust brake lever: drop bar][1g];
[(PT) adjust in-line brake lever][1ag];
[adjust brake levers][19];
[(SB) cables][2e];
[(SB) braking & turning][2c];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) adjust brake lever: upright bar][1f];
[(PT) adjust brake lever: drop bar][1h];
[(CMA) adjust brake lever: flat bar][3g];
[(CMA) adjust brake lever: drop bar][3h];
[(CMA) adjust brake lever reach][3i];
[adjust reach/preload on Shimano hydraulic brake levers][13];

### 4.4 Adjust rim brakes

This checklist combines the (PT) procedures and "Shimano technique" per (SB).

| | |
|:-:|:--|
| (1) | check wheel: |
| | wheel is centered in fork/frame and axle is properly tightened, see [1.2][r1.2] |
| | wheel has no hub play, see [r2.6][2.6] |
| | wheel is true, see [r1.5][1.5] |
| | otherwise, bike is not safe or brake will be difficult/impossible to adjust |
| | |
| (2) | check what coarse adjustments are needed: |
| | pull brake lever firmly a few times to settle cable/brake |
| | do steps (3)..(5) if
| | - brake shoes/pads make less than full contact with rim |
| | - barrel adjuster(s) are set near limit |
| | - just replaced brake pads/shoes |
| | - brake squeals or makes noises |
| | - brake performance is poor |
| | otherwise: |
| | skip to step (7) if brake shoes/pads just don't meet rim at the same time |
| | skip to step (8) if brake has a bit of play or is sticky to retract arms |
| | skip to step (9) if brake lever travels too far or if shoes/pads rub rim |
| | |
| (3) | adjust brake shoes/pads for full contact with rim: |
| | |
| | loosen mounting nut of shoe/pad/fixing bolt, adjust: |
| | height: horizontal/level to rim |
| | extension: pads contact rim when caliper arms are roughly vertical/parallel |
| | - _for smooth stud/post pads:_ slide stud in/out |
| | - _for threaded stud/post pads:_ swap spherical washers |
| | - _for flat-mounted/flat-washer pads:_ reposition washers/spacers |
| | snug mounting nut |
| | |
| | loosen mounting nut a bit, hold shoe/pad against rim, fine-adjust: |
| | face: parallel to rim for full contact |
| | tangent: level to rim, curved pads follow rim's curvature |
| | snug mounting nut |
| | |
| | hold shoe/pad firmly against rim, tighten mounting nut |
| | - _for smooth stud/post pads:_ to 7..9 Nm or recommended torque |
| | - _for threaded stud/post pads:_ to 6..8 Nm or recommended torque |
| | - _for flat-mounted/flat-washer pads:_ to 5..7 Nm or recommended torque |
| | if shoe/pad twists while tightening, loosen and regrease the mounting nut |
| | |
| | adjust both brake sides symmetrically, ignore centering of arms for now |
| | as needed, quick-release brake to allow caliper arms to move more freely |
| | |
| (4) | optional: adjust toe-in to reduce squealing |
| | _for adjustable stud/spherical washers:_ |
| | place a rubber band as shim at back of pad |
| | re-adjust shoes/pads per step (3) |
| | _for flat-washer/flat-mount shoes:_ |
| | slightly bend arms with a plier, careful with aluminum arms |
| | check that arms are aligned symmetrically |
| | |
| (5) | optional: fine-tune shoe/pad height to anticipate wear |
| | _for center-pull calipers:_ low on rim's braking surface |
| | _for side-pull calipers:_ high on rim |
| | _for dual-pivot calipers:_ right/left pad low/high on rim as seen from front |
| | _for U-brakes:_ low on rim |
| | _for cantilevers including V-brakes:_ high on rim |
| | shift shoes/pads a little and re-adjust per step (3) |
| | |
| (6) | adjust cable length and mechanical advantage: |
| | |
| | pull brake lever firmly a few times to settle cable/brake |
| | set the barrel adjuster(s) at lever/brake to ~3mm (~2 turns) from fully closed |
| | |
| | squeeze both pads against rim, secure with a strap or cable tie |
| | check that brake arms are roughly parallel/vertical for best mechanical advantage; |
| | otherwise, re-adjust shoes/pads per step (3) |
| | |
| | _for center-pull brakes:_ check/adjust desired mechanical advantage |
| | higher/lower m.a. = pads push harder/softer and travel slower/faster |
| | yoke angle = angle of the transverse cable from the horizontal (= slope) |
| | yoke angle fixes m.a.: 60°~1.2 .. 45°~1.4 .. 40°~1.6 .. 34°~1.8 .. 30°~2.0 |
| | _for straddle carrier:_ |
| | widen/shorten transverse cable to move yoke or cable carrier lower/higher |
| | _for link unit:_ |
| | choose among fixed link wire length (S < A < B < D < C) |
| | ensure that flexible pipe touches brake arm, lock wire in button |
| | check that cable roughly ligns up with printed guide line on button |
| | |
| | loosen cable pinch bolt, pull cable tight without force, snug pinch bolt |
| | |
| | close down the barrel adjuster in full |
| | pull brake lever firmly a few times to settle cable/brake |
| | the shoes/pads should have cleared the rim, ignore centering |
| | otherwise, repeat step (6) with larger barrel adjuster distance |
| | |
| | remove any rubber band or shim from the brake shoe/pad |
| | |
| (7) | adjust brake/shoe/pad centering |
| | |
| | if centering is off per [4.2][r4.2]: |
| | _for caliper brakes:_ rotate pivot stud |
| | use 2 wrenches, rotate together front double-nut/spring/bridge and back mounting nut |
| | _for cantilever brakes:_ adjust spring tension |
| | - _for centering screw:_ tighten/loosen screw(s) to move arm away from/towards rim |
| | - _for Dia-Compe style:_ loosen mounting bolt, rotate spring block, tighten |
| | _for U-brakes:_ same/similar (?) as Dia-Compe style |
| | |
| | if centering remains off: |
| | _for caliper brakes:_ |
| | check brake play, see step (8)|
| | check if the spring has become fatigued |
| | _for cantilever brakes and U-brakes:_ |
| | check for friction difference on bosses, unhook cable, move each arm freely by hand |
| | remove brake arms, inspect bosses for dirt, clean with isopropyl alcohol, regrease |
| | anchor return spring in (middle) hole, select more tension for narrower rims |
| | reinstall brake arms |
| | |
| (8) | check/adjust brake play: |
| | |
| | pull brake lever firmly a few times to settle cable/brake |
| | rock wheel back/forth while braking, push/pull brake arms in opposite direction |
| | check that brake has no play while brake arms fully open/retract after a squeeze |
| | |
| | _for caliper brakes:_ |
| | hold adjusting nut with thin wrench and loosen locknut with other wrench, |
| | take note of position of adjusting nut and slightly tighten/loosen |
| | hold adjusting nut and secure locknut to 8..9 Nm or recommended torque |
| | if a good setting is not possible, check if the spring has become fatigued |
| | |
| | _for cantilever brakes and U-brakes:_ |
| | these arms tend to flex/have play in the pivots, causes squealing in shoes/pads |
| | to reduce squealing: adjust for toe-in, use soft rubber pads (has cons) |
| | tighten caliper arm's mounting bolts to 5..8..11 Nm or recommended torque |
| | |
| (9) | adjust brake lever travel and shoe/pad clearance: |
| | close/open the barrel adjuster(s) so that |
| | - pad clearance is sufficient to avoid pad rub, see [4.2][r4.2] |
| | - brake lever travel is to rider's preference and within limit range, see [4.3][r4.3] |
| | tighten the barrel adjuster lockring(s) against brake lever/cable stop |
| | |
| (10) | final checks: |
| | pull brake lever a few times with firm pressure |
| | check brake operation, noises, feel, see [4.2][r4.2] |

Resources: \
[(SB) adjust rim brakes][2f];
[(SB) caliper brakes][2g];
[(PT) adjust side-pull calipers][1i];
[(PT) adjust center-pull calipers][1k];
[(PT) adjust U-brakes][1y];
[(SB) cantilever brakes, direct-mount brakes][2k];
[(SB) adjust V-brakes][2j];
[(SB) adjust cantilevers][2i];
[(PT) adjust cantilevers: smooth post][1a];
[(PT) adjust cantilevers: threaded post][1c];
[(BG) adjust cantilevers][4c];
[(BG) mechanical brakes/levers][4h];
[adjust cantilever brakes][20];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) adjust side-pull calipers][1j];
[(PT) adjust center-pull calipers][1l];
[(PT) adjust cantilevers: smooth post][1b];
[(PT) adjust cantilevers: threaded post][1d];
[(CMA) adjust rim brakes][3t];
[(CMA) adjust rim brakes: squeaking][3q];
[(CMA) adjust V-brake spring tension holes][3a];
[adjust V-brakes][7h];
[install/adjust caliper brakes][7j];
[(PT) adjust U-brakes][1z];

Other:
[(CMA) remove/service/install caliper brakes][3j];
[(CMA) install caliper brakes][3k];
[(BG) service road brake][4p];

### 4.5 Adjust disc brakes

This checklist is a bit simpler than the (PT) procedures.

| | |
|:-:|:--|
| | _for hydraulic brakes:_ avoid standing bike upside down (can cause air bubbles) |
| | |
| (1) | check wheel/rotor: |
| | wheel is centered in fork/frame and axle is properly tightened, see [1.2][r1.2] |
| | wheel has no hub play, see [r2.6][2.6] |
| | rotor is true, see [4.7][r4.7] |
| | otherwise, bike is not safe or brake will be difficult/impossible to adjust |
| | |
| (2) | check if coarse adjustments are needed: |
| | pull brake lever firmly a few times to settle cables/lines/pistons |
| | do step (3) if |
| | - pads are not fully parallel to rotor (hold a white paper card behind brake) |
| | - _for mechanical brakes:_ brake lever travel and barrel adjuster(s) are near min/max limit |
| | - _for hydraulic brakes:_ brake lever touches grip or feels spongy: bleed brake, see [4.10][r4.10] |
| | otherwise, skip to step (4) |
| | |
| (3) | recenter the brake caliper/body |
| | |
| | _for mechanical brakes:_ |
| | loosen cable pinch bolt, let cable slack but not slip through, snug cable pinch bolt |
| | loosen the caliper mounting bolts just enough for the brake body to float in the oval holes |
| | |
| | reset barrel adjuster(s) to midpoint, about ~5mm out (or ~3 turns) |
| | reset pad adjuster screw(s) to midpoint, about ~1mm out (or ~1 turn) |
| | |
| | recenter: pull the cable tight until pads lock against rotor, snug cable pinch bolt |
| | tighten the caliper mounting bolts enough to prevent slippage |
| | |
| | _for hydraulic brakes:_ |
| | loosen the caliper mounting bolts just enough for the brake body to float in the oval holes |
| | recenter: pull brake and hold, this locks the pads against rotor |
| | tighten the caliper mounting bolts enough to prevent slippage, release the brake lever |
| | |
| (4) | adjust pad spacing: |
| | |
| | _for mechanical brakes:_ |
| | as needed, tighten pad adjuster screw(s) until pads touch rotor |
| | close down barrel adjuster(s) in full against brake lever/cable stop |
| | |
| | while there's pad rub: back off pad adjuster screw(s), pull brake lever firmly |
| | until there's pad rub: open barrel adjusters at brake/lever, pull brake lever firmly |
| | |
| | check that brake lever travel is as preferred and has enough clearance to grip |
| | otherwise, add/remove cable slack per step (3) |
| | |
| | tighten cable pinch bolt to 5..7 Nm |
| | tighten the barrel adjuster lockring(s) against brake lever/cable stop |
| | |
| | _for hydraulic brakes:_ |
| | pad spacing is typically self-adjusting, though some models have manual controls |
| | |
| (5) | final checks: |
| | pull brake lever a few times with firm pressure |
| | |
| | check that pads aligned parallel to rotor (hold a paper card behind brake); |
| | otherwise, loosen one mounting bolt, move brake slightly, snug bolt, re-check |
| | tighten the caliper mounting bolts to 6..7 Nm or recommended torque |
| | |
| | check brake operation, noises, feel, see [4.2][r4.2] |

Resources: \
[(SB) adjust disc brakes][2l];
[(PT) adjust mechanical disc brakes][1o];
[(PT) adjust hydraulical disc brakes][1q];
[(BG) adjust disc brake: rubbing][4d];
[(BG) adjust disc brake: squealing][4e];
[(BG) adjust/recenter disc brake][4q];
[check/adjust disc brakes][10a];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[adjust disc brakes][7i];
[(PT) adjust mechanical disc brakes][1p];
[(PT) adjust hydraulical disc brakes][1r];
[(CMA) adjust disc brakes][3w];
[(CMA) adjust disc brakes: squeaking][3r];

### 4.6 Replace brake pads/shoes

Shoes/pads for rim/disc brakes come in a variety of types and materials, see:
[(PT) rim][1s], [(SB) rim][2f], [(PT) disc][1m], [disc][15b], [disc][18b].

_for rim brakes:_
| | |
|:--:|:--|
| | take note of alignment adjustments: height, washers, stud extension etc |
| | loosen mounting nut of shoe/pad/fixing bolt |
| | |
| | replace rubber shoes/pads (remove wheel if not enough space, see [1.1][r1.1]) |
| | _for directional shoes:_ observe markings, longer side points rearwards |
| | |
| | roughly adjust alignment as before, snug bolts/nuts |
| | adjust pad alignment/clearance, see [4.4][r4.4] |

_for disc brakes:_
| | |
|:--:|:--|
| | _for hydraulic brakes:_ avoid standing bike upside down (may have to bleed brake) |
| | check brake/rotor spec on supported pad type: resin, metallic, semi-metallic, ceramic |
| | |
| (1) | remove wheel, see [1.1][r1.1] |
| | _for hydraulic brakes:_ avoid actuating brake lever (may have to bleed brake) |
| | |
| (2) |	replace pads/shoes: |
| | |
| | _for mechanical brakes:_ |
| | remove any pad-retaining pin/clip, remove pads |
| | install pads, secure with pin/clip, as needed |
| | |
| | _for hydraulic brakes:_ |
| | check manufacturer's instructions, differences between brands and models |
| | |
| | check if need to vent master cylinder to prevent over-pressure damage to seals: |
| | - remove bleed port on the brake lever |
| | - install bleed funnel filled with a few milliliters of new brake fluid |
| | reset pistons: fully push back into the bore of the caliper (using a piston tool?) |
| | be careful not to damage ceramic pistons (Shimano) |
| | |
| | remove any pad-retaining pin/clip, remove pads |
| | clean the brake caliper and rotor depending on type of brake fluid: |
| | - isopropyl alcohol for mineral oil |
| | - 1:1 mix water:isopropyl alcohol for DOT fluid |
| | lubricate seals with brake fluid, remove all excess fluid |
| | |
| | install pads, secure with pin/clip, as needed |
| | |
| (3) | install wheel, see [1.2][r1.2] |
| | |
| (4) | adjust/recenter brake, see [4.5][r4.5] |

Resources: \
[(PT) remove/install rim brake pads][1s];
[(PT) remove/install disc brake pads][1m];
[check/adjust disc brakes][10a];
[disc brake pads][15b];
[(SB) rim brakes][2f];
[(SB) disc brakes][2l];

Videos: \
[(PT) remove/install rim brakes pads][1t];
[(PT) remove/install disc brake pads][1n];
[(CMA) remove/install disc brake pads][3v];
[(CMA) remove/install rim brake pads][3s];
[(CMA) install disc brake pads with cooling fins][3b];
[(CMA) spread closed disc brake pads][3c];
[(BG) unstuck disc brake piston][4u];
[remove/service/install disc brake pads][8b];

Other: \
[(BG) remove/install road bike brake pads][4b];
[(BG) remove/install disc brake pads][4r];

### 4.7 Replace/true disc brake rotor

| | TODO: summarize |
|:--:|:--|
| | |
| | also replace brake pads when installing new rotor |

Resources: \
[(PT) remove/install disc brake rotor][1w];
[(PT) true disc brake rotor][1u];
[adjust disc brakes][10a];

Videos: \
[(PT) remove/install disc brake rotor][1x];
[(PT) true disc brake rotor][1v];
[(CMA) remove/install disc brake rotor][3p];
[(CMA) remove/install center lock disc rotor][3d];
[(CMA) install 6-bolt disc rotor][3e];
[(BG) unstuck disc brake piston][4u];
[(BG) remove/install disc brake rotor][4s];

Other: \
[(BG) remove/install disc brake rotor][4t];

### 4.8 Service drum brakes

| | |
|:-:|:--|
| | _for coaster brakes:_ |
| | occasional adjustment of the bearing cones, bearing maintenance like other hubs |
| | _for roller brakes:_  |
| | occasional cable adjustment to compensate brake shoe wear |
| | regular relubrication with special Nexus brake grease through small rubber access plug |

Resources: \
[(SB) drum brakes][2m];
[(SB) coaster brakes][2n];
[(SB) roller brakes][2o];

Videos: \
[(CMA) adjust roller brakes][3l];
[(CMA) adjust roller brakes: squeaking][3m];
[(CMA) remove/service/install roller brakes][3n];
[(CMA) lubricate roller brakes][3o];
[(BG) service roller brakes][4o];

### 4.9 Replace brake cables

| | TODO: summarize |
|:--:|:--|
| | |

Resources: \
[(SB) cables][2e];
[(PT) replace brake cable: upright bar][1ac];
[(PT) replace brake cable: drop bar][1ae];

Videos: \
[(PT) replace brake cable: upright bar][1ad];
[(PT) replace brake cable: drop bar][1af];
[(CMA) replace brake cable: flat bar and drop bar][3x];

### 4.10 Bleed hydraulic brakes

Complicated: manufacturer-/model-specific instructions and tools required. \
Here just links.

Resources: \
[(BG) disc brake fluids][4n];

Videos: \
[(PT) service disc brakes][1bb];
[(PT) bleed hydraulic brakes][1bc];
[(BG) service hydraulic brake][4x];
[(BG) service hydraulic brake][4y];
[(BG) bleed hydraulic brake][4v];
[(BG) bleed hydraulic brake][4w];

### 4.11 Resources, playlists

Lists: \
[(SB) brakes][2aa];
[(PT) rim brakes][1bd];
[(PT) rim brakes][1ba];
[(PT) disc brakes][1be];
[(PT) disc brakes][1bb];
[(PT) hydraulic brakes][1bc];
[(BG) brakes][4aa];
[(BG) mechanical brakes][4ab];
[(BG) hydraulic disc brakes][4ac];

[0a]: /post/2023-09-01-bicycle-resources/
[r0]: /post/2023-09-02-bicycle-routine-maintenance0.md
[r1]: /post/2023-09-03-bicycle-routine-maintenance1.md
[r2]: /post/2023-09-04-bicycle-routine-maintenance2.md
[r3]: /post/2023-09-05-bicycle-routine-maintenance3.md
[r4]: /post/2023-09-06-bicycle-routine-maintenance4.md

[1]: https://www.parktool.com/blog/repair-help

[1a]: https://www.parktool.com/en-us/blog/repair-help/cantilever-smooth-post-brake-service
[1b]: https://www.youtube.com/watch?v=G_tDym0G6zo
[1c]: https://www.parktool.com/en-us/blog/repair-help/cantilever-threaded-post-brake-service
[1d]: https://www.youtube.com/watch?v=FvFira2dAPY
[1e]: https://www.parktool.com/en-us/blog/repair-help/brake-lever-mounting-positioning-upright-bars
[1f]: https://www.youtube.com/watch?v=-ZJdqJDrs60
[1g]: https://www.parktool.com/en-us/blog/repair-help/brake-lever-mounting-positioning-drop-bars
[1h]: https://www.youtube.com/watch?v=geSok-YQN-U
[1i]: https://www.parktool.com/en-us/blog/repair-help/sidepull-brake-service
[1j]: https://www.youtube.com/watch?v=UOrIVc5lG-k
[1k]: https://www.parktool.com/en-us/blog/repair-help/center-pull-brake-service
[1l]: https://www.youtube.com/watch?v=iJeYSKUI-6A
[1m]: https://www.parktool.com/en-us/blog/repair-help/disc-brake-pad-removal-installation
[1n]: https://www.youtube.com/watch?v=Xqw0SaZl-jo
[1o]: https://www.parktool.com/en-us/blog/repair-help/mechanical-disc-brake-alignment
[1p]: https://www.youtube.com/watch?v=NmqGeLNcVIg
[1q]: https://www.parktool.com/en-us/blog/repair-help/hydraulic-disc-brake-alignment
[1r]: https://www.youtube.com/watch?v=uk_nC9anQcM
[1s]: https://www.parktool.com/en-us/blog/repair-help/brake-pad-replacement-rim-brakes
[1t]: https://www.youtube.com/watch?v=JpJrslWIUxM
[1u]: https://www.parktool.com/en-us/blog/repair-help/disc-brake-rotor-truing
[1v]: https://www.youtube.com/watch?v=O0c2Ez2v0PU
[1w]: https://www.parktool.com/en-us/blog/repair-help/disc-brake-rotor-removal-installation
[1x]: https://www.youtube.com/watch?v=SE9DJKXmIy8
[1y]: https://www.parktool.com/en-us/blog/repair-help/u-brake-service
[1z]: https://www.youtube.com/watch?v=YS1cW3I4OgQ

[1aa]: https://www.parktool.com/en-us/blog/repair-help/rim-brake-identification
[1ab]: https://www.youtube.com/watch?v=j6ANuieQR4w
[1ac]: https://www.parktool.com/en-us/blog/repair-help/brake-housing-cable-installation-upright-bars
[1ad]: https://www.youtube.com/watch?v=Xg5MrDgLhHI
[1ae]: https://www.parktool.com/en-us/blog/repair-help/brake-housing-cable-installation-drop-bars
[1af]: https://www.youtube.com/watch?v=oL82JM7H2Bw
[1ag]: https://www.parktool.com/en-us/blog/repair-help/in-line-brake-levers

[1ba]: https://www.youtube.com/playlist?list=PLGCTGpvdT04Re5yrxKOVg1yjuollLGDwZ
[1bb]: https://www.youtube.com/playlist?list=PLGCTGpvdT04TW0Qi4wpGxYB2TuzJok9l5
[1bc]: https://www.youtube.com/playlist?list=PLGCTGpvdT04Rdrk87M-iFCcDhEFytyajX
[1bd]: https://www.parktool.com/en-us/blog/repair-help?area%5B%5D=48
[1be]: https://www.parktool.com/en-us/blog/repair-help?area%5B%5D=153809

[2]: https://www.sheldonbrown.com

[2a]: https://www.sheldonbrown.com/canti-u.html
[2b]: https://www.sheldonbrown.com/canti-rollercam.html
[2c]: https://www.sheldonbrown.com/brakturn.html
[2d]: https://www.sheldonbrown.com/brake-choices.html
[2e]: https://www.sheldonbrown.com/cables.html
[2f]: https://www.sheldonbrown.com/rim-brakes.html
[2g]: https://www.sheldonbrown.com/calipers.html
[2h]: https://www.sheldonbrown.com/cantilever-geometry.html
[2i]: https://www.sheldonbrown.com/canti-trad.html
[2j]: https://www.sheldonbrown.com/canti-direct.html
[2k]: https://www.sheldonbrown.com/cantilever-adjustment.html
[2l]: https://www.sheldonbrown.com/disc-brakes.html
[2m]: https://www.sheldonbrown.com/drum-brakes.html
[2n]: https://www.sheldonbrown.com/coaster-brakes.html
[2o]: https://www.sheldonbrown.com/rollerbrakes.html
[2p]: https://www.sheldonbrown.com/creaks.html

[2aa]: https://www.sheldonbrown.com/brakes.html

[3]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[3a]: https://www.youtube.com/watch?v=S9m6iW70bG4
[3b]: https://www.youtube.com/watch?v=krN3EY_mqgI
[3c]: https://www.youtube.com/watch?v=dKibKsyAjHY
[3d]: https://www.youtube.com/watch?v=NVEaTSE3XSI
[3e]: https://www.youtube.com/watch?v=R2Y4_TvWNVc
[3f]: https://www.youtube.com/watch?v=HEYwI9rfiXI
[3g]: https://www.youtube.com/watch?v=NwZL_vSEah8
[3h]: https://www.youtube.com/watch?v=ZegEErSak2Q
[3i]: https://www.youtube.com/watch?v=r1qJkOM3nQw
[3j]: https://www.youtube.com/watch?v=FM2f15LWM94
[3k]: https://www.youtube.com/watch?v=E3gHDxGBUxI
[3l]: https://www.youtube.com/watch?v=ora98VEonow
[3m]: https://www.youtube.com/watch?v=5G9vVKihgtQ
[3n]: https://www.youtube.com/watch?v=Hm5PxA3pHrQ
[3o]: https://www.youtube.com/watch?v=4BZe8HXqJA4
[3p]: https://www.youtube.com/watch?v=FJiL92Su688
[3q]: https://www.youtube.com/watch?v=ciJvu96YhCQ
[3r]: https://www.youtube.com/watch?v=qQ_YIXhTGU0
[3s]: https://www.youtube.com/watch?v=fVSURVAH7lg
[3t]: https://www.youtube.com/watch?v=tqQHJ0WHYWs
[3u]: https://www.youtube.com/watch?v=mFwyeOqsV7w
[3v]: https://www.youtube.com/watch?v=3dhj5qmWh54
[3w]: https://www.youtube.com/watch?v=J8OsyIOovbA
[3x]: https://www.youtube.com/watch?v=zvD2F3JwQc4

[4]: https://bike.bikegremlin.com/post-list-by-category/

[4a]: https://bike.bikegremlin.com/14694/bicycle-tightening-torques/
[4b]: https://www.youtube.com/watch?v=MieUjheXp1o
[4c]: https://bike.bikegremlin.com/1739/cantilever-brake-adjustment/
[4d]: https://bike.bikegremlin.com/7975/disc-brake-rub/
[4e]: https://bike.bikegremlin.com/8037/disc-brake-squeal/
[4f]: https://www.youtube.com/watch?v=QyfZ1shfHP8
[4g]: https://bike.bikegremlin.com/1402/mechanical-bicycle-brake-compatibility/
[4h]: https://bike.bikegremlin.com/1419/bicycle-mechanical-brakes/
[4i]: https://bike.bikegremlin.com/3871/pros-and-cons-od-bicycle-disc-brakes-compared-to-rim-brakes/
[4k]: https://bike.bikegremlin.com/15501/drum-brake-pros-and-cons/
[4l]: https://bike.bikegremlin.com/20335/when-to-replace-bicycle-disc-brake-pads/
[4m]: https://bike.bikegremlin.com/20365/when-to-replace-bicycle-brake-discs-rotors/
[4n]: https://bike.bikegremlin.com/19089/which-fluid-should-i-put-in-my-hydraulic-bicycle-brakes/
[4o]: https://www.youtube.com/watch?v=KtEzy2dfgqU
[4p]: https://www.youtube.com/watch?v=-kEVawGxp0M
[4q]: https://www.youtube.com/watch?v=peyXkNV8gOY
[4r]: https://www.youtube.com/watch?v=KkF8K-1uAsA
[4s]: https://www.youtube.com/watch?v=t7OhWkoOsMU
[4t]: https://www.youtube.com/watch?v=47isRZPYhrs
[4u]: https://www.youtube.com/watch?v=K6mG6UjBh0Q
[4v]: https://www.youtube.com/watch?v=UrJWrlKZs_o
[4w]: https://www.youtube.com/watch?v=NQTIGq4xzZo
[4x]: https://www.youtube.com/watch?v=V4OXnzcPDA4
[4y]: https://www.youtube.com/watch?v=mrKiPETwVpM

[4aa]: https://bike.bikegremlin.com/category/bearings-brakes-wheels/brakes/
[4ab]: https://www.youtube.com/playlist?list=PLlbCLBTvuIss52yspMFdu5abRJGX4PVB0
[4ac]: https://www.youtube.com/playlist?list=PLlbCLBTvuIsu9UxQYUiPh5AxTTUw4dOR7

[5]: https://www.bikeride.com/guide/

[5a]: https://www.bikeride.com/torque-specifications/

[6a]: https://en.wikipedia.org/wiki/Bicycle_brake

[7h]: https://www.youtube.com/watch?v=I3wcX8t7lZw
[7i]: https://www.youtube.com/watch?v=Ym8mreUhelQ
[7j]: https://www.youtube.com/watch?v=A8wmmO-MRrI

[8b]: https://www.youtube.com/watch?v=UPMRtcKp6QI

[9a]: https://www.dedhambike.com/articles/torque-table-pg186.htm

[10a]: https://cyclingsavvy.org/2024/06/caring-for-bicycle-disc-brakes/

[11a]: https://www.youtube.com/watch?v=lG1OnZdTlVk

[12a]: https://road.cc/content/feature/how-do-you-tell-when-your-wheel-rims-worn-out-238960

[13]: https://www.youtube.com/watch?v=6FbNQGtlGSw

[14]: http://bikeretrogrouch.blogspot.com/2014/09/direct-mount-brakes.html

[15a]: https://www.bikeradar.com/features/why-direct-mount-brakes-are-awesome
[15b]: https://www.bikeradar.com/advice/buyers-guides/disc-brake-pads

[16]: https://bicycles.stackexchange.com/questions/44161/what-is-the-difference-between-i-s-post-and-flat-mount-disc-brake-mounting-st

[17]: https://www.qualisports.us/blogs/news/differences-between-post-mount-and-is-mount-on-a-bike

[18a]: https://thelostco.com/blogs/blog/mountain-bike-brake-adapters-explained
[18b]: https://thelostco.com/blogs/blog/mountain-bike-brake-pads-explained

[19]: https://enduro-mtb.com/en/how-to-set-up-your-brake-levers-perfectly/

[20]: https://blackmtncycles.com/get-the-most-out-of-your-canti-brake/
