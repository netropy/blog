---
title: Blending old & tech (7)
subtitle: Having upgraded my touring bicycle from 1993
date: 2021-08-15
tags: ["DIY", "bicycle", "bicycle conversion", "bicycle renovation", "bicycle touring", "bicycle commute", "bicycle internal-gear hub", "Shimano Alfine"]
---

In summary, this [D.I.Y.][1] bike upgrade project yielded good results:
1. A bike with a classic, timeless frame from 1993 and components from 2018.
1. After 3 years of use: the bike is reliable, commutes and touring are fun!
1. An internal-gear hub (8-speed Shimano Alfine) is great for commutes.
1. Having rebuilt my own bike, I can now do all maintenance by myself.
1. Learning about bicycle technology, old and new, turned out to be fun!

A couple of lessons in more detail
... <!--more-->

- If you have an old bike, make an inventory of its components and status.
- If you are comfortable with the frame's geometry, size, quality: keep it!
- 8-speed internal-gear hubs (IGHs) are perfect for commuter/city bikes.
- For bike-touring, an 11- or 14-speed IGH (Shimano, Rohloff) might be better.
- Bike stores chase market trends and focus on mountain, road, and e-bikes.
- Upgrading the entire drivetrain is not too hard: wheels, cranks, gears etc.
- Shopping for drivetrain components: buy quality, robustness, utility.
- Shopping for seatpost, handlebar, brakes, rack, fenders: buy value, design.
- Shopping for saddle, grips, pedals, shoes: must try them out (free returns).
- Some excellent bike tech resources and how-to videos available on the web.

![Torpedo Stratos][angle]

Last word: Communities that are walkable and bike-friendly have such a
superior quality of life!  Who actually wants to commute by car when there's a
safe bike path inviting to a nice morning/evening mini-workout?

### Maps for Bay Area Bicyclists

[Google Bay Area Bike Map](https://www.google.com/maps/@37.6273017,-122.3454433,10z/data=!5m1!1e3)

[511 SF Bay Biking Maps](https://511.org/biking/maps-n-trails)

[Silicon Valley Bicycle Coalition Maps](https://bikesiliconvalley.org/learn-ride/maps)

[angle]: /img/2021-08-09-bicycle-angle.jpg

[1]: https://en.wikipedia.org/wiki/Do_it_yourself
