---
title: Checklists for bicycle maintenance (3)
subtitle: Saddles, seatposts, pedals, shoes, grips, handlebars, stems, headsets
date: 2023-09-05
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle saddles", "bicycle seatposts", "bicycle pedals", "bicycle grips", "bicycle handlebars", "bicycle stems", "bicycle headsets"]
---

It takes a few minutes to go over the ergonomic adjustments to fit a bicycle
to a rider.  But, it makes the difference between fun and pain.  A wrong
saddle height starts hurting almost immediately.  In contrast, a day-long bike
tour is pure pleasure with: a bike of the right size; an individual choice of
saddle, grips, and shoes; and with good adjustments.

Some components in this post also require regular maintenance: pedals and
headsets need to be cleaned and regreased; seatposts and stems should be
installed with grease or anti-seize compound (unless in a carbon-fiber frame);
leather saddles want to be treated with an oil- or wax-type leather dressing.

These checklists cover components that determine whether pedalling is fun
:wink: \
[3.1 adjust saddle, seatpost][r3.1] \
[3.2 adjust grips, handlebar, stem][r3.2] \
[3.3 adjust pedals, shoes][r3.3] \
[3.4 remove/install pedals][r3.4] \
[3.5 service pedals][r3.5] \
[3.6 identify/adjust/service headsets][r3.6] \
[3.7 select/fit frame][r3.7] \
[3.8 resources, playlists][r3.8]
... <!--more-->

[r3.1]:	#31-adjust-saddle-seatpost
[r3.2]:	#32-adjust-grips-handlebar-stem
[r3.3]: #33-adjust-pedals-shoes
[r3.4]: #34-remove-install-pedals
[r3.5]: #35-service-pedals
[r3.6]:	#36-identify-adjust-service-headsets
[r3.7]: #37-select-fit-frame
[r3.8]: #38-resources-playlists

Links to the _resources/videos_ on [(SB)][2], [(PT)][1], [(BR)][5], [(BG)][4],
[(CMA)][3], see [post][0a], given in each task.

_Note:_ I update these checklists _in-place_ for improvements and
clarifications; also, some tasks still need to be written up.  For comments:
martin dot zaun at gmx dot com

### 3.1 Adjust saddle, seatpost

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) leather saddles][2d];
[(SB) saddles][2c];
[(SB) adjust saddle, seatpost][2k];
[(PT) adjust saddle, seatpost][1o];
[(PT) remove/install saddle][1q];
[(PT) remove/install seatpost][1m];
[(SB) unseize seatpost][2j];
[(PT) unseize seatpost, stem][1l];
[(SB) fit riding position][2r];
[(BG) fit riding position][4e];
[(BG) fit riding position][4r];
[(BG) adjust seatpost][4g];
[(BG) select saddle][4h];
[(BG) select saddle][4i];
[(BG) select saddle][4j];
[(BG) select saddle][4k];
[(BG) select saddle][4l];
[(BG) select saddle][4m];
[(BG) seatpost standards][4s];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) adjust saddle, seatpost][1p];
[(PT) remove/install saddle][1r];
[(PT) remove/install seatpost][1n];
[(PT) install seatpost collar][1j];
[(CMA) adjust saddle][3b];
[(CMA) adjust saddle][3a];
[(CMA) adjust saddle, handlebar][3l];
[adjust saddle][7c];
[install dropper post][7f];

Other: \
[(SB) seatpost sizes A-L][2m];
[(SB) seatpost sizes M-Z][2n];
[(SB) repair leather saddle][2g];

### 3.2 Adjust grips, handlebar, stem

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) identify/adjust stems/headsets][2b];
[(SB) install handlebar tape][2a];
[(PT) remove/install grips][1a];
[(PT) remove/install drop bar][1w];
[(PT) remove/install flat bar][1y];
[(PT) remove/install threaded stem][1u];
[(PT) remove/install threadless stem][1s];
[(SB) fit riding position][2r];
[(SB) unseize stem][2l];
[(PT) unseize seatpost, stem][1l];
[(BG) adjust drop bar][4b];
[(BG) adjust drop bar][4c];
[(BG) adjust drop bars][4d];
[(BG) adjust handlebar][4q];
[(BG) adjust mtb flat bar][4f];
[(BG) handlebar standards][4v];
[(BG) stem size standards][4u];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) remove/install grips][1b];
[(PT) remove/install flat bar][1z];
[(PT) remove/install drop bar][1x];
[(PT) remove/install threaded stem][1v];
[(PT) remove/install threadless stem][1t];
[(CMA) tighten handlebar][3c];
[(CMA) adjust handlebar][3g];
[(CMA) extend handlebar][3k];
[(CMA) adjust saddle, handlebar][3l];
[(CMA) remove sticky handlebar grips][3d];
[(CMA) replace threaded stem (quill)][3h];
[(CMA) replace stem, ahead system][3i];
[(BG) remove/install grips][4w];
[adjust threaded stem (quill)][7b];
[adjust threaded stem (quill)][7d];
[adjust stem][7a];
[install flat bars][7e];
[install drop bars][7g];

Other: \
[(SB) cribsheet handlebars][2o];

### 3.3 Adjust pedals, shoes

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) pedals][2h];
[(SB) select/adjust shoes/pedals][2i];
[(PT) adjust shoes/pedals][1aa];
[(BG) pedals standards][4x];
[(SB) fit riding position][2r];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[adjust shoes][7b];

### 3.4 Remove/install pedals

| | |
|:--:|:--|
| | pedals and crank arms are L/R-sided (R = drive side) |
| | |
| (1) | unscrew pedals: |
| | grip pedal axle with a long 15mm (pedal) wrench or a 8mm hex bit in a socket wrench, as needed |
| | align wrench with other side's crank arm, grab each and push/pull in opposite direction |
| | unscrew pedal: _R = anti-clockwise, L = clockwise_ |
| | |
| | hold pedal with wrench while freewheeling crank arm backward |
| | |
| (2) | install pedals:|
| | check pedal for stamped L/R letters or inspect thread: pointing up-right = R-threaded |
| | apply anti-seize compound or grease to the pedal thread |
| | screw in pedal: _R = clockwise, L = anti-clockwise_ |
| | |
| | grip pedal axle with a long 15mm (pedal) wrench or a 8mm hex bit in a socket wrench, as needed |
| | hold pedal with wrench while freewheeling crank arm forward |
| | |
| | align wrench with other side's crank arm, grab both and pull/push in opposite direction |
| | tighten to 35..43 Nm or recommended torque |

Resources: \
[(SB) pedals][2h];
[(PT) remove/install pedal][1c];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) remove/install pedal][1d];
[install pedals][8b];

### 3.5 Service pedals

| | TODO: summarize |
|:--:|:--|
| (1) | _for Shimano SPD pedals:_ ... |

Resources: \
[(SB) pedals][2h];
[(PT) service Shimano SPD pedals][1ab];
[(BG) pedals standards][4x];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) service Shimano SPD pedals][1ac];
[install pedals][8b];
[install pedals][7a];
[install pedals][7c];

### 3.6 Identify/adjust/service headsets

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) identify/adjust stems/headsets][2b];
[(SB) headsets][2e];
[(SB) headsets/handlebars tables][2f];
[(PT) service threaded headset][1g];
[(PT) service threadless headset][1h];
[(PT) headset standards][1e];
[(PT) headset standards][1f];
[(BG) headsets, forks][4n];
[(BG) headsets, forks][4o];
[(BG) headsets, forks][4p];
[(BG) headsets standards][4t];

Videos: \
[(PT) adjust threadless headset][1i];
[(PT) remove/install threaded stem][1k];
[(CMA) inspect/replace headset][3j];
[(CMA) headsets][3e];
[fix headset play][8a];
[headsets][10a];

Other: \
[(SB) cribsheet headsets][2p];

### 3.7 Select/fit frame

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) select/fit frame size][2t];
[(SB) select/fit frame size][2u];
[(SB) select frame for touring][2q];

### 3.8 Resources, playlists

Lists: \
[(SB) fitting, frames, seatposts][2aa];
[(PT) seatposts][1bc];
[(PT) pedals][1be];
[(PT) cranks, pedals][1bf];
[(PT) handlebars, stems, seatposts][1ba];
[(PT) handlebars, stems][1bd];
[(PT) headsets][1bb];
[(BG) bicycle-fitting][4ad];
[(BG) handlebars][4ae];
[(BG) handlebars, tape, grips][4ab];
[(BG) saddles, seatposts][4aa];
[(BG) forks, headsets][4ac];

[0a]: /post/2023-09-01-bicycle-resources/
[r0]: /post/2023-09-02-bicycle-routine-maintenance0.md
[r1]: /post/2023-09-03-bicycle-routine-maintenance1.md
[r2]: /post/2023-09-04-bicycle-routine-maintenance2.md
[r3]: /post/2023-09-05-bicycle-routine-maintenance3.md
[r4]: /post/2023-09-06-bicycle-routine-maintenance4.md

[1]: https://www.parktool.com/blog/repair-help

[1a]: https://www.parktool.com/en-us/blog/repair-help/handlebar-grip-installation-flat-bars
[1b]: https://www.youtube.com/watch?v=DN2Q8tnOLoo
[1c]: https://www.parktool.com/en-us/blog/repair-help/pedal-installation-and-removal
[1d]: https://www.youtube.com/watch?v=LFbSBG7jMzY
[1e]: https://www.parktool.com/en-us/blog/repair-help/headset-standards
[1f]: https://www.parktool.com/en-us/blog/repair-help/standardized-headset-identification-system
[1g]: https://www.parktool.com/en-us/blog/repair-help/threaded-headset-service
[1h]: https://www.parktool.com/en-us/blog/repair-help/threadless-headset-service
[1i]: https://www.youtube.com/watch?v=TihBJNn0Bck
[1j]: https://www.youtube.com/watch?v=RsBXrUtlHkI
[1k]: https://www.youtube.com/watch?v=yM90mfrjkUA
[1l]: https://www.parktool.com/en-us/blog/repair-help/seized-seatposts-and-stems
[1m]: https://www.parktool.com/en-us/blog/repair-help/how-to-remove-and-install-a-seatpost
[1n]: https://www.youtube.com/watch?v=fi9OQRMA3yo
[1o]: https://www.parktool.com/en-us/blog/repair-help/how-to-adjust-a-bike-saddle-seatpost
[1p]: https://www.youtube.com/watch?v=u4_V45BLFXs
[1q]: https://www.parktool.com/en-us/blog/repair-help/how-to-remove-and-install-a-bike-saddle
[1r]: https://www.youtube.com/watch?v=QNg9DeFZYtU
[1s]: https://www.parktool.com/en-us/blog/repair-help/stem-removal-installation-threadless
[1t]: https://www.youtube.com/watch?v=TPYGv6fMnBw
[1u]: https://www.parktool.com/en-us/blog/repair-help/stem-removal-installation-quill-stems
[1v]: https://www.youtube.com/watch?v=yM90mfrjkUA
[1w]: https://www.parktool.com/en-us/blog/repair-help/handlebar-removal-installation-drop-bars
[1x]: https://www.youtube.com/watch?v=AR2PyG0pJ7o
[1y]: https://www.parktool.com/en-us/blog/repair-help/handlebar-removal-installation-flat-bars
[1z]: https://www.youtube.com/watch?v=iE1Si-ndgE4

[1aa]: https://www.parktool.com/en-us/blog/repair-help/pedal-retention-system-and-cleats
[1ab]: https://www.parktool.com/en-us/blog/repair-help/spd-pedal-overhaul
[1ac]: https://www.youtube.com/watch?v=bVmSrsnVUGI

[1ba]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=54
[1bb]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=55
[1bc]: https://www.youtube.com/playlist?list=PLGCTGpvdT04RuBLs8i_YvIeCMx1CbGTzB
[1bd]: https://www.youtube.com/playlist?list=PLGCTGpvdT04T_783SX3q7iiNI7RcQwul2
[1be]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=57
[1bf]: https://www.youtube.com/playlist?list=PLGCTGpvdT04SdAP5vThZlOInVsJ891lm4

[2]: https://www.sheldonbrown.com

[2a]: https://www.sheldonbrown.com/handlebar-tape.html
[2b]: https://www.sheldonbrown.com/handsup.html
[2c]: https://www.sheldonbrown.com/saddles.html
[2d]: https://www.sheldonbrown.com/leather.html
[2e]: https://www.sheldonbrown.com/headsets.html
[2f]: https://www.sheldonbrown.com/cribsheet-headsets.html
[2g]: https://www.sheldonbrown.com/saddle-repair.html
[2h]: https://www.sheldonbrown.com/pedals.html
[2i]: https://www.sheldonbrown.com/shoe-pedal.html
[2j]: https://www.sheldonbrown.com/stuck-seatposts.html
[2k]: https://sheldonbrown.com/adv-cycling/sublimeseatpostI.pdf
[2l]: https://www.sheldonbrown.com/brandt/stuck-stem.html
[2m]: https://www.sheldonbrown.com/seatpost-sizes.html
[2n]: https://www.sheldonbrown.com/seatpost-sizes-m-z.html
[2o]: https://www.sheldonbrown.com/cribsheet-handlebars.html
[2p]: https://www.sheldonbrown.com/cribsheet-headsets.html
[2q]: https://www.sheldonbrown.com/frame-materials.html
[2r]: https://www.sheldonbrown.com/pain.html
[2t]: https://www.sheldonbrown.com/frame-sizing.html
[2u]: https://www.sheldonbrown.com/kops.html

[2aa]: https://www.sheldonbrown.com/frames.html

[3]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[3a]: https://www.youtube.com/watch?v=Q2lo-kDLWWE
[3b]: https://www.youtube.com/watch?v=aDoOKUNQKKo
[3c]: https://www.youtube.com/watch?v=zVYhsP1tgdk
[3d]: https://www.youtube.com/watch?v=nkngEq0E5kc
[3e]: https://www.youtube.com/watch?v=LNv5ywU896s
[3g]: https://www.youtube.com/watch?v=-2XsScBJnZo
[3h]: https://www.youtube.com/watch?v=WpyZvKiBHKk
[3i]: https://www.youtube.com/watch?v=uZhH_NoC-F4
[3j]: https://www.youtube.com/watch?v=ziImgS8Xe1Y
[3k]: https://www.youtube.com/watch?v=l8hyRKFPxWc
[3l]: https://www.youtube.com/watch?v=rTzhWfb1Cwk

[4]: https://bike.bikegremlin.com/post-list-by-category/

[4a]: https://bike.bikegremlin.com/14694/bicycle-tightening-torques/
[4b]: https://bike.bikegremlin.com/946/road-bar-geometry/
[4c]: https://bike.bikegremlin.com/927/road-bar-hand-positions/
[4d]: https://bike.bikegremlin.com/920/setting-up-road-bike-bars/
[4e]: https://bike.bikegremlin.com/360/setting-up-riding-position-bike-fitting/
[4f]: https://bike.bikegremlin.com/1160/mtb-bars-setup/
[4g]: https://bike.bikegremlin.com/3144/minimum-seatpost-insertion-length/
[4h]: https://bike.bikegremlin.com/14991/comfortable-bicycle-saddle/
[4i]: https://bike.bikegremlin.com/3051/05-optimal-bicycle-saddle-seat-width/
[4j]: https://bike.bikegremlin.com/3030/04-measuring-sit-bone-width/
[4k]: https://bike.bikegremlin.com/2985/bicycle-saddle-seat-shape/
[4l]: https://bike.bikegremlin.com/2973/bicycle-saddle-seat-padding/
[4m]: https://bike.bikegremlin.com/2954/bicycle-saddle-materials-explained/
[4n]: https://bike.bikegremlin.com/3243/important-fork-headset-bearing-dimension/
[4o]: https://bike.bikegremlin.com/3246/names-bicycle-head-tube-fork-headset-parts/
[4p]: https://bike.bikegremlin.com/545/bicycle-fork/
[4q]: https://bike.bikegremlin.com/1188/mechanics-bar-angle-height-setup/
[4r]: https://bike.bikegremlin.com/2819/setting-comfortable-cycling-position/
[4s]: https://bike.bikegremlin.com/3114/seat-post-diameter-sizes-standards/
[4t]: https://bike.bikegremlin.com/3476/bicycle-headset-bearings-standards/
[4u]: https://bike.bikegremlin.com/3729/bicycle-stem-size-standards/
[4v]: https://bike.bikegremlin.com/3784/bicycle-handlebar-dimension-standards/
[4w]: https://www.youtube.com/watch?v=bktB9cHpwxY
[4x]: https://bike.bikegremlin.com/1141/pedals-types/

[4aa]: https://bike.bikegremlin.com/category/frame/saddles-seats/
[4ab]: https://www.bikegremlin.com/category/bicycles/handlebars-grips/
[4ac]: https://bike.bikegremlin.com/category/frame/forks/
[4ad]: https://bike.bikegremlin.com/category/basic-things/bicycle-fitting/
[4ae]: https://bike.bikegremlin.com/category/frame/bars/

[5]: https://www.bikeride.com/guide/

[5a]: https://www.bikeride.com/torque-specifications/

[7a]: https://www.youtube.com/watch?v=7UhNZM__zxs
[7b]: https://www.youtube.com/watch?v=RsL-GoWjwPE
[7c]: https://www.youtube.com/watch?v=xBuv5cUenps
[7d]: https://www.youtube.com/watch?v=7aXu2J3m5ws
[7e]: https://www.youtube.com/watch?v=86nFCj2zq_o
[7f]: https://www.youtube.com/watch?v=O3iU5QbNYlY
[7g]: https://www.youtube.com/watch?v=Bh8ItUwkxDs

[8a]: https://www.youtube.com/watch?v=BYr0jmzGBGo
[8b]: https://www.youtube.com/watch?v=1LKo6TB9nLM

[9a]: https://www.dedhambike.com/articles/torque-table-pg186.htm

[10a]: https://www.youtube.com/watch?v=2oTs3YHtJWg
