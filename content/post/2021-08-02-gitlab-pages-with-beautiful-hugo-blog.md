---
title: How to set up a website + blog with GitLab Pages
date: 2021-08-02
tags: ["blog", "website", "GitLab Pages", "Static Site Generator", "Hugo", "Beautiful Hugo Blog Template"]
---

Want to set up a website or a blog?  Easy to run into decision paralysis.  So
many platforms out there: hosting packages, content management systems (CMS),
static site generators (SSG), blog hosters, blog engines, frameworks etc.

For commercial websites, [WordPress][20] has become a de-facto CMS standard,
almost all [domain hosters][21] include it in their plans.  WordPress comes
with a variety of features, plan bundles, plugins, in fact an ecosystem for:
personal, business, or eCommerce sites (features such as email subscription,
premium content, payments etc).

On the other hand, for a public blog, a developer website, and many other
projects: A _static website_, which displays the same content to all visitors,
is not only sufficient but actually preferable (see below)
... <!--more-->

For my personal blog and website, the _Pages_ feature offered by repository
hosters looks most promising, such as [GitHub Pages][0] or [GitLab Pages][1].
_Pages_ allows to publish a static website directly from a hosted or
self-managed repository with reasonably fast compile times.

So, let's build a personal, static website with a blog: _3 decisions, 3 steps,
and 1 summary_.

### Decision #1: Going with _GitLab Pages_

As most of my code is on _GitLab_, I'm picking [GitLab Pages][2] for hosting
my personal website and blog as well.  Can easily cross-reference between code
and blog posts, or merge repositories if desirable.  No need for a separate
account with a [blog hosting service][31] or to join a [blogosphere][32].

Following the _GitLab Pages_ [getting started][3] list, my website and blog
will have these coordinates:
- _Project URL:_ https://gitlab.com/netropy/blog/ \
  hosts a project `blog` under my GitLab username `netropy`
- _Site URL:_ https://netropy.gitlab.io/blog/ \
  where the content will be served after publishing
- Note: domain names and base URLs are configurable, see [here][4]; for
  limitations, see [here][5].

For the website or blog to appear under a purchased domain, [this][29]
tutorial describes the process of connecting the GitLab Pages site.

### Decision #2: Using a Static Site Generator

Web pages can be directly written in plain HTML.  Much better, though, is to
employ a [Static Site Generator (SSG)][6], which takes content written in
[markdown][19] (or other markup languages) and compiles it into the final
_*.{html,css,js}_ site, based on a set of template pages and configurations.
The SSG approach (with a few extensions) has also been coined as
["Jamstack"][25a] (JavaScript, APIs, and Markup).

Just a quick look at pros & cons.  This [article][23] argues the limitations
of SSGs, mainly the lack of dynamic pages, compared to the use of a fully
fledged [Content Management System (CMS)][24].

Ok, but what if a static website would do?  The benefits of then using an SSG
are substantial, see [this][22], [that][25b], and the [other][33]:
- Performance (fast loading pages)
- Scale (CDN-cached pages & resources)
- Security (pre-generated site, read-only files)
- Maintainability, Portability, Developer Experience (simplicity!).

So, what makes an SSG website template deploy in _GitLab Pages_?

See [here][7]: the definition of a [CI/CD][8] job with a `pages` section in
the file `.gitlab-ci.yml`; a directory `public` to hold the compiled site
(unless serving from in-memory); and an enabled [GitLab Runner][9].

### Decision #3: Choosing the _Hugo Pages Template_

It is possible to create a plain _SSG_ or an _SSG Pages_ site from scratch.
Examples: this [6-step tutorial][10] for GitLab Pages using [Jekyll][11]
([Ruby][12]), or this [7-step tutorial][28] for hosted servers using
[Hugo][16] ([Go][15]).

A quicker option, though, is to pick and clone a _template project_ for GitLab
Pages from the community examples, like [here][13a] or [there][13b].  Or
download a curated _theme_ (free or premium) from an SSG vendor (for example,
[Jekyll themes][40]), as described by this [10-step tutorial][29] for GitLab
Pages.

After a look at the features and the design of the listed examples, I chose to
go with the [Beautiful Hugo Blog Template][14].  I like the template's layout,
the simplicity, the blog features, and Hugo's alleged speed.  Also, the Hugo
SSG is [open-source][27] (and I'm conversant in [Go][15]).

As a last check: no major complaints about _Hugo_ in articles surveying the
SSG landscape, for instance, [here][26].

### Step 1: Fork the blog template

So, here we [go][17] (in both senses :-):
1. login to GitLab, create a fork: https://gitlab.com/pages/hugo \
   -> _Fork_, _Project name: blog_, _Project URL: \<select a namespace\>_
1. unlink fork from its upstream project: https://gitlab.com/netropy/blog \
   -> _Settings_ -> _Advanced_ -> _Remove fork relationship_
1. run pipeline and deploy: https://gitlab.com/netropy/blog/-/pipelines \
   -> _Run pipeline_
1. find the pages: https://netropy.gitlab.io/blog/ \
   -> _Settings_ -> _Your pages are served under ..._
1. no need to change the project URL: https://gitlab.com/netropy/blog/ \
   -> _Settings_ -> _Advanced_ -> _Change path_

### Step 2: Build the project locally

Install [Hugo][18].  Clone or download the repository: \
`git clone https://gitlab.com/netropy/blog.git`

Work locally:
1. serve (watches filesystem changes): `hugo server`
1. preview the site: <http://localhost:1313/hugo/>
1. add content
1. CLI: `hugo --help`, `hugo server --help`
1. sometimes needed:
   - kill the server: `Ctrl-C`
   - restart server and reload page in browser
1. usually not needed:
   - recompile (to disk): `hugo`
   - don't serve from mem: `hugo server --renderToDisk`
   - fully re-render on changes: `hugo server --disableFastRender`
1. publish: `... ; git push`

### Step 3: Customize the basic content

For a start, adjust:
1. `config.toml` (baseurl, title, subtitle, [Author], ...)
1. `content/_index.md` (front page content, adjust or delete)
1. `content/page/about.md` (subtitle, ...)
1. `content/post/*` (create 1st .md post, remove sample posts)

The new _baseurl_ makes the preview site URL: <http://localhost:1313/blog/>

Voilà!

### Summary:

1. _GitLab Pages_ does the job well: publish a static website directly from a
   repository (same for _GitHub Pages_).
1. A _Static Site Generator (SSG)_ is a must: write the content in a
   _markdown_ dialect; static pages load fast.
1. The _Hugo SSG_ is indeed quick: recompile times of 4..9 ms for this
   (currently) small blog site.
1. The _Beautiful Hugo Blog Template_ gives a good start: fork & customize the
   project; the blog site looks nice & practical.
1. However, there are compile warnings and the blog site shows a problem:
   issues with the Hugo Blog Template -> discussed in the next post.

So far, so good.

[0]: https://pages.github.com/

[1]: https://docs.gitlab.com/ee/user/project/pages/

[2]: https://docs.gitlab.com/ee/user/project/pages/introduction.html

[3]: https://docs.gitlab.com/ee/user/project/pages/index.html#getting-started

[4]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html

[5]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#limitations

[6]: https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/

[7]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-pages-requirements

[8]: https://docs.gitlab.com/ee/ci/

[9]: https://docs.gitlab.com/runner/

[10]: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html

[11]: https://jekyllrb.com/

[12]: https://www.ruby-lang.org/en/

[13a]: https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/#ssgs-examples

[13b]: https://gitlab.com/pages

[14]: https://pages.gitlab.io/hugo/

[15]: https://golang.org/

[16]: https://gohugo.io/

[17]: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html

[18]: https://gohugo.io/overview/installing/

[19]: https://en.wikipedia.org/wiki/Markdown

[20]: https://wordpress.org

[21]: https://www.top10.com/hosting/domainhosting-comparison

[22]: https://gohugo.io/about/benefits/

[23]: https://www.sitepoint.com/7-reasons-not-use-static-site-generator/

[24]: https://en.wikipedia.org/wiki/Content_management_system

[25a]: https://jamstack.org

[25b]: https://jamstack.org/why-jamstack/

[26]: https://www.sitepoint.com/static-site-generators/

[27]: https://github.com/gohugoio/hugo

[28]: https://www.atlantic.net/vps-hosting/how-to-install-hugo-website-generator-on-ubuntu-20-04/

[29]: https://blog.prototypr.io/gitlab-pages-101-for-designers-a689d2cb8f5f

[30]: https://jekyllthemes.io/

[31]: https://www.blogtyrant.com/best-blog-host/

[32]: https://en.wikipedia.org/wiki/Blogosphere

[33]: https://bejamas.io/blog/jamstack/
