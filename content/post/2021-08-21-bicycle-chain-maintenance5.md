---
title: About bicycle chains (5)
subtitle: How to clean a chain for oil lubrication
date: 2021-08-21
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain"]
---

All bicyclists know that the only correct way of cleaning a chain is: the
[ShelBroCo][11] system.  Start with disassembling the chain into its 456
parts, then... well, click on the link :wink:

Happily, a chain can be cleaned _quickly & effectively_: by taking it off the
bike and soaking it in a strong solvent, such as petrol or mineral spirits.
This method also washes out the dirt and grime from _within_ the chain.  After
the solvent has evaporated, the chain is mounted and relubricated again.

_The result:_ Lower friction + longer lifespan of the entire
[drivetrain][13a].

This post explains the hows & whys of cleaning a chain for traditional oil
lubrication
... <!--more-->

Topics: \
[Where on the chain is lubricated needed, unwanted?](#where-on-the-chain-is-lubricated-needed-unwanted) \
[What is that black grime on the bicycle chain?](#what-is-that-black-grime-on-the-bicycle-chain) \
[Bad: Not cleaning the chain before lubricating it](#bad-not-cleaning-the-chain-before-lubricating-it) \
[Messy: Cleaning and lubricating the chain on the bike](#messy-cleaning-and-lubricating-the-chain-on-the-bike) \
[Better: Cleaning the chain on the bike with a scrubber tool](#better-cleaning-the-chain-on-the-bike-with-a-scrubber-tool) \
[Far Better: Cleaning the chain off the bike (oil lubrication)](#far-better-cleaning-the-chain-off-the-bike-oil-lubrication) \
[Not Better: Ultrasonic cleaners](#not-better-ultrasonic-cleaners) \
[How to responsibly dispose of liquid solvents?](#how-to-responsibly-dispose-of-liquid-solvents)

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### Where on the chain is lubricated needed, unwanted?

In short: inside the chain -- vital; on the outside -- detrimental.  Best
explained in this nice video by [(OZC2)][32a] (first 2 minutes).

### What is that black grime on the bicycle chain?

That black paste is made of dirt, dust, and metal particles [(SB0)][10a]:

> Most of the schmutz that contaminates a chain is thrown up by the front tire
> and lands on the outer circumference of the chain.  Sheldon Brown
>
> Some of the grit is metal wear particles from the sprockets and from the
> chain itself.  Aluminum oxide from chainrings makes chain dirt black -- and
> it is very hard and abrasive.  John Allen

Once that grit makes it inside the chain, it is not only felt as power loss
from friction while riding but also causes _chain wear_ resulting in "chain
stretch" [(SB2)][12]:

> The black stuff is oil colored by steel wear particles, nearly all of which
> come from pin and sleeve wear, the wear that causes pitch elongation.  The
> rate of wear is dependent primarily on how clean the chain is internally
> rather than visible external cleanliness that gets the most attention...
>
> Chain wear depends almost entirely on cleanliness and lubrication rather
> than being a load problem.  For bicycles the effect of load variations is
> insignificant compared to the lubricant and grit effects.  For example,
> motorcycle primary chains, operated under oil in clean conditions, last as
> much as 100,000 miles while exposed rear chains must be replaced often.
> Jobst Brandt

### Bad: Not cleaning the chain before lubricating it

Applying lubricant on a dirty chain will dilute and transport the grime from
the outside into the chain; see this nice video demonstration [(OZC2)][32c],
or [(SB2)][12]:

> Only when a dirty chain is oiled, or has excessive oil on it, can this grit
> move inside to cause damage.  Commercial abrasive grinding paste is made of
> oil and silicon dioxide (sand) and silicon carbide (sand).  You couldn't do
> it better if you tried to destroy a chain, than to oil it when dirty.  Jobst
> Brandt

Some product labels of chain lubes state "cleans & lubricates"; this should
_not_ be taken as substitute for cleaning the chain, per this video by
[(OZC0)][30a].

__Problems:__ (0) Excessive wear on the entire drivetrain.  (1) "Leg tatoos"
and dirty hands.

### Messy: Cleaning and lubricating the chain on the bike

Leave the chain on the bike -- this is how most of us learned to clean and
lube a chain.  Examples: method _easy drivetrain cleaning_ in [(CT1)][40]; or
[this][x0] and [this][x1] video in German.

It goes thike this:
1. Turn the bike on its head, get a stool to sit on, as this takes a while.
1. Clean the outside of the chain and sprockets with rags or paper towels.
1. If the grime doesn't come off easily, apply a solvent like [WD-40][20e].
1. To clean beween the cassette cogs, fold a paper towel and pull it through.
1. Lay a line of lubricant on the chain while pedalling backwards.
1. Wipe off excess oil, clean the bike from splashed oil or solvent residues.

__Problems:__  (2) It's messy, why we've avoided cleaning and lubrication.
(3) We merely cleaned the outside of the chain, the grime remains inside
causing excessive wear.

### Better: Cleaning the chain on the bike with a scrubber tool

Problem (2) can be alleviated by using a _chain scrubber_ (brush cleaner),
such as [(SB0)][10b]: The chain runs through a liquid degreaser and is
agitated by brushes.

The method is rightly labeled _moderate drivetrain cleaning_ in [(CT1)][40]:
One rotates the chain for ~1 minute in degreaser, then in soapy water, then
cleans sprockets and derailleurs, rinses all with water, spins the chain dry,
and finally applies an oil lubricant.

_Resources:_ How to use a chain scrubber, [(PT)][53] or videos [(PT2)][52],
[(PT1)][51a], [(OZC0)][30d]; the problem with chain scrubbers [(OZC2)][32d];
how to lubricate the chain [(PT1)][51b]; how to wash a bike, [(PT)][54].

This procedure requires tools and effort, as per above resources:
- Cover the ground, best have a bike work stand, gloves, apron, and rags.
- Unmount the rear wheel, install a dummy hub or chain keeper [(PT)][50d],
  [(CT)][41].
- Use a bicycle-specific and truly bio-degradable degreaser [(PT)][50c],
  [(CT)][42].
- Some scrubbers have a magnet to hold back metallic particles in the fluid
  [(PT)][50a].
- Clean all sprockets and derailleurs, for example, using this special brush
  [(PT)][50b].
- Avoid getting solvents into the bearings or freehub, it contaminates the
  grease.
- Tons of chain lubricants out there (dry, wet, ...) besides this all-round
  lube [(PT)][50e].
- Apply the lubricant one roller at a time and check each link for damage.
- Lastly, wipe the chain with a rag to remove excess oil, which only attracts
  dirt.

Despite all this effort of cleaning, some grime remains inside [(SB2)][12]:

> Devices with rotating brushes that can be clamped on the chain while on the
> bicycle, do a fair job but are messy and do not prevent fine grit from
> becoming suspended in the solvent.  External brushing or wiping moves grit
> out of sight, but mainly into the openings in the chain where subsequent
> oiling will carry it inside.  Jobst Brandt

__Problems:__  (2) It's still messy and time consuming.  (3) Effectiveness of
cleaning, some of the grime remains inside the chain.

### Far Better: Cleaning the chain off the bike (oil lubrication)

Problems (2) and (3) have some bicyclists conclude: _Never oil a chain on the
bike_ [(SB2)][12].

> This means the chain should be cleaned of grit before oiling, and because
> this is practically impossible without submerging the chain in solvent
> (kerosene, commercial solvent, or paint thinner), it must be taken off the
> bicycle.  Jobst Brandt

My experience: This method is easy & effective, it cleans a bicycle chain in
no time!
1. Place the chain (with its master link) in a wide, airtight, plastic jar
   with a lid.
1. Cover it with a strong solvent, such as [mineral spirits][20a] or
   [petrol][20b].
1. Close the lid, shake the jar, let soak for ~10 minutes, shake again, let
   settle.
1. Optionally: remove oily solvent residues with a degreaser, rinse with
   water.
1. Hang the chain for drying (or sling it around), let solvent residues
   evaporate.

For gummed chains, extend the soaking time (overnight) and repeat step #3 with
fresh solvent until the chain comes out clean.  Another solvent option is
[paint thinner][20c] (less potent than mineral spirits, in my experience);
still, any of them is much stronger than degreaser with hot water. The most
cost-effective solvent is probably _petrol (gasoline)_.

_Resources:_ This Container Method is rightly labeled _detailed drivetrain
cleaning_ in [(CT1)][40]; this nice video by [(OZC0)][30e] shows the steps;
this older video by [(OZC1)][31] features cutting inserts and a sieve for the
plastic container, so that dissolved dirt can sink to the bottom.

On the last step #5, solvent residues [(SB2)][12]:

> Removing solvent from the chain after rinsing is important.  Compressed air
> is not readily available in the household, nor is a centrifuge...  The
> other way is to evaporate it.  Accelerated drying methods by heating should
> be avoided because they can be explosive.

Thought so, too. :wink:

__Problems:__ (4) Handling: Need rubber gloves, ventilated space.  (5)
Disposal: Petrochemical solvents can be reused a few times, but eventually
become hazardous waste (see below).

### Not Better: Ultrasonic cleaners

Problems (4) and (5) can be avoided by using a bio-degradable degreaser with
hot water in an [ultrasonic cleaner][20f].

Obviously, _no inflammable_ solvents must be used in an ultrasonic cleaner.
Instead, a degreaser, washing detergents, and water are doing the job --
greatly assisted by heat and ultrasound forming microscopic cavitation
bubbles.  Note, though, the devices are quite expensive for household use, for
example, [(vevor)][x2].

_Resources:_ This video by [(OZC0)][30c] shows the cleaning procedure; article
[(CT1)][40] portrays the effectiveness of ultrasonic cleaning (no data) as:

> You’ll get mixed results with really stubborn grit, but it’ll do an
> impressive job at getting in all the nooks and crannies.  Pay close
> attention to the heat and solvent used in order not to cause plastic or
> anodisation damage.

This video by [(OZC0)][30b] confirms that claim in an _N=1_ test, ranking the
methods as:
1. homemade container with petrol
1. chain scrubber with degreaser and laundry liquid
1. ultrasonic cleaner with degreaser and laundry liquid.

__Problems:__ (6) Costs: Expensive.  (7) Less effective on gummed chains.  (8)
Pollution: Often, solvents are not 100% bio-degradable, contaminated water is
flushed in the sink.

### How to responsibly dispose of liquid solvents?

The ultrasonic cleaner method produces 1..2 l of contaminated water per chain
clean.  Hence, it's _vital_ to check the degreaser's listed ingredients for
_environmental harm_.

Using _wax_ instead of an _oil-based_ chain lubricant further reduces the
environmental impact, as no or lesser degreaser will be used, see next post
[(6)][0f].

The container method uses ~0.25 l (1 cup) of a petrochemical solvent per
chain.  However, the mineral spirits or petrol can be decanted or filtered --
and so be _reused_ a few of times.

When these solvents become too contaminated, though, they must be discarded.
Where I live, this means scheduling an appointment with a _hazardous waste_
station ~12 mi away (which burns 1 gal of car fuel for the roundtrip, unless
riding by bike :wink:).

By comments in bicycle fora, many riders prefer to let evaporate their
quantities of petrochemicals -- _not ideal_, though.  Others suggested that
filtered petrol can still be put to use in a gasoline lawnmower.

_My guess:_ Safely burning small amounts of spirits does the least
environmental harm.

### TODO [added 2024-10]:

Try out [(BG)][21]: "chainsaw bar oil (diluted with about 10 to 30 % of diesel
or odourless mineral spirits if riding in very low temperatures). The thicker
it is, the longer it will last and resist rain washout better, but attract
more dirt – and, again, vice-versa for thinned down oil."

Also cover: [(BG1)][21a], [(BG2)][21b].

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[10a]: https://www.sheldonbrown.com/chains.html#oil
[10b]: https://www.sheldonbrown.com/chains.html#cleaning

[11]: https://www.sheldonbrown.com/chainclean.html

[12]: https://www.sheldonbrown.com/brandt/chain-care.html

[13a]: https://www.sheldonbrown.com/gloss_dr-z.html#drivetrain

[20a]: https://en.wikipedia.org/wiki/White_spirit
[20b]: https://en.wikipedia.org/wiki/Gasoline
[20c]: https://en.wikipedia.org/wiki/Paint_thinner
[20e]: https://en.wikipedia.org/wiki/WD-40
[20f]: https://en.wikipedia.org/wiki/Ultrasonic_cleaning

[21]: https://bike.bikegremlin.com/5855/the-best-bicycle-chain-lubricant/
[21a]: https://bike.bikegremlin.com/1986/bicycle-chain-lubricants-explained/
[21b]: https://bike.bikegremlin.com/44/best-bicycle-chain-lube/

[30a]: https://www.youtube.com/watch?v=D0eOxbnzDQA&t=69s
[30b]: https://www.youtube.com/watch?v=D0eOxbnzDQA&t=142s
[30c]: https://www.youtube.com/watch?v=D0eOxbnzDQA&t=235s
[30d]: https://www.youtube.com/watch?v=D0eOxbnzDQA&t=375s
[30e]: https://www.youtube.com/watch?v=D0eOxbnzDQA&t=506s

[31]: https://www.youtube.com/watch?v=9x8JTUa_hZU&t=45s

[32a]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=160s
[32c]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=360s
[32d]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=519s

[40]: https://cyclingtips.com/2018/05/chain-cleaning-and-maintenance-how-to/

[41]: https://cyclingtips.com/2018/01/best-chain-keepers-reviewed-chain-clean-tool/

[42]: https://cyclingtips.com/2018/12/the-best-degreaser-for-bicycles/

[50a]: https://www.parktool.com/product/cyclone-chain-scrubber-cm-5-3
[50b]: https://www.parktool.com/product/gearclean-brush-gsc-1
[50c]: https://www.parktool.com/product/bio-chainbrite-cb-4
[50d]: https://www.parktool.com/product/dummy-hub-dh-1
[50e]: https://www.parktool.com/product/synthetic-blend-chain-lube-with-ptfe-cl-1

[51a]: https://www.youtube.com/watch?v=B2sKhSDrugE&t=215s
[51b]: https://www.youtube.com/watch?v=B2sKhSDrugE&t=852s

[52]: https://www.youtube.com/watch?v=MuwS_nSevy4&t=33s

[53]: https://www.parktool.com/blog/repair-help/chain-cleaning-with-a-park-tool-chain-scrubber

[54]: https://www.parktool.com/blog/repair-help/bike-washing-and-cleaning

[x0]: https://www.youtube.com/watch?v=DayB7k9aEMc

[x1]: https://www.youtube.com/watch?v=9DrVyCjvMLA

[x2]: https://www.vevor.com/collections/cleaning-equipment
