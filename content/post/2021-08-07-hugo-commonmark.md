---
title: Grateful for CommonMark in Hugo, LaTeX in GitLab
date: 2021-08-07
tags: ["blog", "website", "Hugo", "CommonMark", "Markdown"]
---

_Markdown_ is a simple, human-readable and -writable format for text.  The
basics can be learned in 60 Seconds, see [here][56].

At the same time, the fragmented landscape of Markdown [dialects and
implementations][55] has been a problem -- why I view the [CommonMark][50]
standard as a blessing.  (I do not follow the objection against the name
_Common Markdown_ along with the demand of an apology by one of the two
Markdown creators, back in [2014][54].) 

Happily, _Hugo Blog_ switched to a CommonMark-compliant engine in [2019][53].
So far, this blog site gave me no trouble using: _lists, block quotes, code
spans, reference links, autolinks, images, tables, hard line breaks_
... <!--more-->

Note: The [GitHub][51]- and [GitLab][52]-Flavored Markdown versions are
different extensions of _CommonMark_.  Porting some of my content between them
was easy -- except for embedded + separate [LaTeX/Math][57] formulas, which
have been supported in GitLab but not in GitHub (at least as of 2019/2020).

The alternative to compose math formulas in HTML from unicode symbols cannot
convince: their readability, writability, and rendering doesn't match LaTeX.

Another option would be to render Tex/LaTeX from JavaScript (for example, via
[MathJax][59] or [TeXnous][58]).  However, rendering static pages has just
fewer moving parts.

[50]: https://commonmark.org

[51]: https://github.github.com/gfm/

[52]: https://docs.gitlab.com/ee/user/markdown.html

[53]: https://gohugo.io/news/0.60.0-relnotes/

[54]: https://blog.codinghorror.com/standard-markdown-is-now-common-markdown/

[55]: https://en.wikipedia.org/wiki/Markdown#Standardization

[56]: https://commonmark.org/help/

[57]: https://www.latex-project.org

[58]: https://gitlab.com/TeXnous

[59]: https://www.mathjax.org/
