---
title: Bicycle Tool Storage Bottle
subtitle: DIY repair on the road
date: 2021-08-23
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle tools"]
---

To be prepared for emergency repairs on the road, the market offers a
multitude of products for carrying tools and spare parts: mini tool wallets,
top tube bags, frame triangle bags, handlebar stem bags, under saddle bags,
handlebar storage sleeves, tool storage bottles etc.

My preference is for a _tool storage bottle_ that fits the bottle cages on our
bikes:
- A tall bike bottle can hold a mini pump, a spare tube, and a few tools.
- No unmounting and mounting of bags between bikes, just grab the tool
  bottle.
- A used, plastic squeeze bottle attracts no attention when leaving the bike
  for a minute.

This post shows photos of the tool bottle and its content as I packed it for
our rides
... <!--more-->

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

[(OZC)][1] posted a video on how to reuse an old, plastic squeeze bottle as a
tool storage bottle: Cut out a wall segment and make the bottle ends slide
into another.  The result: a closeable, wide-mouth container fitting the
bike's bottle cage.

As another option, the market offers various bike storage containers; price
range $7..$15: [zefal][10], [rei][11], [lezyne][12], [profile][13], [pdw][14]
et al.

My preference is for a bottle tall enough to hold a mini bike pump, a spare
tube, and a few tools -- why I opted for a 1l (32 fl oz) squeeze water bottle
as tool container.

So, here are my two bike bottles.  Guess, which one holds the tools and parts?

![tool and water bottle][bottle1]

It's the red bottle (more opaque, white bottle stays cooler).  Here's its
content...

![tool storage bottle with content][bottle2]

... from right to left:

- [BlenderBottle Halex][20] squeeze water bottle with straw, switchable modes:
  locked, tilt up, tilt down; largest size still fits our 3" bottle cages: ~1l
  or 32 fl oz (3.09" x 3.09" x 10.71").
- A few sheets of folded kitchen paper towel.
- Spare tube (presta valve); the folded shape stabilized with rubber bands.
- [pdw 3wrencho tire lever][21] and 3 plastic levers; the metal lever deals
  with [tight tires][2] without pinching the tube; its 15mm axle nut wrench is
  needed for our [IGH][3] bikes.
- [SRAM 8Speed PowerLink][24] reusable spare master link; can be locked
  without plier.
- Bike multifunction tool with essential hex wrenches, Phillips and flathead
  screwdriver; for better-equipped mini-tools, see [(PT)][37], [Topeak][30],
  [1][31], [2][32], [3][33], [4][34], [5][35] etc.
- [Pro Bike Tool Mini Bike Pump][23] for Presta and Schrader valves:
  - The attachable hose is stored within the pump and has an integrated
    gauge.
  - Not using the pump holder bracket with strap, which mounts with a
    bottle cage.
  - Aluminium alloy; length: 22cm (8.8"), diameter: 2.5cm (1"), weight: 128g
    (4.5 oz).
  - Pumping takes 3..5 minutes for a 700c 30mm tire, the metal handle gets
    hot.
  - Product page says 100 psi (6.9 bar), I cannot get the tire pressure above
    70 psi.
  - Good enough to get us home on our performance or touring bikes. :smile:

So far, our master link chains have never given me problems; yet, see prior
[post][chain2] on tool-less master links and pocket-size chain tools.

Weight of the packed tool bottle: 680g (24 oz) -- not great but ok.  I usually
grab this tool bottle for rides farther away than ~5mi or 8km from our home or
car.

P.S.: A good discussion of tools for on-road repairs by [(SB)][4].

P.P.S.: Imaginative ideas for mini-tools and storage by some manufacturers; to
pick just one: [handlebar storage sleeves][36].

[chain2]: /post/2021-08-18-bicycle-chain-maintenance2/
[bottle1]: /img/2021-08-23-bicycle-tool-bottle1.jpg
[bottle2]: /img/2021-08-23-bicycle-tool-bottle2.jpg

[1]: https://www.youtube.com/watch?v=hJsGuWemPjE
[2]: https://cyclingtips.com/2017/12/tips-for-installing-removing-impossibly-tight-tyres/
[3]: https://www.sheldonbrown.com/internal-gears.html
[4]: https://www.sheldonbrown.com/on-road-repairs.html

[10]: https://www.zefal.com/en/frame-bag/311-z-box-l.html
[11]: https://www.rei.com/product/183635/co-op-cycles-keg-bike-tool-storage-bottle
[12]: https://ride.lezyne.com/products/1-bc-flwcaddy-v104
[13]: https://profile-design.com/collections/storage/products/water-bottle-storage-ii?variant=33212687810623
[14]: https://ridepdw.com/collections/tools-and-spare-parts/products/rose-city-tool-keg

[20]: https://www.blenderbottle.com/products/halex-squeeze-bottle-with-straw?variant=40863146311878
[21]: https://ridepdw.com/collections/tools-and-spare-parts/products/3wrencho
[22]: https://ridepdw.com/collections/tools-and-spare-parts/products/theyre-tire-levers
[23]: https://www.probiketool.com/us/product/mini-high-pressure-bike-pump-with-gauge/
[24]: https://www.sram.com/en/sram/models/cn-plnk-8spd-a1

[30]: https://www.topeak.com/us/en/products/75-Mini-Tools
[31]: https://www.bicycling.com/bikes-gear/a26222598/best-multi-tools/
[32]: https://www.outdoorgearlab.com/topics/biking/best-bike-multi-tool
[33]: https://www.bikeradar.com/advice/buyers-guides/best-multi-tool/
[34]: https://bikerumor.com/2021/06/25/best-bike-multitools-minitools/
[35]: https://www.cyclingnews.com/features/best-bike-multi-tools/
[36]: https://www.wolftoothcomponents.com/collections/tools/products/encase-system-bar-kit-one
[37]: https://www.parktool.com/category/multi-tools
