---
title: About bicycle chains (3)
subtitle: How to take off and mount a chain in seconds
date: 2021-08-19
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain", "bicycle tools"]
---

Post [(2)][0b] recapped the essential tools: chain checker, link plier,
and chain breaker/tool; it also covered _tool-less_ master links, which can be
opened and closed by hand.

This post follows up with photos of how to swap a master link chain using a
link plier.  Whether by plier or by hand -- it only takes seconds. And this
makes a difference: We take the chain off the bike whenever it has picked up
dirt.

Then, for an oil-lubricated chain, we clean it in a jar with a solvent and put
it back on, see post [(5)][0e].  For a waxed chain, we rinse it with boiling
water and rewax, see post [(6)][0f].   Better: We just replace the chain with
a spare, and later clean the lot of them in one go.

This post assumes having a chain of correct length; see post [(4)][0d] for
sizing a chain.  So, let's go and take the chain off the bike and mount a new
one in no time
... <!--more-->

Topics: \
[How to disconnect a master link chain](#how-to-disconnect-a-master-link-chain) \
[How to connect a master link chain](#how-to-connect-a-master-link-chain) \
[Resources](#resources)

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### How to disconnect a master link chain

The SRAM [8SPD][91b] master link comes with the [PC-870][91a] chain.  Using a
_chain hook_ is optional, it just prevents the ends from falling.  Engage the
master link with the _link plier_ and...

![Chain Replacement][chain-replace01]

...push _inwards_.  It doesn't take much force...

![Chain Replacement][chain-replace02]

...and the master link unlocks with a click.

### How to connect a master link chain

We could connect the chain with by same procedure in reverse.  Although, for
this non-derailleur drivetrain (internal-gear hub), there's not much chain
slack to work with: Even if we use the hook to hold the two ends, it get's
tricky to align the sides of the master link.  So, we will use a different
technique...

![Chain Replacement][chain-replace03]

If you've noticed the speck of grime rubbing _between_ the link plates at the
rivet pin: We better put away this chain for cleaning and grab a clean one.

Now, with the new chain: We put it over the rear sprocket but we leave it off
the front chainring.  This way, we can easily _align the master link plates_,
engage the pliers, and...

![Chain Replacement][chain-replace04]

...pull _outwards_ until...

![Chain Replacement][chain-replace05]

...the master link plates firmly _interlock_ with a click.

Noticed the speck of _rust_ around the rivet pin on the new master link?
This corrosion ("Hey now, hey now now" :wink:) resulted from soaking the link
for too long in _denatured  alcohol_, see post [(6)][0f]; we'll discard it
anyway when the chain is due.

We can now put the chain on the top of the chainwheel as far as it goes, lift
the rear wheel, and turn the crank forward...

![Chain Replacement][chain-replace06]

... or, easier, put the chain on the _bottom_ of chainwheel and turn the crank
_backward_, see nice this nice video by [(SB)][11].

We check the chain tension, see post [(4)][0d], and keep the old chain for
cleaning, see [(5)][0e], [(6)][0f]:

![Chain Replacement][chain-replace07]

### Resources

Nice videos and articles on chain replacement: [(CT1)][41], [(PT1)][51],
[(PT)][50].  Segments:
- Routing the chain through a derailleur or chain tensioner:
  [(PT1)][51a]
- [Un-]	locking a master link with a link plier:
  [(CT1)][41a], [(PT1)][51b]
- [Un-]	locking a master link using with a needle-nose plier:
  [(CT1)][41b], [(PT1)][51e]
- [Un-]	locking a tool-less [connex link][90] by hand:
  [Chs][24], [(CT1)][41c]
- [Un-]	locking a connecting rivet with a chain tool:
  [(PT1)][51c]
- [Dis-] connecting chains for single-speed drivetrains:
  [(PT)][52], [(PT2)][54].

[chain-replace01]: /img/2021-08-17-bicycle-chain-replace01.jpg
[chain-replace02]: /img/2021-08-17-bicycle-chain-replace02.jpg
[chain-replace03]: /img/2021-08-17-bicycle-chain-replace03.jpg
[chain-replace04]: /img/2021-08-17-bicycle-chain-replace04.jpg
[chain-replace05]: /img/2021-08-17-bicycle-chain-replace05.jpg
[chain-replace06]: /img/2021-08-17-bicycle-chain-replace06.jpg
[chain-replace07]: /img/2021-08-17-bicycle-chain-replace07.jpg

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[11]: https://www.sheldonbrown.com/chain-drop.html

[24]: https://www.youtube.com/watch?v=TGuBLovhwhc&t=3s

[41]: https://www.youtube.com/watch?v=e-JRkZwuLAs
[41a]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=71s
[41b]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=159s
[41c]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=183s

[50]: https://www.parktool.com/blog/repair-help/chain-replacement-derailleur-bikes

[51]: https://www.youtube.com/watch?v=VdUQKVMPF5I
[51e]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=146s
[51a]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=268s
[51b]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=323s
[51c]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=353s

[52]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes

[53a]: https://www.parktool.com/blog/repair-help/chain-replacement-derailleur-bikes#article-section-5

[54]: https://www.youtube.com/watch?v=88tDcVvS7mU

[90]: https://www.connexchain.com/en/product/connex-link.html

[91a]: https://www.sram.com/en/sram/models/cn-870-a1
[91b]: https://www.sram.com/en/sram/models/cn-plnk-8spd-a1
