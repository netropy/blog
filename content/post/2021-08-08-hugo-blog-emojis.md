---
title: Emojis in Markdown
date: 2021-08-08
tags: ["blog", "website", "Hugo", "Emojis"]
---

Easy to enable in [Hugo Blog][14], see [here][51]: Set _enableEmoji = true_
in the [site configuration][51].

These shortcodes in _lowercase:_ `:Smile:` `:Sunny:` `:Apple:` rendered as:
:smile: :sunny: :apple:

List of emoji shortcodes: [here][52].

[14]: https://pages.gitlab.io/hugo/

[50]: https://gohugo.io/functions/emojify/

[51]: https://gohugo.io/getting-started/configuration/

[52]: https://www.webfx.com/tools/emoji-cheat-sheet/
