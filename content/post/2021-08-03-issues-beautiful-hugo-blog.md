---
title: Problems with the 'Hugo Blog' template for GitLab Pages
date: 2021-08-03
tags: ["blog", "website", "GitLab Pages", "Static Site Generator", "Hugo", "Beautiful Hugo Blog Template"]
---

Alas, our newly created blog site shows some issues, which were not apparent
in the [Beautiful Hugo Blog Template][14]
... <!--more-->

### Summary

1. The front page fails to show any posts, only the _Blog_ page lists them
   -> _Fixable_.
1. Lists are not rendered with bullet points/numbers but displayed as
   continous text -> _Fixable_.
1. Multiple compile warnings for use of deprecated features and failure to get
   translated strings -> _Fixable/Ignored_.
1. The GitLab project for the Beautiful Hugo Blog Template seems inactive and
   might have been abandoned :-(

___Lesson:___ When selecting a blog theme or website template, check the
activity status of the project. 

So far, the issues above turned out fixable, except for a few persistent
compile warnings; but the state of the template project raises worries about
possible incompatibilities with future [Hugo][16] versions.

Details below.

### Testing with different Hugo/SSG versions

[Hugo][16] SSG is a standalone binary, which makes it easy to test other
versions: [Download][30] a release, unpack it, run it locally (`<path>/hugo
server`), and check the site at: <http://localhost:1313/blog/>

### Front page not showing any posts

The [Beautiful Hugo Blog Template][14] runs on _Hugo v0.55.6_ (May 2019),
which does not show this problem.  Version-testing showed the issue connected
to this compile warning:
```code
In the next Hugo version (0.58.0) we will change how $home.Pages behaves. If you want to list all regular pages, replace .Pages or .Data.Pages with .Site.RegularPages in your home page template.
```

Symptom + patch confirmed by this [open issue][31] from 2019-09:
```
--- a/themes/beautifulhugo/layouts/index.html
+++ b/themes/beautifulhugo/layouts/index.html
@@ -11,3 +11,3 @@
         <div class="posts-list">
-          {{ $pag := .Paginate (where .Data.Pages "Type" "post") }}
+          {{ $pag := .Paginate (where .Site.RegularPages "Type" "post") }}
           {{ range $pag.Pages }}
```

### Lists not rendered correctly

Unless an [un-]ordered list is preceded by a blank line in the .md source, it
is rendered as continous text.

Turned out dependent upon the used Hugo version: problem only before _0.60_
(when Hugo switched the [default markdown engine][40] for compliance with
[CommonMark][39]).

### Compile warnings

Multiple warnings:
```code
Failed to get translated string for language "en" and ID "postedOnDate": template: :1:13: executing "" at <.Count>: can't evaluate field Count in type string language "en" and ID "postedOnDate"
```

Symptom + patch confirmed by this [Hugo forum][33]:
```
--- a/themes/beautifulhugo/i18n/en.yaml
+++ b/themes/beautifulhugo/i18n/en.yaml
@@ -4,3 +4,3 @@
 - id: postedOnDate
-  translation: "Posted on {{ .Count }}"
+  translation: "Posted on {{ . }}"
 - id: translationsLabel
```

No fixes found for these two compile warnings (find+replace unsuccessful):
```java
Page.URL is deprecated and will be removed in a future release. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url

Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.
```

### Project _Beautiful Hugo Blog Template_ abandoned?

A look at the commit log and the [past activity][34] of the [Beautiful Hugo
Blog Template][33] project shows that commits petered out in 2018, with a
single merge in 2019.  Among the 17 open [merge requests][35] are many created
year or more ago.

Is the [GitLab Pages][1] community too small compared to other hosters?
Or most folks using other SSGs, templates, or website themes?

[1]: https://docs.gitlab.com/ee/user/project/pages/

[8]: https://docs.gitlab.com/ee/ci/

[14]: https://pages.gitlab.io/hugo/

[16]: https://gohugo.io/

[30]: https://github.com/gohugoio/hugo/tags

[31]: https://gitlab.com/pages/hugo/-/issues/42

[32]: https://discourse.gohugo.io/t/hugo-v0-78-2-regression-executing-at-count-cant-evaluate-field-count-in-type-string/29411

[33]: https://gitlab.com/pages/hugo

[34]: https://gitlab.com/pages/hugo/-/graphs/master

[35]: https://gitlab.com/pages/hugo/-/merge_requests

[37]: https://jekyllthemes.io/github-pages-themes

[38]: https://about.gitlab.com

[39]: https://spec.commonmark.org

[40]: https://gohugo.io/news/0.60.0-relnotes/
