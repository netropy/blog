---
title: Blending old & new tech (5)
subtitle: Conversion from derailleur system to an 8-speed internal-gear hub
date: 2021-08-13
tags: ["DIY", "bicycle", "bicycle renovation", "bicycle conversion", "bicycle internal-gear hub", "bicycle frame hub-spacing", "Shimano Alfine"]
---

[Internal-gear hubs (IGHs)][3] are not exactly new (remembering the 3-speed
hubs some of us had as children).  However, the modern 8- and 11-speed hubs
are a technology that happened since the 1990s -- a true _innovation_.

IGHs seem perfect for commuter and touring bikes.  Not many new bikes, though,
are offered with an IGH in recent years; they also are more expensive than
derailleur gears.

An old bike can be converted from derailleur gears to an IGH -- see this nice
[example][18].  For this, the [spacing][12] between the frame's [forkends][13]
([droputs][14]) must fit the  IGH's axle length (actually, [over-lock-nut
distance][15]).  Also, with _vertical droputs_ one cannot adjust the _chain
tension_, so one might have to install a [tensioner][16].

The challenges in converting my old bike from a 3x7 derailleur system to an
8-speed IGH:
1. The _inside hub spacing_ between the droputs was _5mm_ short for the IGH;
   so, it needed widening by bending the steel frame ([cold-setting][11]).
2. If I messed up the [frame's alignment][17] (or worse, its integrity), I'd
   have to start over and buy a frame from somewhere.
3. I've never done it before.

__In summary:__ Converting my bike to an _8-speed internal-gear hub_ turned
out an excellent decision -- it worked out well and I could do it by myself.

Here, some data points on the _why_, _what_, and _how_
... <!--more-->

### Conversion to an internal gear hub, new drivetrain

The pros & cons of IHGs (per [Sheldon Brown's website][3], which lives on!): \
__+__ much less maintenance \
__+__ more reliable than derailleur systems \
__+__ can shift at a stop (nice in stop-and-go urban traffic) \
__-__ usually heavier than derailleur systems \
__-__ slightly less efficient in some gears than derailleur systems \
__-__ typically, no quick-release axle.

=> An IGH is perfect for a commuter and touring bike! What make & model?

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

[SRAM][5] had stopped selling their IGHs in 2017.  [Rohloff's Speedhub][6]
gets only superb reviews but sells at a price for two new commuter bikes.
[Shimano Alfine][4] also gets excellent reviews, and has been the choice of a
household member, driving an older Alfine SG-S501 8-Speed for the prior 10
years, at ~2000 mi per year -- with strong approval.

=> Alfine is is.  The [8-speed][21] or [11-speed][22]?

Surprisingly, their weight is almost the same: ca. 1670g.  The gear ranges:
- 307% for the 8-speed (Ok for moderate mountain range),
- 409% for the 11-speed (better for all-terrain touring).

However, the complex mechanic of the 11-speed is oil-lubricated.  The hub is a
sealed shell, and an oil change (with a syringe and a hose at the hub's bleed
nipple) is recommended for every two years or 5,000 km.  The 8-speed is
grease-lubricated (it still needs an oil bath at the same service interval),
but owners reported servicing it every 20,000 km.

=> Alfine 8-speed for its alleged robustness.  What about the drivetrain?

After 25 years of wear & tear, the entire drivetrain needed replacing.  We
went with Shimano's [SM-BBR60B][25] bottom bracket, [FC-S501][24]
crankset with 42T, and [CS-S500][23] 18T sprocket.

The 42T-18T gear ratio came out lucky: The chain has perfect length, and
despite vertical drop-outs, there's no need for a chain tensioner.  Also, my
typical gear range in the city is 5-7 (so, 40T-20T would be too low).

=> Could I do it by myself?  Yes!

Ordered at bike stores: the new wheel with the Alfine hub (comes with cable,
shifter), front wheel, an Alfine crankset, a new bottom bracket, chain etc.

The most difficult part: The old frame had an inside [hub spacing][9]
between the forkends (dropouts) for the rear wheel of _130 mm_.  But the
Alfine requires a hub spacing of _135mm_.

The frame being _Chromoly Steel_ (not aluminum), I managed to [cold-set][11]
the forkends: carefully widening them with a car jack for _+5mm_.  Worked!
The Alfine hub slides in and out the vertical forkends; tightening the axle
removes any play.

Finally, I measured the alignment with the [string method][10].

=> The frame's alignment for symmetry: all straight!

![Torpedo Stratos][drivetrain]

P.S.: Switched to _wax_ for chain lubrication, not going back to oil etc.

[drivetrain]: /img/2021-08-09-bicycle-drivetrain.jpg

[3]: https://www.sheldonbrown.com/internal-gears.html

[4]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700.html

[5]: https://www.bicycleretailer.com/international/2017/02/15/sram-ends-sales-internal-gear-hubs

[6]: https://www.rohloff.de/en/products/speedhub

[9]: https://www.sheldonbrown.com/frame-spacing.html

[10]: https://www.sheldonbrown.com/frame-spacing.html#symmetry

[11]: https://www.youtube.com/watch?v=YdibmxBuMy0

[12]: https://www.sheldonbrown.com/gloss_sp-ss.html#spacing

[13]: https://www.sheldonbrown.com/gloss_e-f.html#forkend

[14]: https://www.sheldonbrown.com/gloss_dr-z.html#dropout

[15]: https://www.sheldonbrown.com/gloss_n-o.html#old

[16]: https://www.sheldonbrown.com/gloss_ch.html#chaintensioner

[17]: https://www.sheldonbrown.com/gloss_aa-l.html#alignment

[18]: https://www.sheldonbrown.com/nexus8.shtml#upgrade

[21]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-8.html

[22]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-11.html

[23]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/FC-S501.html

[24]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/CS-S500.html

[25]: https://bike.shimano.com/en-EU/product/component/105-5800/SM-BBR60.html
