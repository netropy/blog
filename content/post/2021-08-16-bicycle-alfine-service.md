---
title: Servicing a Shimano 8-speed Alfine internal-gear hub
subtitle: Photo blog about a DIY oil maintenance
date: 2021-08-16
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle hub", "bicycle internal-gear hub", "Shimano Alfine"]
---

Previous posts described how I converted my 25+ year old touring bicycle from
a derailleur system to an 8-speed [internal-gear hub (IGH)][3] (very pleased).
Now, 3 years and ~3000 km later, the IGH was overdue for an _oil service_ and
a _cone adjustment_.

_Symptoms:_ The service interval has long passed (every 2000km); when spun by
hand, the rear wheel wasn't turning for long; the hub sounded a little bit
louder than it used to.  Not good.

_Challenge:_ The [Alfine Inter 8][21] hub must be disassembled, get an oil
bath, be greased, reassembled, and the cone must be adjusted _just right_.
I've never done that for an IGH.  Happily, experts have posted helpful photo
guides and videos on the interweb.

Advisable as a [DIY][14] project?  Yes!  For two reasons: a) Good luck finding
a nearby bike shop offering Alfine service; labour and materials might add up
to a few hundred bucks.  b) This is recurring maintenance, ideally done 1x per
year.

_Result:_ The Alfine 8-speed hub service worked out well (and I now know how
to do [D.I.Y.][53] next time).

Here, the 7 steps with photos and where I deviated from other guides
... <!--more-->

This is the bike's Shimano [drivetrain][4] with the [8-speed Alfine IGH][21]:

![Torpedo Stratos][drivetrain]

For orientation, the bicycle's _drive_-side (where the chain is) is typically
labeled the _right_ or _R-side_; the _non-drive_-side the _left_ or _L-side_.

The 8-speed Alfine hub's oil and grease maintenance consists of 7 steps: \
[Step 1: Get the materials (transmission fluid, grease)](#step-1-get-the-materials-transmission-fluid-grease) \
[Step 2: Disassemble the internal gear hub](#step-2-disassemble-the-internal-gear-hub) \
[Step 3: Inspect the bearings, cone, and internal unit](#step-3-inspect-the-bearings-cone-and-internal-unit) \
[Step 4: Immerse the internal unit in ATF](#step-4-immerse-the-internal-unit-in-atf) \
[Step 5: Grease the hub shell, the bearing, and all moving parts](#step-5-grease-the-hub-shell-the-bearing-and-all-moving-parts) \
[Step 6: Do the cone adjustment (use your fingertip feeling)](#step-6-do-the-cone-adjustment-use-your-fingertip-feeling) \
[Step 7: Reassemble the internal gear hub in reverse order](#step-7-reassemble-the-internal-gear-hub-in-reverse-order) \
[Summary](#summary)

Also check out these resources:
- photo guides: [english][35], [english][36]
- videos tutorials: [english][32], [german][33], [german][34]
- technical articles: [Sheldon Brown][11a], [ParkTool][44a]

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### Step 1: Get the materials (transmission fluid, grease)

Firstly, note that the _8-speed_ Shimano [Alfine Inter 8][21] is serviced
differently from the _11-speed_ IGHs, [Alfine Inter 11][22] and [Rohloff
Speedhub][25]: other oils, other procedure!

Regardless of the model, the _service interval_ is about: _1x per year_. In
detail: [Rohloff][28] = every 5000 km or 12 months, whichever comes fist;
[Shimano][47b] = at 1000 km, then every 2000 km.

Interestingly, the 11-speed IGHs, _Rohloff Speedhub_ and _Alfine Inter 11_,
are sealed hubs whose oil is changed with a syringe kit (like [this][27] or
[this][28], see photo guides [here][29] or [here][30]): First, the hub is
rinsed with a cleaning oil, then the actual, synthetic and bio-degradable
(Rohloff) oil is filled in.

In contrast, the _Alfine Inter 8_ must be disassembled, gets an oil bath,
and is greased.  [Shimano's Maintenance Oil][24] for 3/7/8-speed IGHs is quite
expensive.  This has led bicyclists looking for alternatives and trying out
other oils, see [here][23], or [automatic transmission fluid (ATF)][37], see
[here][31] and [here][32], as substitute for Shimano's low viscosity oil.

Same with the grease: a [poly-lube grease][39a] or [white lithium grease][39b]
are less expensive than [Shimano's Internal Hub Grease][38].  While lithium
grease [product labels][39d] state that it is not suited for "wheel bearings",
some bicyclists regard this warning as applying to the automotive sector,
whereas bicycle bearings never reach temperatures of [130 Celsius][39c].
Sheldon Brown's [site][3] also recommends _lithium grease_ for the gears and a
sealing layer of _marine grease_ for the outer bearings facing wet climate.

_Why I'm using:_ 1) white lithium grease (for gears), 2) poly-lube (for
bearings), 3) a generic _ATF_ with low viscosity (150s) for the internal
unit's cleaning and lubrication.

![ATF and lithium grease][alfine01]

### Step 2: Disassemble the internal gear hub

Dismount the rear wheel.  Standard procedure, like shown in detail in
[this][40] video (in German).

Disassemble the internal gear hub, from the _R-side_ (drive side).  Most steps
are straight-forward, but see [this][41], [this][42], or [this][43] video for
tricks.

__The only tricky part is prying off the R-side's "snap ring from hell", best
by using two small screwdrivers.__

The "Dust Cap A" on the _R-side_ is best removed using the [Alfine TL-AF10
installation tool][45].  Note that this dust cap is right-hand, so, get a good
grip and gently turn clockwise to open.

The parts ordered left-to-right with tools for the (1) _R-side_, (2) _L-side_,
and (3) internal unit:

![Disassembled Alfine][alfine02]

### Step 3: Inspect the bearings, cone, and internal unit

For in-depth guides of how to do a _hub overhaul with cone adjustment_, see
the excellent articles by [Sheldon Brown][12] and [ParkTool][44a].

The hub shell's _R-side_: relatively clean, but not much grease where it
touches the bearings.

![Hub shell R-side][alfine03]

The hub's _L-side_: not much grease on the bearings; the balls are still
shiny, not dull.  Good!

![Hub shell L-side][alfine04]

The L-side's cone shows no signs of pitting or damage.  Good, not too late!

![Cone][alfine05]

Not much (white or clear) grease on the bearings and cogs of the internal
unit.  About time!

![Internal unit][alfine06]

### Step 4: Immerse the internal unit in ATF

I cut a 1.25 l (1.32 qt) sparkling water bootle as container for the automatic
transmission fluid and poured about 0.5 l of ATF into it.  Per Shimano manual,
their maintenance oil is not supposed to reach the internal unit's top ring.

_In my book_, however: all moving parts ought to be flushed, and afterwards,
all gears and bearings are to be regreased liberally.

So, I repeated this procedure a few times to flush out any dirt or metal
abrasions: Immerse the internal unit in the ATF completely, let the fluid
reach all parts (60s), take out the unit quickly, and let the fluid run
off...

![Internal unit ATF bath][alfine07]

Removing the unit for the last time, let all excess fluid drip off (60s):

![Internal unit ATF drip off][alfine08]

### Step 5: Grease the hub shell, the bearing, and all moving parts

Wipe the hub shell clean with a paper towel and grease the openings.

_The next time:_ Better use (green) _poly-lube_ instead of lithium grease for
the outer bearings.  Also: LESS GREASE!

![Hub shell greased][alfine09]

Oh my, the next photo ain't showing an ice cream cone! :astonished:

_For the next time:_ Grease the gears and the inner bearing rings with _white
lithium lube_; grease the outer bearing with _poly-lube_.  Use a small paint
brush!  Also: LESS GREASE!

![Internal unit greased][alfine10]

Insert the internal unit back into the hub shell so that it fits snugly.
(By the way, the next photo shows the _centerlock ring_ onto which a _disc
brake rotor_ could be mounted.)

Verify that all balls of the _L-side's_ bearing are in place, apply a seal of
poly-lube to the bearing, screw on the conem and tighten all the way down _but
by thumb and finger only_.  Also: LESS GREASE!

![L-side sealed by grease][alfine11]

Verify that all balls of the _R-side's_ bearing are in place, apply a seal of
poly-lube to the bearing, and screw on the dust cap by hand
(counter-clockwise, don't tighten yet).  Also: LESS GREASE!

![R-side sealed by grease][alfine12]

### Step 6: Do the cone adjustment (use your fingertip feeling)

__This step turned out tricky, it took me serveral attempts to get it right.__

The professional advice usually goes like [this][44b]:
- Tighten the cone with a 15mm cone wrench, such as [this][46], until one
  feels a bit of resistence,
- then loosen the cone by 1/4 turn (90 degrees) and hold it there while
  tightening the outer locknut against it firmly to fixate them;
- then test for play by holding axle and moving rim laterally, there shall be
  no knocking sensation.

However, I was encountering two problems:
- A) By loosening the cone by only 1/4 turn, there remained _zero play_ in the
  axle, to the effect that the adjustement was too tight once the hub had been
  clamped tightly in the frame (audible noise when spinning the wheel).
- B) A loud clicking noise occured when turning the wheel while in 5..8th
  gear, as if some part was slipping inside the hub.
- This loud clicking noise had me _worried_, as it was _new and louder_ than
  the normal freewheel ticks in gears 5..8.  Some reports (see [comment][11b]
  by Bruce Dance) warned that an incorrectly assembled unit could have parts
  become adrift and destroy the IGH.
- On the other hand, some noise in gears 5..8 is normal, as per [Alfine
  manual, page 6][52]:
  > Noise may also occur if the gear is positioned at 5 to 8 (internal 8-speed
  > hub) or 7 to 11 (internal 11-speed hub), when the crank is turned backward
  > or when the bicycle is pushed backward. All of these phenomena occur due
  > to the built-in gear-shifting structure and are not the failure of the
  > internal components.
- Happily, I got this noise resolved by rocking the wheel a bit while
  tightening the cone!

This procedure worked for me:
1. I tightened the _L-side_ cone _by thumb and finger only_, that is, w/o
   force;
1. took the wheel by the axle and rolled it backward and forward on the floor,
   rocking the wheel a bit laterally;
1. felt parts slipping into place which allowed me to further tighten the cone
   a few turns _by thumb and finger only_;
1. repeated above steps about 2..3x times until I could no longer tightening
   the cone _by thumb and finger only_;
1. verified that the loud clicking noise was gone when spinning the wheel in
   either direction;
1. then loosened the cone a bit (1/8 turn) to give it a minimal amount of
   play.

Finally, I fixated the cone by holding it in place with a 15mm cone wrench
while _firmly_ tightening the outer locknut against it with a 17mm wrench.

![Tighten cone and locknut][alfine13]

### Step 7: Reassemble the internal gear hub in reverse order

Final checks!  Clamp the hub _tightly_ in the frame and verify that
- the wheel turns _quietly and freely_,
- there's _zero play_ (pull on rim laterally, no knocking sensation).

Dismount the rear wheel again, and tighten the _R-side_ dust cap with the
[installation tool][45]:

![Tighten cone and locknut][alfine14]

Finally, reassemble the hub in reverse order of step #2 above (check video
links there).

__The tricky part, this time, is prying *on* the R-side's "snap ring from
hell".__

It took me ~25 minutes of trying until the ring snapped into place, using 2
screwdrivers and leverage to bend the ring open a bit... :angry:

Voilà -- The Alfine 8-speed hub runs smoothly and is good to go for another
couple of years!

### Summary

Internal-gear hubs (IGHs) are a great piece of bicycle technology: They are
robust and need less maintenance than derailleur gears, which are fully
exposed to dirt and weather.  Still, the recommended service interval for IGHs
is about: _1x per year_.

The 11-speed [Rohloff Speedhub][25] and [Shimano Alfine Inter 11][26] receive
an oil change using a syringe and tube.  In contrast, the [8-speed Alfine][21]
requires: disassembling, an oil bath, greasing, and reassembling.

The result: _The 8-speed Alfine hub service turned out doable by myself!_
:smiley:

Not many materials or tools are needed -- but some patience, if done for the
first time.  Out of the 7 steps, 3 took me some practicing: #2 (disassemble),
#6 (cone adjustment), and #7 (reassemble).  Will become routine quickly.

So, I hope these notes and photos are of help to you (as they've been to me).
Also check out the other resources (videos, articles) listed in this blog!


[drivetrain]: /img/2021-08-09-bicycle-drivetrain.jpg
[alfine01]: /img/2021-08-16-bicycle-alfine-maintenance01.jpg
[alfine02]: /img/2021-08-16-bicycle-alfine-maintenance02.jpg
[alfine03]: /img/2021-08-16-bicycle-alfine-maintenance03.jpg
[alfine04]: /img/2021-08-16-bicycle-alfine-maintenance04.jpg
[alfine05]: /img/2021-08-16-bicycle-alfine-maintenance05.jpg
[alfine06]: /img/2021-08-16-bicycle-alfine-maintenance06.jpg
[alfine07]: /img/2021-08-16-bicycle-alfine-maintenance07.jpg
[alfine08]: /img/2021-08-16-bicycle-alfine-maintenance08.jpg
[alfine09]: /img/2021-08-16-bicycle-alfine-maintenance09.jpg
[alfine10]: /img/2021-08-16-bicycle-alfine-maintenance10.jpg
[alfine11]: /img/2021-08-16-bicycle-alfine-maintenance11.jpg
[alfine12]: /img/2021-08-16-bicycle-alfine-maintenance12.jpg
[alfine13]: /img/2021-08-16-bicycle-alfine-maintenance13.jpg
[alfine14]: /img/2021-08-16-bicycle-alfine-maintenance14.jpg

[3]: https://www.sheldonbrown.com/internal-gears.html

[4]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700.html

[11a]: https://www.sheldonbrown.com/nexus8.shtml

[11b]: https://www.sheldonbrown.com/nexus8.shtml#instructions

[12]: https://www.sheldonbrown.com/cone-adjustment.html

[14]: https://en.wikipedia.org/wiki/Do_it_yourself

[21]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-8.html

[22]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-11.html

[23]: https://phil-wood-co.myshopify.com/products/4-oz-bottle-of-phil-tenacious-oil

[24]: https://www.universalcycles.com/shopping/product_details.php?id=40041

[25]: https://www.rohloff.de/en/products/speedhub

[26]: https://bikeshed.johnhoogstrate.nl/bicycle/drivetrain/shimano_alfine_inter_11/

[27]: https://www.universalcycles.com/shopping/product_details.php?id=42539

[28]: https://www.rohloff.de/en/products/maintenance

[29]: https://bikeshed.johnhoogstrate.nl/bicycle/drivetrain/shimano_alfine_inter_11/oil_change/

[30]: https://thelazyrando.wordpress.com/2013/05/15/shimano-alfine-11-igh-oil-change/

[31]: https://www.mtbr.com/threads/alfine-grease-oil-alternatives.1004837/

[32]: https://www.youtube.com/watch?v=lFOMD2q5-o4

[33]: https://www.youtube.com/watch?v=XqbEbPqKB8o

[34]: https://www.youtube.com/watch?v=pB_s4lP_xk4

[35]: https://hokkaidowilds.org/shimano-alfine-8-speed-internal-gear-hub-oil-bath

[36]: http://thegoldenwrench.blogspot.com/2011/01/servicing-of-internally-geared-hub.html

[37]: https://en.wikipedia.org/wiki/Automatic_transmission_fluid

[38]: https://www.amazon.com/Shimano-Inner-Hub-Grease-G/dp/B00B709EBS/

[39a]: https://www.parktool.com/product/polylube-1000-lubricant-tube-ppl-1

[39b]: https://knowhow.napaonline.com/know-how-notes-different-types-of-grease-and-where-to-use-them/

[39c]: https://q20.co.za/what-is-white-lithium-grease-and-when-to-use-it/

[39d]: https://lucasoil.com/products/grease/white-lithium-grease

[40]: https://www.youtube.com/watch?v=3HzsYZxV2zA

[41]: https://www.youtube.com/watch?v=DCmTRVgDjO8

[42]: https://www.youtube.com/watch?v=Dz-3PfboKTw

[43]: https://www.youtube.com/watch?v=TZc6HMirzG8

[44a]: https://www.parktool.com/blog/repair-help/hub-overhaul-and-adjustment

[44b]: https://www.parktool.com/blog/repair-help/hub-overhaul-and-adjustment#article-section-7

[45]: https://www.modernbike.com/shimano-tl-af10-alfine-right-hand-dust-cap-tool

[46]: https://www.parktool.com/product/double-ended-cone-wrench-dcw-4

[47a]: https://si.shimano.com/#/en/search/Series?name=ALFINE&generation=Inter-8

[47b]: https://si.shimano.com/api/publish/storage/pdf/en/dm/SG0004/DM-SG0004-06-ENG.pdf

[52]: https://si.shimano.com/#/en/DM/SG0004

[53]: https://en.wikipedia.org/wiki/Do_it_yourself
