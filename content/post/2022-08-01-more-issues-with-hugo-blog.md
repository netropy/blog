---
title: More problems with the 'Hugo Blog' template for GitLab Pages
date: 2022-08-01
tags: ["blog", "website", "GitLab Pages", "Hugo", "Beautiful Hugo Blog Template"]
---

This "how to create a blog site" series from a year ago concluded back then:
- [GitLab Pages][12] with a [Static Site Generator (SSG)][16] turned out a [good
  choice][0a]: write content in [markdown][17], fast compile times, fast page
  loading times.
- The [Hugo SSG][13a] with the [Beautiful Hugo Blog Template][14a] gave an easy
  start: nice & functional design, compared well against other [builtin][0b]
  or [external][0d] GitLab themes.
- Yet, the Hugo Blog Template also showed [issues][0c]: minor patches
  required, warnings with newer versions of Hugo, and (worst) it appeared
  abandoned since 2019.

Alas, the list of issues has grown over time.  Investigating and fixing the
Hugo Blog Template's code is not something I'd planned to do.  But happily a 
series of commits occured in spring of 2022 switching to a new theme with a
design called ["Ananke"][10a].

So, how does the new Hugo theme for GitLab Pages look like?  How easy is the
setup on GitLab and locally?  How much effort to customize the new theme as my
blog site?

The out-of-the-box experience turned out disappointing: a more complex remote
and local installation, rendering issues with the initial template, and a
different and less blog-centric design -- all suggesting some effort (at least
to me) to customize the new Hugo template.

Hence, I'll stick a with the old Hugo blog template based on the [Beautiful
Hugo][11a] theme and try to work around issues as they come
... <!--more-->

### Issue: Incorrect alignment of table colums

This table has some columns that are center- and right-aligned...
```
  | cycles* | °F | °C | time | bottom rack | 1/3 rack | 2/3 rack | comments |
  |:---:|:---:|:---:|---:|:---:|:---:|:---:|:---|
  | 1x | 500 | 260 | 8m | 0.4l water | + | + | start heavier loaves on 2/3 rack, better rise |
  ...
```

...but local builds show all of them left-aligned:

![Old Hugo Incorrect table rendering][0e]

Workaround: Current local Hugo version for Debian/Ubuntu/Mint is _0.64.0_;
download and install newer version _v0.91.0_ (but not higher, see next).

### Issue: Broken GitLab CI/CD build with 'hugo:latest'

Local build against hugo v0.92.0 (or later) reports errors:
```
$ hugo serve
  hugo v0.92.0-B3549403 linux/amd64 BuildDate=2022-01-12T08:23:18Z VendorInfo=gohugoio
  ERROR 2022/08/04 19:01:40 Page.URL is deprecated and will be removed in Hugo 0.93.0. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
  ERROR 2022/08/04 19:01:40 Page.Hugo is deprecated and will be removed in Hugo 0.93.0. Use the global hugo function.
```

Whereas, build against hugo v0.91.0 (or earlier) only warns:
```
$ hugo serve
hugo v0.91.0-D1DC0E9A linux/amd64 BuildDate=2021-12-17T09:50:20Z VendorInfo=gohugoio
  WARN 2022/08/04 19:04:00 Page.URL is deprecated and will be removed in a future release. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
  WARN 2022/08/04 19:04:00 Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.
```

Apparently, reported as: https://gitlab.com/pages/hugo/-/issues/69

Workaround: freeze Hugo version in `<blog dir>/.gitlab-ci.yml` to:
```
#image: registry.gitlab.com/pages/hugo:latest
image: registry.gitlab.com/pages/hugo:0.91.0
#image: registry.gitlab.com/pages/hugo:0.55.6
```

Interestingly, version _0.91.0_ is not listed among the available docker
images at: https://gitlab.com/pages/hugo/container_registry

Works, not great, but good enough for a while.

### New project activity, new theme design "Ananke"

After 2 years of (near?) inactivity, the [GitLab Hugo Template][14a] saw a
series of commits in spring of 2022:

![New commits][0f]

The new theme design ["Ananke"][10a] looks nice & clean, has a few new
features (contact form).  But is less blog-centric (missing "Tags" menu).
A screenshot as it looks in August 2022:

![Hugo's new "Ananke" theme][0g]

### Building locally and on GitLab

Straight-forward to fork the latest [GitLab Pages Hugo template][14a].  The
instructions show a few more steps that before to build the site locally.  A
quick test was not successful (neither with current 0.101.0 nor with older
versions), for example:
```
blog-1$ hugo version
Hugo Static Site Generator v0.68.3/extended linux/amd64 BuildDate: 2020-03-25T06:15:45Z
blog-1$ hugo mod init gitlab.com/pages/hugo
blog-1$ hugo mod get -u github.com/theNewDynamic/gohugo-theme-ananke
blog-1$ hugo server
Error: module "github.com/theNewDynamic/gohugo-theme-ananke" not found; either add it as a Hugo Module or store it in "/home/mz/@mzmac/gitlab/blog-1/themes".: module does not exist
```

Hugo is a standalone binary and should not be affected by the installed
version of [Go][15] (upgrading to go1.14, go1.18, go1.19 made no difference).

Also, running the GitLab pipeline out-of-the-box rendered the website
incorrectly:

![Hugo Ananke out-of-the-box][0h]

### Outlook

The GitLab Pages templates appear to consist of these two parts:
1. integration code for GitLab CI/CD and Hugo (_.gitlab-ci.yml_)
2. theme and customizations (_blog/themes/_, _config.toml_ etc)

The repository for the [Beautiful Hugo][11a] theme shows ongoing activity.
Possibly, I can selectively update my _blog/themes/_ with a newer version.
This means, though, to maintain my own themes code from then on.

More pointers for future reference: \
[Ananke][10b] \
[Hugo][13b], [Hugo in GitLab Pages][14b], [More Hugo Themes][10c]

[0a]: /post/2021-08-02-gitlab-pages-with-beautiful-hugo-blog/
[0b]: /post/2021-08-03-issues-beautiful-hugo-blog/
[0c]: /post/2021-08-04-gitlab-pages-with-other-builtin-templates/
[0d]: /post/2021-08-05-gitlab-pages-with-external-templates/
[0e]: /img/2022-08-01.old-hugo-incorrect-table-rendering.jpg
[0f]: /img/2022-08-01.new-hugo-commits.jpg
[0g]: /img/2022-08-01.new-hugo-ananke-theme.jpg
[0h]: /img/2022-08-01.new-hugo-ananke-theme-out-of-box.jpg

[10a]: https://gohugo-ananke-theme-demo.netlify.app/
[10b]: https://github.com/theNewDynamic/gohugo-theme-ananke
[10c]: https://master--hugothemes.netlify.app/tags/blog/

[11a]: https://github.com/halogenica/beautifulhugo

[12]: https://docs.gitlab.com/ee/user/project/pages/

[13a]: https://gohugo.io/
[13b]: https://github.com/gohugoio/hugo

[14a]: https://gitlab.com/pages/hugo
[14b]: https://pages.gitlab.io/hugo/

[15]: https://go.dev

[16]: https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/

[17]: https://en.wikipedia.org/wiki/Markdown
