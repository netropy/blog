---
title: Resources on all things bicycle
subtitle: DIY bicycle work made easy
date: 2023-09-01
tags: ["DIY", "bicycle", "bicycle maintenance"]
---

These expert sites are a my go-to resources for bicycle maintenance:

| ref | authors |
|:---:|:---|
| (SB) | [Sheldon Brown][1a], [Bicycle Glossary][1b] |
| (PT) | [Park Tool Repair Help][2a], [YouTube][2b] |
| (OZC) | [oz cycle YouTube][4] |
| (RJ) | [RJ The Bike Guy][6a], [YouTube][6b] |
| (BG) | [Bike Gremlin][5a], [YouTube][5b], [Bicycle Glossary][5c] |
| (BR) | [BikeRide][22a], [YouTube][22b] |
| (CMA) | [Cycle Maintenance Academy][3a], [YouTube][3b] |
| (WP) | [Wikipedia: Bicycle Parts][7] |

These tech resources and how-to videos enable every pedaler to [D.I.Y.][23]
all bicycle work: service, repairs, or upgrades -- with fun :smile:

Some notes on these resources, plus a few other links
... <!--more-->

_Sheldon Brown_.  The [(SB) glossary][1b] is a life-saver when ordering parts
or searching for articles, especially for non-native English speakers.  The
[(SB)][1a] website covers nearly all there is to know about bicycle old and
new technology -- with dedication, detail, and in a [DIY][1e]-spirit (\*).

Sadly, [Sheldon Brown][1d] passed away in 2008.  Fortunately, the invaluable
[(SB)][1a] website wih the [SheldonBrownBlog][1c] are continued by expert and
friend John Allen.

_Park Tool_ is a [shop][2c] that manufactures precision tools and lubrication
products; their DIY help articles and videos are informative and methodical --
explained by humorous, no-nonsense Midwesteners.

Park Tool's [Big Blue Book of Bicycle Repair][2d] (as ebook or print copy) is
a collection of illustrated howto-articles (also on their website).

_oz cycle_ is a site by Steven Leffanue: fun to watch, excellent technical
explanations, pragmatic and cost-saving tips.  An Aussie who tries out and
reports from expertise.

_RJ The Bike Guy_ is a site by a gifted mechanic who loves fixing and
restoring used bicycles.  To any technical problem, he finds a pragmatic
solution with taste and respect for each bike's era.

_Bike Gremlin_ is an in-depth site with detailed how-to videos by Relja
Novović from Serbia, dedicated to DIY bike repair, buying tips.

_BikeRide_ is a community page founded by Brent Soderberg as an events
calendar for rides and races in the U.S.  It also features a user forum, tech
reviews -- and excellent, straight-to-the-point maintenance articles and
videos created by Alex Ramon of formerly BicycleTutor.com ("nobody ever needs
a new bike" [(2012)][22c]).

_Cycle Maintenance Academy_ is a team of quirky mechanics and instructors from
Manchester, UK (an opportunity to learn some British English terminology of
parts and tools).  Their extensive list of articles and videos is a treasure
trove, especially for older bike technology.

_Wikipedia's_ community-edited pages are extensive and detailed on bicycle
parts and technology; however, information on maintenance and "how-to"
instructions seems outside their scope.

The unrelated [wikiHow][18] pages on bicycle maintenance only cover most basic
tasks (at this time).

A true expert book ("any good bicycle shop will have a copy"
([John S. Allen](http://john-s-allen.com)) seems to be: \
[Sutherland's Handbook for Bicycle Mechanics, 7th edition][17a]

There are countless other blogs and videos.  I also found helpful:

| sites |
|:---|
| [BikeForums][11a]: FAQ |
| [Diamondback Bicycles][16a]: how-to videos |
| [bikeradar][19]: how-to articles |
| [Loner Bikes][8a]: how-to videos |
| [GCN][9a], [GCN][9b], [GCN][9c]: how-to videos |
| [xfahrrad][10a]: how-to videos (German) |
| [road.cc][20]: how-to articles, tech reviews |
| [explainmybike][12a]: bicycle tire labeling |
| [rec.bicycles.tech][15a]: FAQ by Jobst Brandt et al |
| [CyclingSavvy][21]: articles by American Bicycling Education |
| defunct: |
| [CyclingTips][13a] |

P.S.: \* Is it [DIY][23] or [D.I.Y.][14a]? :wink:

[1a]: https://www.sheldonbrown.com
[1b]: https://www.sheldonbrown.com/glossary.html
[1c]: https://sheldonbrown.com/blog/
[1d]: https://en.wikipedia.org/wiki/Sheldon_Brown_(bicycle_mechanic)
[1e]: https://www.sheldonbrown.com/diy.html

[2a]: https://www.parktool.com/blog/repair-help
[2b]: https://www.youtube.com/c/parktool/videos
[2c]: https://www.parktool.com/category/tools
[2d]: https://www.parktool.com/en-us/product/big-blue-book-of-bicycle-repair-4th-edition-bbb-4

[3a]: https://www.cyclemaintenanceacademy.com/
[3b]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[4]: https://www.youtube.com/c/stevenleffanue/videos

[5a]: https://bike.bikegremlin.com
[5b]: https://www.youtube.com/c/BikeGremlinTube/videos
[5c]: https://bike.bikegremlin.com/168/bicycle-glossary/

[6a]: http://www.rjthebikeguy.com/
[6b]: https://www.youtube.com/c/RJTheBikeGuy/videos

[7]: https://en.wikipedia.org/wiki/Bicycle#Parts

[8a]: https://www.youtube.com/@lonerbikes/videos

[9a]: https://www.youtube.com/playlist?list=PLH49XyD2ofsXzMTbrSNLAXnQo0pSyDEb6
[9b]: https://www.youtube.com/playlist?list=PLUdAMlZtaV1333Cy1QnIZwqDXj1q0Ooyy
[9c]: https://www.youtube.com/playlist?list=PLUdAMlZtaV12XzL1k3PhB0lZyRkF5tfDO

[10a]: https://www.youtube.com/@xfahrrad/videos

[11a]: https://www.bikeforums.net/

[12a]: https://explainmybike.com/

[13a]: https://cyclingtips.com/

[14a]: https://www.youtube.com/watch?v=_7LrEB63li8

[15a]: https://www.sheldonbrown.com/brandt/index.html

[16a]: https://www.youtube.com/playlist?list=PLt1vXTcxRuLr84gyBl0UggPQSWnJrt7eE

[17a]: https://www.sutherlandsbicycle.com/products/repair-manual-7th-edition-book-cd

[18]: https://www.wikihow.com/Category:Bicycles

[19]: https://www.bikeradar.com/advice/workshop

[20]: https://road.cc/features

[21]: https://cyclingsavvy.org/articles/

[22a]: https://www.bikeride.com/guide/
[22b]: https://www.youtube.com/@BikeRidecom/playlists
[22c]: https://www.seattletimes.com/business/good-deals-on-used-bikes-but-first-kick-the-tires/

[23]: https://en.wikipedia.org/wiki/Do_it_yourself
