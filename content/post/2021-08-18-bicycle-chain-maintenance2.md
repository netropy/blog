---
title: About bicycle chains (2)
subtitle: Chain wear and essential chain tools
date: 2021-08-18
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain", "bicycle tools"]
---

When a bicycle chain wears out -- it gets visibly longer.  This _chain
stretch_ is not caused by metal plates extending under tension but by
abbrasion of the pins and sleeves inside the chain.  The main cause is dirt
and grime that got inside the chain by lubrication.

Carrying on with an elongated chain will soon wear out the cogs and chainrings
to the point were all need replacing; conversely, worn-out sprockets will wear
out a new chain ahead of time.  So, the chain is the key factor of wear on
the drivetrain.

Happily, it only takes seconds to measure the chain wear with a _chain
checker_.  So, this is an inexpensive tool, which sooner or later every
bicyclist wants to use.

What mileage can be expected of chains, cogs, and chainrings?  Multiple
factors are at play, chiefly, how well the chain is kept.  In this post, we
sum up a few expert estimates and form mileage expections for our touring
bikes with internal-gear hubs and wax-lubricated chains.

Post [(1)][0a] recapped the two types of chain connectors, [master links][20c]
and [connecting rivets][51a].  Here, we detail the tools for them: a _link plier_ to
lock and unlock master links; a _chain tool_ to drive in connecting rivets to
connect a chain, or to drive out regular rivet pins to split it.

Let's go: Q&A about chain and sprocket wear, essential tools for bicyclists
... <!--more-->

Topics: \
[Chain wear: What is chain stretch?](#chain-wear-what-is-chain-stretch) \
[How to measure chain stretch?](#how-to-measure-chain-stretch) \
[When should we replace a chain?](#when-should-we-replace-a-chain) \
[How long do chains, sprockets, chainrings last?](#how-long-do-chains-sprockets-chainrings-last) \
[Essential chain tools](#essential-chain-tools) \
[Pocket-size chain tools](#pocket-size-chain-tools) \
[Master links without tools](#master-links-without-tools)

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### Chain wear: What is chain stretch?

Chain wear shows becomes visible as elongation of the chain, called _chain
stretch_ or _chain pitch_.  Small numbers add up: An elongation of 0.25 mm or
1% at each link extends the entire bicycle chain by ~2.9 cm -- see these nice,
brief video segments [(PT3)][57b], [(PT4)][59a].

The misalignment between chain links and sprocket teeths then _wears out_ the
cogs and the chainwheel, for details see [(SB3)][15], [(BG)][21].

The cause of chain stretch is dirt grinding away the metal _inside_ the chain
[(SB)][14]:

> Riders often speak of "chain stretch," a technically misleading and
> incorrect term.  Chains do not stretch, in the dictionary sense, by
> elongating the metal by tension.  Chains lengthen because their hinge pins
> and sleeves wear.  Chain wear is caused almost exclusively by road grit that
> enters the chain when it is oiled.  Jobst Brandt

_Resources:_ For an excellent, visual explanation of chain stretch and the
other kinds of chain wear, see this video segment by [(OZC1)][30a].

### How to measure chain stretch?

Many are familiar with a simple _chain checker_ or _chain wear indicator_,
like this one (which came as part of a tool set).  When the checker sits flat
on the chain it indicates wear of 0.75% or 1%, respectively:

![Simple Chain Checker][chain-tools01]

My preference is for this [CC-2 (PT)][62a] chain checker, which shows an
analog range that also indicates chain wear of 0.5% (or lower):

![Better Chain Checker][chain-tools02]

It only takes _seconds_ -- should be routine!  So, this waxed chain is still
good after ~2k km.

For other methods to measure chain stretch, including a manual test, see this
nice video [(PT4)][59b] and articles [(PT)][58], [(CT2)][42].

_Resources:_ For a product comparison of chain wear tools, see [(CT2)][42].
On the [CC-2 (PT)][62a] chain checker: "It's very easy to force a false
reading from it" -- no information on how to avoid this.  _My experience:_ I
find the CC-2 checker easy to use and producing repeatable readings.

### When should we replace a chain?

Expert recommendations vary a bit -- [(OZC1)][30c]: 0.5% always; [(CT2)][42]:
0.5% for _10+_ speeds;  [(PT4)][59c]: 0.5% for _11+_ speeds, 0.75% for _10-_
speeds, and 1% for non-derailleurs.

_I'm going with:_

|wear | #sprockets, chain |
|---:|---|
| 0.5% | 9+ speeds |
| 0.75% | 6-8 speeds or 3/32" chain |
| 1.0% | single sprocket or 1/8" chain |

No data, just my preference.

### How long do chains, sprockets, chainrings last?

A worn chain soon wears out the sprockets and the chainrings to the point were
all need replacing.  So, the chain is the key factor of wear on the drivetrain
[(OZC1)][30b].

As a quick test for when the rear cogs need replacing, [(OZC1)][30e] suggests:
When a _new_ chain on the _old_ cogs can be felt jumping under load.

For the expected lifespan, [(OZC2)][31a] estimates
- 2-3k km lifetime for an oil-lubricated chain before it reaches a 0.5% wear
  mark,
- 15k km for a well-kept, wax-lubricated chain, see post [(6)][0f],
- 45k km for the cassette, or every 3 wax-lubricated chains.

[Update 2024-10:] A test by [(BG2)][21a] found a lifespan (until chain stretch
reached 0.5%) of
- 2k km for an inexpensive 6/7-speed chain,
- 4.5k km for a quality 8-speed chain,
- 4.4k km for a high-end 10-speed chain.

[(CT)][46b] quotes experts: 30k km for the cassette and chainrings with a
waxed chain.  [(CT2)][42] quotes: 50k km for the chainrings under best
circumstances.  [(CT2)][42] cautions:

> Chain wear is not linear.   If you get 3,000km of riding to .25% wear,
> you’ll unlikely get another 3,000km by the time the chain reaches .5% wear.

_My expectation:_ For our touring bikes (internal-gear hub, on-road, dry
conditions) with an 8-speed quality chain...

| If chain kept clean & wax-lubricated: |
|---|
| 1 chain ~ 5k km |
| 1 sprocket ~ 15k km or 3 chains |
| 1 chainring ~ 30k km or 2 sprockets |

No data, just my guesses (cautious about the chain, optimistic about the
chainring).  [update 2024-10:] The SRAM [PC-870][95b] master link chain seems
to reach a lifespan of 5k km, but not the cheaper models PC-850 and PC-830.

_Addendum:_ For non-derailleur drivetrains, [(SB)][16] suggests that its
lifespan can be prolonged if one 1) uses even numbers of teeth on the
sprockets, and 2) keeps the chain always set the same way on them.

### Essential chain tools

For master link chains, a _link plier_ or _chain plier_ (right) makes it
extremely easy to unlock and lock a master link; see post [(3)][0c] for
photos.  It's worth the small expense, since a _needle-nose plier_ tends to
slip off.  An alternative are _tool-less_ master links, see next topic.

A _chain breaker_, _chain cutter_, or _chain tool_ (left) is used to drive out
a rivet pin to split a chain.  It is also used to drive in a connecting rivet
pin.  This tool here comes with a spare tip (pin) and a small instruction
card.

![Chain Breaker and Link Plier][chain-tools03]

This chain tool also comes with a _hook_, which clips on to the backside of
the handle and is convenient to hold in place the two loose ends of a chain:

![Chain Breaker and Link Plier][chain-tools04]

The use of these tools -- [link plier][63d], [chain breaker][63e], and
[needle-nose plier][63c] (for some track chain master links) -- is simple; see
these articles [(PT)][51a], [(PT)][54a] and excellent video segments:
- disconnect a master link or drive out a rivet [(PT1)][52a], [(PT2)][55a],
  [(PT2)][55c],
- connect a master link or a connecting rivet [(CT1)][40a], [(PT1)][52b],
  [(PT2)][55b], [(PT2)][55d].

_Resources:_ For a product comparison of chain tools, see [(CT)][44].

### Pocket-size chain tools

For chain repairs on the ride, tool vendors offer mini chain tools are
available, which weigh between 46-85g (1.5-3 oz), for example: [(PT)][62b],
[Connex][93b] ([manual][93c]), [Topeak][96b], [Topeak][96c], [CleverS][98b],
[CleverS][98c], [CleverS][98d].  For chain repair on the road, see this nice
video and article [(PT)][61], or [(PT)][60].

Some multi-function tools carry emergency chain breakers, for example:
[(PT)][64], [Topeak][96a].

Link pliers are also available as pocket version, for example: [Topeak][96d],
[WolfT][97a], [WolfT][97b], [CleverS][98a].  See this nice video segment
[(CT1)][40c].

_My experience:_ So far, the master link chains on our bikes have never given
us a problem.

### Master links without tools

A link plier makes it easy to unlock and lock a master link, but there are
_tricks_ to do without.

_Unlock:_ See these video segments for how to open a master link with
- a piece of brake cable or cord [(CT1)][40e], [(SB1)][11b];
- a piece of wire from a bracket and a plier [Chs][24b];
- a plier [Kenney][23], [(CT1)][40d].

_Lock:_ It is easier to close a master link without a link plier: Just pull
the rear break and push the crank forward; see these short video clips
[Chs][24c], [(CT1)][40b].

Still, it gets better: Some chain manufacturers offer _tool-less_ master
links.

__SRAM__

SRAM _claims_ that some [PowerLinks][95] can be unlocked by hand, for example,
the [8SPD][95a] link.  Alas, the product page do not say _how_ to do so:

![SRAM CN-PLNK-8SPD-A1][sram8spd]

Some YouTube clips, like [MTBtips][25], show that by squeezing the master link
plates they can be shifted against another.

_My experience:_ Negative - as shown in this clip by [Chs][24a], the 8-speed
link's plates don't shift.  The SRAM [8SPD][95a] link _interlocks tightly_ on
a SRAM [PC-870][95b] 8-speed chain.  And if it weren't, I'd consider it a
safety risk and replace it.

__Connex, KMC__

There are chain manufacturers that offer _tool-less_ connector links, which
can be unlocked and locked _by hand_.  Known brands are [Connex][93a] by
Wippermann and [KMC][94b] ([KMC 1s][94a]).

The experience is like in this video clip by [Chs][24] - 3 seconds, nice!

[drivetrain]: /img/2021-08-09-bicycle-drivetrain.jpg
[sram8spd]: /img/2021-08-17-bicycle-chain-sram-powerlink.jpg

[chain-tools01]: /img/2021-08-17-bicycle-chain-tools01.jpg
[chain-tools02]: /img/2021-08-17-bicycle-chain-tools02.jpg
[chain-tools03]: /img/2021-08-17-bicycle-chain-tools03.jpg
[chain-tools04]: /img/2021-08-17-bicycle-chain-tools04.jpg

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[11b]: https://www.sheldonbrown.com/chains.html#connect

[14]: https://www.sheldonbrown.com/chain-wear.html

[15]: https://www.sheldonbrown.com/brandt/chain-care.html

[16]: https://sheldonbrown.com/chain-life.html

[20c]: https://en.wikipedia.org/wiki/Master_link

[21]: https://bike.bikegremlin.com/3306/bicycle-chain-wear-elongation/
[21a]: https://bike.bikegremlin.com/14422/bicycle-chain-durability-test/

[22]: https://bicyclethailand.com/identifying-shimano-chain-connecting-rivets/

[23]: https://www.youtube.com/watch?v=2evVtKy7SoQ&t=49s

[24]: https://www.youtube.com/watch?v=TGuBLovhwhc&t=3s
[24a]: https://www.youtube.com/watch?v=TGuBLovhwhc&t=13s
[24b]: https://www.youtube.com/watch?v=TGuBLovhwhc&t=26s
[24c]: https://www.youtube.com/watch?v=TGuBLovhwhc&t=55s

[25]: https://www.youtube.com/watch?v=1GnlOS5Bgn0&t=36s

[30a]: https://www.youtube.com/watch?v=CZz2gdMCp2A&t=99s
[30b]: https://www.youtube.com/watch?v=CZz2gdMCp2A&t=843s
[30c]: https://www.youtube.com/watch?v=CZz2gdMCp2A&t=904s
[30e]: https://www.youtube.com/watch?v=CZz2gdMCp2A&t=999s

[31a]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=327s

[40a]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=71s
[40b]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=98s
[40c]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=138s
[40d]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=159s
[40e]: https://www.youtube.com/watch?v=e-JRkZwuLAs&t=169s

[42]: https://cyclingtips.com/2019/08/bicycle-chain-wear-and-checking-for-it/

[44]: https://cyclingtips.com/2018/10/the-best-chain-breaker-home-mechanic-15-reviewed/

[46b]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#waxing

[51a]: https://www.parktool.com/blog/repair-help/chain-replacement-derailleur-bikes#article-section-5

[52a]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=126s
[52b]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=324s

[54a]: https://www.parktool.com/blog/repair-help/chain-replacement-single-speed-bikes#article-section-2

[55a]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=57s
[55b]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=216s
[55c]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=490s
[55d]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=517s

[57b]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=212s

[58]: https://www.parktool.com/blog/repair-help/when-to-replace-a-chain-on-a-bicycle

[59a]: https://www.youtube.com/watch?v=gXd-3UnqoaM&t=6s
[59b]: https://www.youtube.com/watch?v=gXd-3UnqoaM&t=41s
[59c]: https://www.youtube.com/watch?v=gXd-3UnqoaM&t=167s

[60]: https://www.parktool.com/blog/repair-help/on-the-ride-chain-repair

[61]: https://www.youtube.com/watch?v=HpUCCrgugQE

[62a]: https://www.parktool.com/product/chain-checker-cc-2
[62b]: https://www.parktool.com/product/mini-chain-brute-chain-tool-ct-5
[63c]: https://www.parktool.com/product/needle-nose-pliers-np-6
[63d]: https://www.parktool.com/product/master-link-pliers-mlp-1-2
[63e]: https://www.parktool.com/product/chain-tool-ct-3-2

[64]: https://www.parktool.com/category/multi-tools

[93a]: https://www.connexchain.com/en/product/connex-link.html
[93b]: https://www.connexchain.com/en/product/connex-chain-tool.html
[93c]: https://www.connexchain.com/fileadmin/user_upload/Service_Downloads/Anleitungen/Bedienungsanleitung_Tool_2015_englisch.pdf

[94a]: https://www.kmcchain.com/en/series/chain-connector-single-speed
[94b]: https://www.kmcchain.com/en/series/chain-connector-12s-11s-10s-9s-8s-7s-6s-speed

[95]: https://www.sram.com/en/search?t=powerlink
[95a]: https://www.sram.com/en/sram/models/cn-plnk-8spd-a1
[95b]: https://www.sram.com/en/sram/models/cn-870-a1

[96a]: https://www.topeak.com/us/en/products/75-Mini-Tools
[96b]: https://www.topeak.com/us/en/products/75-Mini-Tools/373-universal-chain-tool
[96c]: https://www.topeak.com/us/en/products/75-Mini-Tools/181-super-chain-tool
[96d]: https://www.topeak.com/us/en/products/75-Mini-Tools/1370-POWER-LEVER-X

[97a]: https://www.wolftoothcomponents.com/collections/tools/products/pack-pliers
[97b]: https://www.wolftoothcomponents.com/collections/tools/products/8-bitpack-pliers

[98a]: https://cleverstandard.com/collections/home-page/products/clever-lever-original
[98b]: https://cleverstandard.com/collections/home-page/products/clever-chain-barrel-magnet
[98c]: https://cleverstandard.com/collections/home-page/products/chain-barrel-hex
[98d]: https://cleverstandard.com/collections/home-page/products/clever-chain-barrel
