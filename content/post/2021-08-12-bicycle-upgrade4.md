---
title: Blending old & new tech (4)
subtitle: Timeless bicycle frame geometries
date: 2021-08-12
tags: ["DIY", "bicycle", "bicycle touring", "bicycle commute", "bicycle renovation", "bicycle frame geometry", "bicycle handle bar", "bicycle locks"]
---

Modern bicycle frames of the past 20 years look somewhat differently: for
male adult bikes, for example, their top tube is often not horizontal but lowered
towards the seatpost.  Presumably, this makes a large frame less injury-prone
for a smaller male adult (and also saves weight?).

So, is a modern bike frame geometry an argument for a new bike? Are some
geometry changes merely a _fad_ or have there been new results from research?

For sure, [bicycle frames and fitting][13] is a field for [experts][2], which
I'm not.  Still, I can contribute some thoughts from experience
... <!--more-->

### Timeless frame geometry and handle bar shape

As a tall person, I find the frame's size and geometry of my 1993 bike nearly
perfect.  The long top tube gives my arms and legs sufficient space.  The long
wheelbase makes for good directional stability.  The tall bottom-bracket
height allows me to keep pedalling while cornering shallow curves!

Question: After a ~100 years of optimization since the [modern bicycles][1]
took shape, is it possible that the classical, _diamond_ frame geometry for
adult male bikes is close to being optimal?

I don't know the answer.  Given that [expert bike-frame databases][2] list so
many frame geometries, I'd assume there's at least a _range_ of time-proven
geometries & dimensions; but not many modifications make for an improvement.

Then there's the slightly sweeped and raised handle bar, which allows for a
flexible, neutral-to-upright position: bent elbows for a neutral, straight
elbows for almost upright.  Here in the U.S., I have not often seen this
handle bar shape: more sweep than a _riser bar_ but (much) less than a
_cruiser bar_.

__In summary:__ After 25+ years of commuting and touring my old bike, I feel very
comfortable with its classic frame geometry, size, and the handle bar shape!

![Torpedo Stratos][bars]

P.S.: I have not yet found a better place for the folding bike lock, as I need
both bottle holders.  [Update 2024: That folding lock broke and got replaced
with a smaller model of a different make.]

[bars]: /img/2021-08-09-bicycle-bars.jpg

[1]: https://en.wikipedia.org/wiki/History_of_the_bicycle#The_safety_bicycle:_1880s_and_1890s

[2]: https://geometrygeeks.bike

[13]: https://www.sheldonbrown.com/frames.html
