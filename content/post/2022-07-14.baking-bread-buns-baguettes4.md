---
title: How to Bake Breads, Buns, Baguettes (4)
subtitle: Step 4 - Dividing & Resting & Shaping (20m..45m)
date: 2022-07-14
tags: ["DIY", "baking", "bread", "sourdough"]
---

This step makes for a fun experience: take a lump of dough, divide it into
portions, and shape them into loaves for breads, buns, or baguettes.

These actions take a bit of practice: to fold the dough as it awakens from
fridge fermentation, to tell its cohesiveness and extensibility, to divide and
scale the dough portions, to form rounds (boules), to let them rest a little,
and, finally, to shape the bread loaves, or to fold bun rounds, or to roll
baguette tubes.

Shaping techniques: there are plenty.  This post shows some of them, such as
(as I'd call it) "flatten & fold & stuff & seal & coat" to form rounds, or
"fold & roll" for baguette loaves...

Then, the shaped loaves need to be proofed, see [step 5][r05].  Again, there
are plenty of techniques, such as proofing loaves in a linen cloth, or in a
banneton basket, or on a silicone baking mat...

This post with photos and videos describes methods of shaping and proofing
loaves
...<!--more-->

_Use as little flour as possible when dividing, folding, and shaping loaves._

#### 4.1 Prepare work surface, divide dough into portions

Flour a dry, clean, and smooth work surface (countertop, glass cooktop etc.).

Fold the dough in the bowl one last time and drop onto the work surface. \
Dust all sides with flour, lift and slap off excess flour:
![dust dough][baking41a]

Divide the dough using a bench scraper or a wetted knife:
![divide into portions][baking41b]

Check the portions' weight on a scale, even out to ~1%.

Poke the dough with a fork to degas and pop bubbles, as needed.

#### 4.2 Gently flatten dough portions, shape rounds (boule)

Various techniques exist how to shape portions into rounds, ovals, or tubes.

_Preferences:_ Use much less flour than shown in photos/videos below.

##### 4.2.1 Preferred method: flatten & fold & stuff & seal & coat

Flatten dough portion and poke large air bubbles...
![fold a dough portion into a round 1][baking42a]
... grab an edge, fold it over halfway, repeat, form a "packet"...
![fold a dough portion into a round 2][baking42b]
... flip, and keep stuffing the outside into the bottom side's center...
![fold a dough portion into a round 3][baking42c]
... until the round's surface shows tension; squeeze the seam to seal...
![fold a dough portion into a round 4][baking42d]
... dip the round in flour and drop seamside-down onto _un_floured surface.

Here, as a \
[time lapse video: divide dough and fold a portions into a rounds][baking42e].

##### 4.2.2 Alternative method: flatten & fold & roll into a ball

Some "how to" videos, [here][12c] or [here][12d], suggest this technique: \
\- Slap dough portions onto a _sticky_ and only _lightly_ floured surface. \
\- Flatten and de-gas the dough portion a bit, fold sides into a packet. \
\- To seal the seam, roll into a ball against the surface and tug in edges.

=> Working with little flour requires a good work surface (wooden countertop).

##### 4.2.3 Alternative method: roll into a ball & pinch & stuff

Yet another video (no link) suggested this technique: \
\- Slap dough portions onto a _non-sticky_ work surface to de-gas them a bit. \
\- Wet both hands, fold into a ball, roll across the surface to seal the seam; \
\- Lift up, pinch a side, and stuff the edges inside to form surface tension.

=> Working with _wetted_ hands turned out less practical than with flour.

##### 4.2.4 Alternative method: pre-shape into an oval or cylinder

To shape an oval: shape a round (boule), flatten a bit, and tug in the sides.

To shape a tube or a cylinder, see video [here][12a]: \
\- Flatten the dough portion into a rectangle, de-gas large bubbles. \
\- Fold the edges to the vertical centerline. \
\- Roll up the rectangle from the top, seal the seam by pressing down.

=> Working with rounds as a _pre-shape_ is easiest; gravity flattens nicely.

#### 4.3 Let rounds rest seamside-down

Some bread recipes skip this "bench rest" step after folding rounds:
![let rest seamside-down][baking43a]

_Preferences:_ Let rounds rest for 5..40m to \
\- bring them up to room temperature after fridge fermentation, \
\- use the rounds' own weight to seal the bottom-side seam, \
\- relax the dough for easier shaping into the final form.

When the rounds start swelling, continue to shape and proof the loaves.

#### 4.4 How to shape & proof buns

No need to use a scale when dividing the rounds into portions for buns.

Here, a \
[time lapse video: divide dough portions for buns][baking44a].

One can shape small bun loaves the same way as for rounds (see 4.2).

Here, a \
[video: how to flatten & fold & pinch a bun "dumpling"][baking44b] \
... and as a \
[time lapse video: flatten & fold & pinch 16 bun loaves][baking44c].

_Optional:_ Roll dumplings in sesame/poppy seeds or in ground black pepper:
![ground black pepper, sesame seeds, poppy seeds][baking44d]

Then, place dumplings seamside-down on a baking mat (or parchement paper). \
To form "doubles" or "pull-apart" buns place them at ~1cm/0.4" distance...
![place pull-apart buns on silicone baking mat][baking44e]
... on silicone mats that are certified to 500°F/260°C (from [here][11b]).

#### 4.5 How to shape & proof baguettes

Various techniques exist how to shape baguette loaves and how to proof them.

First, one needs to shape this: round -> oval -> rectangle -> cylinder \
\- Flip the dough round (boule) seamside-up and flatten into an oval. \
\- Do a _letter fold:_ pull the top third across the centerline, flatten... \
\- ... pull the bottom third across the centerline; flatten into a rectangle. \
\- Fold once more along the centerline and seal the seam with the thumb... \
\- ... or simply roll up the packet (from either side) into a cylinder. \
\- See videos in 4.5.1.

From here on, there are two ways of _shaping_ and of _proofing_ baguettes.

_Preferences:_ "fold & roll" (4.5.1), then proofing in a cloth (4.5.3)...
![Baguettes folded & rolled, proofed in a cloth][baking02]
... makes a well-shaped baguette with a nice, floury surface (what I do now).

_Beginner's Method:_ "roll & sling" (4.5.2), then proofing on a mat (4.5.4)...
![baguettes rolled & slung, proofed on a baking mat][baking45a]
... _can_ make a decent baguette with a smooth surface (how I started).

Here, another comparison...
![baguettes folded & rolled vs rolled & slung][baking45b]
... baguettes in the middle: "fold & roll" (4.5.1) \
... baguette at bottom: "roll & sling" (4.5.2) \
... then all proofed in a cloth (4.5.3).

##### 4.5.1 Traditional method: fold & roll loaves to full length

Roll loaf on the surface back and forth with the floured palms of both hands. \
Each time, stretch the loaf by moving hands slightly outwards (mark a "V"):
![roll baguettes loaves][baking45c]

Here, a \
[video: how to fold & roll a baguette loaf][baking45d] \
... and as a \
[time lapse video: fold & roll 2 baguette loaves][baking45e].

The trick is to use just the right amount of _flour_ and _force:_ \
\- too much flour: the loaves will skid on the surface; \
\- too much force: the loaves will flatten and not roll; \
\- too little flour: the loaves will stick to the surface; \
\- too little force: the loaves will not elongate while rolling.

Also, the dough must be not too wet (= sticky) or too elastic (= springy).

It just takes a bit of practice.

For professional videos, see [here][12c], [here][12a], or [here][12b].

=> "fold & roll" makes evenly-shaped baguettes with a nice, floury surface.

##### 4.5.2 Alternative method: roll & sling loaves to full length

Alternatingly, roll the loaf between the floured palms of both hands...
![sling & roll baguette loaves][baking45f]
... and then sling it like a rope to elongate.  Repeat until full length.

Work the thickest segments to shape the loaf as evenly as possible.

=> "roll & sling" makes a quick loaf, but the shape tends to turn out uneven.

##### 4.5.3 Traditional method: proof loaves in a cloth

Bakers use a _couche_ from natural flax linen (such as [this][11e]). \
A _pure linen_ kitchen towel works as well.  Avoid cotton as dough will stick. \
(Some recipes suggest using spray-oil or lots of flour, but this makes a mess.)

Pull up cloth ~5cm/2" as a separating wall before placing the next loaf. \
Also, hold up the sides with a "book end" to keep loaves from spreading out:
![proofing baguette loaves in a cloth][baking45g]

When the loaves have risen, place a long board (peel) right next to a loaf. \
Then lift the cloth and roll the long and wobbly loaf onto the board. \
From there, roll the loaf onto a baking mat and straighten out the shape.

For video instructions, see again [here][12c], [here][12a], or [here][12b].

The benefit of proofing in a cloth: the dough's floury surface stays dry. \
This makes for an appealing, textured, crispy, and floury crust after baking.

=> Proofing loaves in a cloth makes well-shaped baguettes with a nice surface.

##### 4.5.4 Alternative method: proof loaves on a baking mat in a mold

One can find baskets and molds for baguettes (e.g., [this][11f], [this][11g]). \
(I have not tried out any of these.)

How I started: A stapled cardboard mold with 3x slots (~4x40cm/~1.6x16")...
![cardboard mold with 3 slots for baguettes 1][baking45h]
... for a half-sheet-size silicone baking mat (from [here][11b])...
![cardboard mold with 3 slots for baguettes 2][baking45i]
... alas, the loaves spread out when pulling the mat out of the mold...
![cardboard mold with 3 slots for baguettes 3][baking45j]
... Ok, these loaves also started out too uneven (better roll than sling).

All in all, the baguettes still come out OK...
![baguettes rolled & slung, proofed on a baking mat][baking45a]
... but this method clearly has two snags:

1) The dough sticks to the mat where in contact, including the side walls. \
When pulling the mat out of the mold, the loaves spread out and smear. \
Each loaf's shape must then be fixed with wetted bench scrapers.

2) The dough's floury surface cannot breathe in the mat and becomes moist. \
After baking, the baguettes lack the nicely textured and floury appearance.

=> Proofing loaves on a mat in a mold gives an easy start but has snags.

#### 4.6 How to shape & proof breads

Breads are usually sized in pounds (ca. 1/4..1/3 weight loss during baking): \
0.5 lb (small), 1 lb (medium), 1.5 lb (large), 2 lb (xlarge) etc.

_Preferences:_ Shape loaves for small breads: \
\- more crust, so, more flavour per volume (square–cube law :wink:); \
\- less stale bread, small loaves are used up faster, can freeze them; \
\- more variety, can cut into multiple breads, say, a rye and a wheat.

Fold ~300g/0.67 lb portions into rounds (see 4.2), let rest (see 4.3):
![fold rounds][baking46a]

As with baguettes, there are various ways of proofing bread loaves: In a... \
\- _couche_ (cloth) from natural flax linen (see 4.5.3), placed in a bowl; \
\- _banneton_ (bentwood willow) basket, oftentimes coated with rice flour; \
\- _silicone/plastic basket_ made from a foodsafe, anti-stick material; \
\- _wicker basket_ (woven basket), typically covered with a _linen cloth._

_Preferences:_ Proof loaves in _banneton_ baskets: \
\- Place loaves directly in the baskets, not in their linen cover; \
\- the bentwood imprints a pattern, which makes for a nice, appealing crust. \
\- To prevent dough sticking to the basket, dampen and coat with rice flour.

Here, a set of 5.5" bannetons (from [here][11h]), coated with rice flour:
![rice-flour-coated 5.5" banneton baskets][baking46b]

Note: Other 5.5" bannetons turned out smaller than advertised (too small).

For round bannetons, one should plan for:

| bread weight | dough weight \* | min diameter \* | min height \* |
|--:|--:|--:|
| 0.5 lb | 0.67 lb | 5.5"/14.0cm | 2.5"/6.5cm |
| 1.0 lb | 1.33 lb | 7"/17.8cm | 3.3"/8.4cm |
| 1.5 lb | 2.00 lb | 8.5"/21.6cm | 3.5"/8.9cm |
| 2.0 lb | 2.67 lb | 10"/25.4cm | 4.0"/10.2cm |

\* Estimated (so far, I have not yet baked loaves larger than 1 lb).

Sprinkle the basket's bottom with flour...
![place loaf in banneton basket 1][baking46c]
... drop loaf _seamside-up,_ sprinkle flour between loaf and basket...
![place loaves in banneton basket 2][baking46d]
... also dust the top side...
![place loaf in banneton basket 3][baking46e]
... and, here, as a \
[time lapse video: place loaves in banneton baskets][baking46f].

[baking02]: /img/2022-07-10-baking02.jpg

[baking41a]: /img/2022-07-14-baking41a.jpg
[baking41b]: /img/2022-07-14-baking41b.jpg

[baking42a]: /img/2022-07-14-baking42a.jpg
[baking42b]: /img/2022-07-14-baking42b.jpg
[baking42c]: /img/2022-07-14-baking42c.jpg
[baking42d]: /img/2022-07-14-baking42d.jpg
[baking42e]: /img/2022-07-14-baking42e.x264.mp4

[baking43a]: /img/2022-07-14-baking43a.jpg

[baking44a]: /img/2022-07-14-baking44a.x264.mp4
[baking44b]: /img/2022-07-14-baking44b.x264.mp4
[baking44c]: /img/2022-07-14-baking44c.x264.mp4
[baking44d]: /img/2022-07-14-baking44d.jpg
[baking44e]: /img/2022-07-14-baking44e.jpg

[baking45a]: /img/2022-07-14-baking45a.jpg
[baking45b]: /img/2022-07-14-baking45b.jpg
[baking45c]: /img/2022-07-14-baking45c.jpg
[baking45d]: /img/2022-07-14-baking45d.x264.mp4
[baking45e]: /img/2022-07-14-baking45e.x264.mp4
[baking45f]: /img/2022-07-14-baking45f.jpg
[baking45g]: /img/2022-07-14-baking45g.jpg
[baking45h]: /img/2022-07-14-baking45h.jpg
[baking45i]: /img/2022-07-14-baking45i.jpg
[baking45j]: /img/2022-07-14-baking45j.jpg

[baking46a]: /img/2022-07-14-baking46a.jpg
[baking46b]: /img/2022-07-14-baking46b.jpg
[baking46c]: /img/2022-07-14-baking46c.jpg
[baking46d]: /img/2022-07-14-baking46d.jpg
[baking46e]: /img/2022-07-14-baking46e.jpg
[baking46f]: /img/2022-07-14-baking46f.x264.mp4

[r05]: /post/2022-07-15.baking-bread-buns-baguettes5/

[11b]: https://www.amazon.com/gp/product/B08RDX5PXZ
[11e]: https://www.amazon.com/gp/product/B06XXXQVNZ
[11f]: https://www.amazon.com/gp/product/B07ZKHWTWP
[11g]: https://www.amazon.com/gp/product/B086D1GHQQ
[11h]: https://www.amazon.com/gp/product/B078W4V7KD

[12a]: https://www.youtube.com/watch?v=ba2DHI299PU
[12b]: https://www.youtube.com/watch?v=fKRZOpHvcdI
[12c]: https://www.youtube.com/watch?v=PmxDKuGLWuE
[12d]: https://www.youtube.com/watch?v=7suBiDyRzYs

[pr1]: https://www.penguinrandomhouse.com/books/250188/the-bread-bakers-appr
