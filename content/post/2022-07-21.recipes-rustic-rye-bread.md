---
title: Recipes - Rustic Rye Bread
subtitle: Rustic dark rye, whole wheat bread with crust and flavour
date: 2022-07-21
tags: ["DIY", "recipes", "baking", "bread", "sourdough", "flours"]
---

This rye-wheat sourdough relies on a sizable amount of levain and poolish for
a thorough yet slow fermentation in the fridge.  The result is an airy crumb
with an intense flavour.  This recipe bakes one half-sheet pan of 4x small
1/2 lb breads.

| | Signature: |
|:---:|:---|
| 3:2:1 | dark rye flour : whole wheat flour : bread flour |
| 80% | hydration (%total flour weight) |
| 40% | levain |
| 20% | poolish |
| | ~48h cold fermentation, ~60h total time, ~3h total work |

Goal: distinctive & nuanced flavour, chewy & moist crumb, crispy & tasty crust
...<!--more-->

#### Makes:

4x small breads of ~270g (9.6oz, >1/2lb), ~15cm (6") diameter \
from ~345g (12.2oz) dough portions for 1x half-sheet baking pan (tray).

Stores for 3 days.  Freezes for a couple of months.

Almost perfect (extend final proofing time, score more evenly):
![4x 1/2lb rye sourdoughs][rye-sourdoughs]

#### 6h..12h: pre-ferments (sponges), 8h@22°C/72°F

240g levain: 120g bread flour (mix), 120g water, ~60g sourdough starter. \
120g poolish: 0.12g instant yeast, 60g mellow all-purpose flour, 60g water.

See [pre-ferments][r01].

#### 30m: scaling & mixing

__[try:]__
| | | ~1400g dough |
|--:|--:|:--|
| 300.0g | 50% | dark rye flour |
| 200.0g | 33% | whole wheat flour \* |
| 100.0g | 17% | bread flour or strong all-purpose flour |
| 14.0g | 1.80% | salt (%TFW) |
| = 600g | | flour weight |
| | | |
| 240.0g | 40% | levain (bread flour mix) |
| 80.0g | 13% | poolish (mellow all-purpose flour) |
| 460.0g | 77% | water, room temperature (~25°C/77°F) \*\* |
| = 620g | 80% | hydration (%TFW) |
| | | |
| ~20g | 3% | all-purpose flour for shaping |
| = 780g | | total flour weight |

__[was:]__
| | | ~1440g dough |
|--:|--:|:--|
| 300.0g | 50% | dark rye flour |
| 200.0g | 33% | whole wheat flour \* |
| 100.0g | 17% | bread flour or strong all-purpose flour |
| 14.0g | 1.75% | salt (%TFW) |
| = 600g | | flour weight |
| | | |
| 240.0g | 40% | levain (bread flour mix) |
| 120.0g | 20% | poolish (mellow all-purpose flour) |
| 460.0g | 77% | water, room temperature (~25°C/77°F) \*\* |
| = 640g | 80% | hydration (%TFW) |
| | | |
| ~20g | 3% | all-purpose flour for shaping |
| = 800g | | total flour weight |

\* _Optional:_ For a milder flavour, substitute _white whole wheat_ flour.

\*\* _As needed:_ Increase/reduce hydration for stronger/mellower flours.

See [scaling & mixing][r02].

#### 48h..60h: bulk rise, 2h@22°C/72°F + 48h@8°C/46°F

_For shorter fermentation:_ 4h@22°C/72°F + 24h@8°C/46°F

See [bulk rise][r03].

#### 50m: dividing & shaping & resting 40m@22°C/72°F

Proof loaves in 4x 5" banneton baskets.

See [dividing & shaping][r04].

#### 30m..60m: proofing 20m@30°C/86°F + 10m@22°C/72°F & preparing oven

See [proofing & preheating oven][r05].

#### 70m..80m: scoring & baking 38m@260°C/500°F (with steam) & cooling

_Preference:_ Score loaves 2..3cm (~1") deep, down all sides.

Bake on 2x half-sheet pans (trays) with 1x extra pan hot water for steam. \
For 1x tray: skip the 1/3 rack position, shorten total oven time to 50m (?). \
For 3x trays: extend total oven time to ~70m (?).

_Method #1:_ Place pan with hot water in cold oven, preheat to 500°F/260°C.

| minute | duration | °F | °C | bottom rack | 1/3 rack | 2/3 rack | comments |
|:---:|---:|---:|---:|:---:|:---:|:---:|:---|
| 00 | ~5m |  ~70 |  ~20 | 0.8l hot water | - | - | place pan in cold oven, pour hot water |
|    |     |      |      | + | - | - | start preheating to 500°F/260°C |
| 05 | ~7m | ~300 | ~150 | about to boil | - | - | begin to score loaves |
| 12 | 15m | ~360 | ~180 | boiling | + | + | place trays with scored loaves |
| 27 |  5m | ~430 | ~220 | - | + | + | remove pan, swap/rotate trays |
| 32 |  5m | ~500 | ~260 | - | + | + | swap trays |
| 37 |  5m | ~500 | ~260 | - | + | + | swap/rotate trays |
| 42 |  8m | ~500 | ~260 | - | + | + | cancel heating, swap trays |
| 50 | 10m |      |      | - | + | + | leave oven door ajar for crust |
| 60 | 20m |      |      | - | - | - | remove from oven, let cool, dust off flour |

_Method #2:_ Place pan with hot water in oven preheated to 500°F/260°C.

| cycles | °F | °C | time | bottom rack | 1/3 rack | 2/3 rack | comments |
|:---:|---:|---:|---:|:---:|:---:|:---:|:---|
| 1x |  72 |  22 | ~15m | - | - | - | start preheating to 500°F/260°C |
| 1x | 500 | 260 |  ~5m | 0.8l hot water | - | - | place pan in hot oven, pour hot water, score loaves |
| 1x | 500 | 260 |  14m | boiling | + | + | place trays with scored loaves |
| 4x | 500 | 260 |   6m | - | + | + | remove pan, swap/shift/rotate trays as needed |
|    |     |     |  10m | - | + | + | cancel heating, leave oven door ajar for crust |
|    |     |     |  20m | - | - | - | remove from oven, let cool, dust off flour |

_Preference:_ Bake to medium-dark brown with a crispy crust.

_Caution:_\
Step aside when opening the oven door, steer clear of the steam rushing out. \
Wear oven mitts when rotating trays or removing the pan with boiling water.

See [scoring & baking][r06].

[rye-sourdoughs]: /img/2023-05-12-rye-sourdoughs.jpg

[r00]: /post/2022-07-10.baking-bread-buns-baguettes0/
[r01]: /post/2022-07-11.baking-bread-buns-baguettes1/
[r02]: /post/2022-07-12.baking-bread-buns-baguettes2/
[r03]: /post/2022-07-13.baking-bread-buns-baguettes3/
[r04]: /post/2022-07-14.baking-bread-buns-baguettes4/
[r05]: /post/2022-07-15.baking-bread-buns-baguettes5/
[r06]: /post/2022-07-16.baking-bread-buns-baguettes6/
