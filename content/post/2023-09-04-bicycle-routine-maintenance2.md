---
title: Checklists for bicycle maintenance (2)
subtitle: Drivetrain, chains, hubs, gears, cranks, bottom brackets
date: 2023-09-04
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle hubs", "bicycle gears", "bicycle chains", "bicycle derailleurs", "bicycle shifters", "bicycle cranks", "bicycle bottom brackets", "bicycle drivetrain"]
---

The prior blog series "about bicycle chains" [(1)][0b] recaps this lesson
learnt: \
\- Key to a long-lasting, smooth-shifting drivetrain is to always ride with a
   _clean chain._ \
\- A chain with a _master/quick/connex link_ is taken off/mounted on the bike
   in seconds. \
\- This allows to cleanse, lubricate, or rewax the chain _off the bike_ in a
   lidded container.

The blog series then details basic chain maintenance: measure chain wear
[(2)][0d], remove and install a chain [(3)][0d], size a new chain [(4)][0d],
clean and lubricate [(5)][0d] or wax [(6)][0d] a chain.  Another post showed
how to [oil-service][0h] a Shimano 8-speed Alfine internal-gear hub.

These checklists summarize those blog posts and other how-to guides: \
[2.1 check chain wear][r2.1] \
[2.2 remove/install chain][r2.2] \
[2.3 clean/rewax chain][r2.3] \
[2.4 size/cut chain][r2.4] \
[2.5 check/adjust Alfine/Nexus 8-speed gear hub][r2.5] \
[2.6 service Alfine/Nexus 8-speed gear hub][r2.6] \
[2.7 service Alfine/Nexus dynamo hub][r2.7] \
[2.8 service/adjust hub][r2.8] \
[2.9 service/adjust derailleur gears][r2.9] \
[2.10 service shifters][r2.10] \
[2.11 replace shifter cables][r2.11] \
[2.12 remove/install crank][r2.12] \
[2.13 service bottom bracket][r2.13] \
[2.14 resources, playlists][r2.14]
... <!--more-->

[r2.1]: #21-check-chain-wear
[r2.2]: #22-remove-install-chain
[r2.3]: #23-clean-rewax-chain
[r2.4]: #24-size-cut-chain
[r2.5]: #25-check-adjust-alfine-nexus-8-speed-gear-hub
[r2.6]: #26-service-alfine-nexus-8-speed-gear-hub
[r2.7]: #27-service-alfine-nexus-dynamo-hub
[r2.8]: #28-service-adjust-hub
[r2.9]: #29-adjust-derailleur-gears
[r2.10]: #210-service-shifters
[r2.11]: #211-replace-shifter-cables
[r2.12]: #212-remove-install-crank
[r2.13]: #213-service-bottom-bracket
[r2.14]: #214-resources-playlists

Hmm, what about servicing _pedals_ and _shoes,_ aren't they part of the
drivetrain?
[Yes!](https://www.youtube.com/watch?v=bVmSrsnVUGI&t=11s) :wink: \
But as this post is quiet long, they're covered with other _adjustables_ in
the next [post][r3].

Links to the _resources/videos_ on [(SB)][2], [(PT)][1], [(BR)][5], [(BG)][4],
[(CMA)][3], see [post][0a], given in each task.

_Note:_ I update these checklists _in-place_ for improvements and
clarifications; also, some tasks still need to be written up.  For comments:
martin dot zaun at gmx dot com

### 2.1 Check chain wear

| | |
|:--:|:--|
| (1) | measure chain stretch with a _chain checker_ tool, see [post][0c] |
| | |
| (2) | replace chain when stretch reaches: |
| | ~ 0.5% for 9+ speed chains |
| | ~ 0.75% for 6-8 speed or 3/32" chains |
| | ~ 1.0% for single sprocket or 1/8" (track) chains |
| | also replace chain when: |
| | - showing large sideways play |
| | - chain got bent/damaged in a "chain jam" |
| | |
| (3) | replace chain + sprockets + chainring when |
| | - worn and wearing down new chains quickly |
| | - showing "chain suck" (sticking to chainring) |

### 2.2 Remove/install chain

| | |
|:--:|:--|
| | _for a connecting rivet chain:_ consider switching to a master-link chain, see [post][0c] |
| | |
| (1) | disconnect/connect master-link chain: |
| | find/fit master link, engage with _link plier,_ see [post][0d] |
| | disconnect: push inwards until link unlocks with a click (or pull rollers together with a wire) |
| | connect: pull outwards until link locks with a click (or apply brake and pedal forward) |
| | |
| (2) | install master-link chain: |
| | route chain through any derailleurs or tensioners, lay onto smallest sprocket |
| | if possible, leave chain off chainring to allow for more slack when fitting master link |
| | alternatively, fixate chain ends with a _chain hook_ for enough slack to fit master link |
| | _for asymmetrical chains:_ (high-end Shimanos) sideplates with marking face outward |
| | |
| (4) | put chain back on: |
| | hook chain onto rear sprocket and the bottom teeth of smallest chainring |
| | backpedal to engage |

Resources: \
[(SB) measure chain wear][2j];
[(SB) fix chain drop/jam][2k];
[(SB) connect/disconnect/cut chain][2n];
[(PT) identify chain][1a];
[(PT) check chain wear][1b];
[(PT) fix broken chain][1d];
[(PT) break/cut/install single-speed chain][1h];
[(PT) break/cut/install derailleur chain][1f];
[(BG) measure chain wear][4b];
[(BG) chain types/compatibility][4c];

Videos: \
[(PT) measure chain wear][1c];
[(PT) fix broken chain][1e];
[(PT) break/cut/install single-speed chain][1i];
[(PT) break/cut/install derailleur chain][1g];
[(CMA) tighten chain][3d];
[(CMA) remove/cut/install chain][3e];
[(CMA) remove/cut chain][2f];
[(BG) diagnose chain suck][4d];
[(BG) diagnose sideways play][4e];

Long videos: \
[(BG) connect chains][4m];

### 2.3 Clean/rewax chain

| | TODO: summarize |
|:--:|:--|
| | summarize [post][0g] |

Resources: \
[(SB) service chain][2i];
[(SB) service chain][2l];
[(PT) clean/lubricate chain][1l];
[(BG) lubricate chain][4g];
[(BG) lubricate chain][4h];
[(BG) lubricate chain][4i];

Videos: \
[(PT) clean/lubricate chain][1m];
[(CMA) clean/lubricate chain][3i];
[(CMA) lubricate chain][3g];
[(CMA) clean drivetrain][3h];

### 2.4 Size/cut chain

| | TODO: summarize |
|:--:|:--|
| | summarize [post][0g] |

Resources: \
[(SB) size chain](https://www.sheldonbrown.com/derailer-adjustment.html#chain);
[(SB) connect/disconnect/cut chain][2n];
[(PT) identify chain][1a];
[(PT) size/cut chain][1j];
[(PT) size/cut/install single-speed chain][1h];
[(PT) size/cut/install derailleur chain][1f];
[(PT) size/cut/install derailleur chain: alt method][1s];

Videos: \
[(PT) size/cut chain][1k];
[(PT) size/cut/install single-speed chain][1i];
[(PT) size/cut/install derailleur chain][1g];
[(PT) size/cut/install derailleur chain: alt method][1t];
[(BG) size chain][4k];
[(BG) size chain][4l];

### 2.5 Check/adjust Alfine/Nexus 8-speed gear hub

| | |
|:--:|:--|
| (1) | check gear hub: |
| | check that wheel is fully seated and centered in fork/frame, axle is tightened |
| | shift to middle gear shown on shifter lever |
| | check that the yellow setting lines on the cassette joint bracket and pulley are aligned |
| | |
| (2) | adjust gear hub: |
| | turn the shifter cable adjustment barrel to align the yellow setting lines, as needed |
| | shift through all gears and recheck that yellow setting lines are still aligned |

Resources: \
[Alfine/Nexus Hub Gear][10a];
[Alfine Hub Gear][10b];

### 2.6 Service Alfine/Nexus 8-speed gear hub

| | TODO: summarize |
|:--:|:--|

Resources: \
[Alfine/Nexus Hub Gear][10a];
[Alfine Hub Gear][10b];

### 2.7 Service Alfine/Nexus dynamo hub

| | TODO: summarize |
|:--:|:--|

Resources: \
[Alfine Hub Dynamo][10c];

Videos:
[(CMA) service Shimano Alfine/Nexus dynamo hub][3j];

### 2.8 Service/adjust hub

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) adjust cone][2m];
[(SB) service hub][2b];
[(BG) service hub][4n];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

### 2.9 Service/adjust derailleur gears

| | TODO: summarize |
|:--:|:--|

Resources: \
[(SB) service/adjust derailleur][2a];

Videos: \
[(CMA) adjust derailleur, fix chain drop][3a];
[adjust front derailleur][7d];
[adjust rear derailleur][7e];
[adjust derailleur][8b];
[adjust derailleur][8a];
[derailleurs][11b];

### 2.10 Service shifters

| | TODO: summarize |
|:--:|:--|

Videos: \
[(PT) shift levers / shifters][1n];
[compatibility Shimano shifters with derailleurs][11a];

### 2.11 Replace shifter cables

| | TODO: summarize |
|:--:|:--|

### 2.12 Remove/install crank

| | TODO: summarize |
|:--:|:--|
| (1) | crank arms are L/R-sided (R = drive side) |

Resources: \
[(SB) cranks][2d];
[(SB) cotterless cranks][2h];
[(PT) remove/install crank][1ab];
[(PT) remove/install crank: three-piece][1ad];
[(PT) remove/install crank: bmx three-piece][1aj];
[(PT) remove/install crank: two-piece][1af];
[(PT) remove/install crank: self-extracting][1ah];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) identify crank type][1aa];
[(PT) remove/install crank][1ac];
[(PT) remove/install crank: three-piece][1ae];
[(PT) remove/install crank: two-piece][1ag];
[(PT) remove/install crank: self-extracting][1ai];
[(CMA) remove crank][3b];
[(CMA) install crank][3c];
[compatibility bottom brackets with cranksets][11c];

Other: \
[(PT) remove crank: damaged threads (square type)][1ak];

### 2.13 Service bottom bracket

| | TODO: summarize |
|:--:|:--|
| (1) | bottom brackets are L/R-sided (R = drivetrain side) |

Resources: \
[(SB) remove/install bottom brackets: cartridge][2c];
[(SB) remove/install/service bottom brackets: cup-and-cone][2g];
[(SB) square taper bottom bracket][2f];
[(PT) identify bottom brackets][1am];
[(PT) remove/install bottom brackets: threaded][1ao];
[(PT) remove/install bottom brackets: thread-together][1aq];
[(PT) remove/install bottom brackets: press fit][1as];
[(PT) remove/install/service bottom brackets: cup-and-cone][1au];
[(PT) adjust bottom bracket preload ring][1aw];
[(BR) torque table][5a];
[(BG) torque table][4a];
[torque table][9a];

Videos: \
[(PT) identify bottom brackets][1an];
[(PT) remove/install bottom brackets: threaded][1ap];
[(PT) remove/install bottom brackets: thread-together][1ar];
[(PT) remove/install bottom brackets: press fit][1at];
[(PT) remove/install/service bottom brackets: cup-and-cone][1au];
[(PT) adjust bottom bracket preload ring][1ax];

Other: \
[(SB) bottom bracket size database][2e];
[(PT) bottom bracket standards][1al];
[(PT) bottom bracket tools: threaded, thread-together][1ca];
[(PT) bottom bracket tools: press fit][1cb];
[(PT) bottom brackets: bb30 adapters][1cc];
[(PT) ceramic bearings][1cd];
[compatibility bottom brackets with cranksets][11c];

### 2.14 Resources, playlists

Lists: \
[(SB) drivetrain, gears][2aa];
[(PT) cassettes & freewheels][1bh];
[(PT) cassettes & freewheels][1bd];
[(PT) derailleurs][1bk];
[(PT) derailleurs & shifting][1bc];
[(PT) shift cable & housing][1bf];
[(PT) hubs][1bl];
[(PT) chains][1bi];
[(PT) chains][1bb];
[(PT) cranks][1bj];
[(PT) cranks][1ba];
[(PT) bottom brackets][1bg];
[(PT) bottom brackets][1be];
[(CMA) drivetrain, chains, cranks, cassettes/freewheels][3aa];
[(BG) drivetrain][4ac];
[(BG) shifters, derailleurs][4ab];
[(BG) hubs][4ae];
[(BG) hubs][4af];
[(BG) shifters, derailleurs][4ab];
[(BG) rear hubs][4ad];
[(BG) drivetrain, chains, chainrings, cassettes][4aa];

Other: \
[(PT) chainline concepts][1o];
[(PT) chainline concepts][1p];
[(PT) chainring lockring tools][1q];
[(SB) singlespeeds, fixed gears][2ab];
[(SB) derailerless drivetrains][2ac];

[0a]: /post/2023-09-01-bicycle-resources/
[r0]: /post/2023-09-02-bicycle-routine-maintenance0.md
[r1]: /post/2023-09-03-bicycle-routine-maintenance1.md
[r2]: /post/2023-09-04-bicycle-routine-maintenance2.md
[r3]: /post/2023-09-05-bicycle-routine-maintenance3.md
[r4]: /post/2023-09-06-bicycle-routine-maintenance4.md

[0b]: /post/2021-08-17-bicycle-chain-maintenance1.md
[0c]: /post/2021-08-18-bicycle-chain-maintenance2.md
[0d]: /post/2021-08-19-bicycle-chain-maintenance3.md
[0e]: /post/2021-08-20-bicycle-chain-maintenance4.md
[0f]: /post/2021-08-21-bicycle-chain-maintenance5.md
[0g]: /post/2021-08-22-bicycle-chain-maintenance6.md
[0h]: /post/2021-08-16-bicycle-alfine-service.md

[1]: https://www.parktool.com/blog/repair-help

[1a]: https://www.parktool.com/en-us/blog/repair-help/chain-compatibility
[1b]: https://www.parktool.com/en-us/blog/repair-help/when-to-replace-a-chain-on-a-bicycle
[1c]: https://www.youtube.com/watch?v=gXd-3UnqoaM
[1d]: https://www.parktool.com/en-us/blog/repair-help/on-the-ride-chain-repair
[1e]: https://www.youtube.com/watch?v=HpUCCrgugQE
[1f]: https://www.parktool.com/en-us/blog/repair-help/chain-replacement-derailleur-bikes
[1g]: https://www.youtube.com/watch?v=VdUQKVMPF5I
[1h]: https://www.parktool.com/en-us/blog/repair-help/chain-replacement-single-speed-bikes
[1i]: https://www.youtube.com/watch?v=88tDcVvS7mU
[1j]: https://www.parktool.com/en-us/blog/repair-help/chain-length-sizing
[1k]: https://www.youtube.com/watch?v=O0YibMDWBAw
[1l]: https://www.parktool.com/en-us/blog/repair-help/chain-cleaning-with-a-park-tool-chain-scrubber
[1m]: https://www.youtube.com/watch?v=MuwS_nSevy4
[1n]: https://www.parktool.com/en-us/blog/repair-help/shift-levers-shifters
[1o]: https://www.parktool.com/en-us/blog/repair-help/chainline-concepts
[1p]: https://www.youtube.com/watch?v=0lN3Zf9gpp8
[1q]: https://www.parktool.com/en-us/blog/repair-help/chainring-lockring-tool-selection
[1s]: https://www.parktool.com/en-us/blog/repair-help/chain-length-sizing-campagnolo
[1t]: https://www.youtube.com/watch?v=s6TodcAH948

[1aa]: https://www.youtube.com/watch?v=VMV-SOIhM2c
[1ab]: https://www.parktool.com/en-us/blog/repair-help/how-to-remove-and-install-a-crank
[1ac]: https://www.youtube.com/watch?v=zbza6CA6YSI
[1ad]: https://www.parktool.com/en-us/blog/repair-help/crank-removal-and-installation-three-piece
[1ae]: https://www.youtube.com/watch?v=cPQyQnNdews
[1af]: https://www.parktool.com/en-us/blog/repair-help/crank-removal-and-installation-two-piece-compression-slotted
[1ag]: https://www.youtube.com/watch?v=QqBtB8Kyl2U
[1ah]: https://www.parktool.com/en-us/blog/repair-help/crank-removal-and-installation-self-extracting
[1ai]: https://www.youtube.com/watch?v=mxAj-zbynPI
[1aj]: https://www.parktool.com/en-us/blog/repair-help/bmx-three-piece-crank-service
[1ak]: https://www.parktool.com/en-us/blog/repair-help/removal-of-cranks-with-damaged-threads-square-type-only
[1al]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-standards-and-terminology
[1am]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-identification
[1an]: https://www.youtube.com/watch?v=e-8G1G9QNX8&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=2
[1ao]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-removal-installation-threaded
[1ap]: https://www.youtube.com/watch?v=xUtOeFJJycg&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=3
[1aq]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-removal-installation-thread-together
[1ar]: https://www.youtube.com/watch?v=84Ut53i17iQ&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=5
[1as]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-removal-installation-press-fit
[1at]: https://www.youtube.com/watch?v=2VnwKrC9rS0&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=4
[1au]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-service-adjustable-cup-and-cone
[1av]: https://www.youtube.com/watch?v=sEv8irsdQI8&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=6
[1aw]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-adjustment-using-preload-ring
[1ax]: https://www.youtube.com/watch?v=OjE1YMj9Al0&list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-&index=9

[1ca]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-tool-selection-threaded-and-thread-together
[1cb]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-tool-selection-press-fit
[1cc]: https://www.parktool.com/en-us/blog/repair-help/bottom-bracket-service-bb30-adaptors
[1cd]: https://www.parktool.com/en-us/blog/repair-help/ceramic-bearings

[1ba]: https://www.youtube.com/playlist?list=PLGCTGpvdT04SdAP5vThZlOInVsJ891lm4
[1bb]: https://www.youtube.com/playlist?list=PLGCTGpvdT04RFvpef1qIJSQygRL8sv0eh
[1bc]: https://www.youtube.com/playlist?list=PLGCTGpvdT04TlNwuGy2pd_y8wt-oLfuij
[1bd]: https://www.youtube.com/playlist?list=PLGCTGpvdT04R2nn5bxTvnwy37G6vWBJcV
[1be]: https://www.youtube.com/playlist?list=PLGCTGpvdT04S-dkxjU7HJjboDnTJNmjp-
[1bf]: https://www.youtube.com/playlist?list=PLGCTGpvdT04QA-ebMFtDA0l7HW0WKbsgy
[1bg]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=47
[1bh]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=49
[1bi]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=50
[1bj]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=51
[1bk]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=52
[1bl]: https://www.parktool.com/en-us/blog/repair-help?query=&area%5B%5D=56

[2]: https://www.sheldonbrown.com

[2a]: https://www.sheldonbrown.com/derailer-adjustment.html
[2b]: https://www.sheldonbrown.com/tooltips/hubs.html
[2c]: https://www.sheldonbrown.com/tooltips/cartridge.html
[2d]: https://www.sheldonbrown.com/cranks.html
[2e]: https://www.sheldonbrown.com/bbsize.html
[2f]: https://www.sheldonbrown.com/bbtaper.html
[2g]: https://www.sheldonbrown.com/tooltips/bbadj.html
[2h]: https://www.sheldonbrown.com/tooltips/cotterless.html
[2i]: https://www.sheldonbrown.com/chains.html
[2j]: https://www.sheldonbrown.com/chain-wear.html
[2k]: https://www.sheldonbrown.com/chain-drop.html
[2l]: https://www.sheldonbrown.com/brandt/chain-care.html
[2m]: https://www.sheldonbrown.com/cone-adjustment.html
[2n]: https://www.sheldonbrown.com/harris/tools/chain.html

[2aa]: https://www.sheldonbrown.com/bicycleGears.html
[2ab]: https://www.sheldonbrown.com/fixedgear.html
[2ac]: https://www.sheldonbrown.com/no-derailers.html

[3]: https://www.youtube.com/@cyclemaintenanceacademy/playlists

[3a]: https://www.youtube.com/watch?v=Ryuiq_jfCDE
[3b]: https://www.youtube.com/watch?v=sCLQcxXUlMg
[3c]: https://www.youtube.com/watch?v=VMYlb_UcVOk
[3d]: https://www.youtube.com/watch?v=MU5vTv64Q-A
[3e]: https://www.youtube.com/watch?v=S_CnUWA7pWQ
[3f]: https://www.youtube.com/watch?v=IOm6Rinckg8
[3g]: https://www.youtube.com/watch?v=C0TVQxRTyls
[3h]: https://www.youtube.com/watch?v=2V5CcRXq3xY
[3i]: https://www.youtube.com/watch?v=Bb7xBoRUEqw
[3j]: https://www.youtube.com/watch?v=bw7hYsSEKwg

[3aa]: https://www.youtube.com/playlist?list=PLao4ofgFC9hyeSs5Ox5uQIoLvKCYwX__U

[4]: https://bike.bikegremlin.com/post-list-by-category/

[4a]: https://bike.bikegremlin.com/14694/bicycle-tightening-torques/
[4b]: https://bike.bikegremlin.com/733/change-chain-bicycle/
[4c]: https://bike.bikegremlin.com/1220/1-bicycle-chains-compatibility/
[4d]: https://www.youtube.com/watch?v=IRP_jlNr7Z0
[4e]: https://www.youtube.com/watch?v=hzXpiF-UT-A
[4f]: https://www.youtube.com/watch?v=9W8l60krrG0
[4g]: https://bike.bikegremlin.com/44/best-bicycle-chain-lube/
[4h]: https://bike.bikegremlin.com/1986/bicycle-chain-lubricants-explained/
[4i]: https://bike.bikegremlin.com/44/best-bicycle-chain-lube/

[4k]: https://bike.bikegremlin.com/614/chain-length-sizing-for-bicycles-with-derailleurs/
[4l]: https://bike.bikegremlin.com/597/determining-single-speed-chain-length/
[4m]: https://www.youtube.com/watch?v=HgAIEcBllA4
[4n]: https://bike.bikegremlin.com/1461/hub-overhaul/

[4aa]: https://www.youtube.com/playlist?list=PLlbCLBTvuIsuDr5kLKUYTXaQixntFQxRY
[4ab]: https://www.youtube.com/playlist?list=PLlbCLBTvuIstQmktOo_cgA7X6INB1xmoH
[4ac]: https://bike.bikegremlin.com/category/bicycle-drivetrain/
[4ad]: https://bike.bikegremlin.com/158/bicycle-rear-hub/
[4ae]: https://bike.bikegremlin.com/category/bearings-brakes-wheels/wheels/
[4af]: https://www.youtube.com/playlist?list=PLlbCLBTvuIstsl8a9sC02j06nriuCqVRO

[5]: https://www.bikeride.com/guide/

[5a]: https://www.bikeride.com/torque-specifications/

[7a]: https://www.youtube.com/watch?v=FJEEGYTyQq4
[7b]: https://www.youtube.com/watch?v=kn4cMsC1aUw
[7c]: https://www.youtube.com/watch?v=zsxhlpAcZRs
[7d]: https://www.youtube.com/watch?v=hyz0bJ38Y4I
[7e]: https://www.youtube.com/watch?v=xOHmlPhkgBs

[8a]: https://www.youtube.com/watch?v=LzqVcvdjZvM
[8b]: https://www.youtube.com/watch?v=LzqVcvdjZvM

[9a]: https://www.dedhambike.com/articles/torque-table-pg186.htm

[10a]: https://si.shimano.com/en/pdfs/dm/CASG004/DM-CASG004-03-ENG.pdf
[10b]: https://si.shimano.com/en/pdfs/dm/SG0004/DM-SG0004-09-ENG.pdf
[10c]: https://si.shimano.com/en/pdfs/si/2ZS0C/SI-2ZS0C-001-ENG.pdf

[11a]: https://www.youtube.com/watch?v=myXVbwITvNk
[11b]: https://www.youtube.com/watch?v=eQHICjn8U_A
[11c]: https://www.youtube.com/watch?v=aeYlWxYdl9E
