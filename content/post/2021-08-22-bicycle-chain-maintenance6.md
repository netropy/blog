---
title: About bicycle chains (6)
subtitle: Why & how to wax a chain
date: 2021-08-22
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain"]
---

The prior post [(5)][0e] described how to clean a chain effectively: Take the
chain off the bike, soak it in a strong solvent, rinse or let evaporate.
Then, mount the chain, apply an oil-based lubricant... and watch it attract
dirt again.

Another method of lubricating bicycle chains has been known for a long time:
[paraffin wax][22a].  Yes, not oil (attracting dirt) but candle wax (repelling
dirt). :thinking:

My experience: _The easiest, fastest, and cleanest chain lube._

This post concludes the series on bicycle chain maintenance and discusses
- the benefits of wax and what makes it superior to oil-based lubricants,
- how for the 1st time, the chain must be cleaned sterile before waxing,
- how thereafter cleaning and rewaxing the chain is quick and simple,
- what wax mileage to expect and whether to buy chain wax or mix your own
... <!--more-->

15 questions & answers in logical order:

[What are the benefits of wax as chain lubricant?](what-are-the-benefits-of-wax-as-chain-lubricant)

1st time wax preparation: \
[How to strip the factory grease off a chain?](#1st-time-prep-how-to-strip-the-factory-grease-off-a-chain) \
[Should I start with a new chain?](#1st-time-prep-should-i-start-with-a-new-chain) \
[How to clean a chain? (container method)](#1st-time-prep-how-to-clean-a-chain-container-method) \
[What solvents to use? (container method)](#1st-time-prep-what-solvents-to-use-container-method) \
[Why not to use an ultrasonic cleaner?](#1st-time-prep-why-not-to-use-an-ultrasonic-cleaner)

Waxing: \
[Buy a chain wax or mix your own?](#waxing-buy-a-chain-wax-or-mix-your-own) \
[Use a slow cooker or hot water bath?](#waxing-use-a-slow-cooker-or-hot-water-bath) \
[Just submerse the chain and agitate, right?](#waxing-just-submerse-the-chain-and-agitate-right)

Rewaxing: \
[How soon? (wax mileage)](#rewaxing-how-soon-(wax-mileage)) \
[What causes poor wax mileage?](#rewaxing-what-causes-poor-wax-mileage) \
[How to clean the chain?](#rewaxing-how-to-clean-the-chain) \
[Drip wax or wax bath?](#rewaxing-drip-wax-or-wax-bath) \
[When to replace the wax in a pot?](#rewaxing-when-to-replace-the-wax-in-a-pot)

Summary: \
[A clean chain - always and easy!](#summary-a-clean-chain---always-and-easy)

Some product-related links, photos, or comments below (I'm not sponsored by
anybody).

### What are the benefits of wax as chain lubricant?

In my experience: the _easiest, fastest, cleanest_ chain lube.
1. Easiest: Rinse the chain with boiling water, submerse it in molten wax.
2. Fastest: Low friction, my waxed chain feels as smooth as the prior oiled
   chain.
3. Cleanest: See this photo from a previous post, touching the chain leaves no
   mark.

![Waxed Chain Drivetrain][drivetrain]

__Resources & Quotes:__

Excerpt [(SB0)][10a]: repels dirt

> The hot wax is of a thin enough consistency that it can theoretically
> penetrate into the private parts of the chain, then when it cools off, you
> have a nice thick lubricant in place where it can do the most good.  The
> major advantage to this approach is that, once cooled off, the wax is not
> sticky, and doesn't attract dirt to the outside of the chain as readily.

Video segments [(CT4)][44a], [(OZC4)][34b], [(OZC5)][35a], [(OZC5)][35b], and
[(OZC4)][34g]: summary

> Wax has low friction, sticks to metal well, repels water and dirt, and keeps
> the whole drivetrain clean and efficient, saves money, ensures clean hands,
> no "leg tatoos".

Wax vendor [(mspeedwax)][60b], [(mspeedwax)][60a]: summary

> Fastest lube, cleanest lube, easy to apply, repels dirt, saves time,
> increases drivetrain life, safe for the environment, lubricates completely,
> lasts long on the chain.

Excerpt [(CT6)][46a]: ease of use

> The wax itself can be removed with hot water, meaning any dirt stuck to the
> chain can be cleaned with hot water, too.

Excerpt [(CT)][45]: frictional losses

> In the tests where wax-based (dry) lubricants were compared to liquid (wet)
> lubricants, and given the same test conditions, the wax-based dry lubes (not
> to be confused with "dry" oil-based lubes) exhibited less increase in
> frictional losses.

Excerpt [(CT)][43]: frictional losses & drivetrain durability

> Case in point: White Lightning Epic Ride was the least-efficient lube Smith
> tested at 8.9W, and also yielded one of the shortest chain lifespans in
> Kerin’s tests, lasting less than 3,000km before hitting that critical 0.5mm
> mark.
>
> At the opposite end, a chain treated with Molten Speed Wax ate up just 4.7W
> of rider energy in Smith’s testing, while outlasting Kerin’s 6,000km-long
> wear testing procedure (and proving in the real world, with real riders, to
> go past 15,000km), something no drip lube has done as of yet.
>
> On average, one can expect 3,000 to 6,000km from drip lubes versus an
> average of 15,000km for MSW.

Excerpt [(bicyclingaustralia)][80]: drivetrain durability

> A waxed chain can bang out up to 20,000km before needing replacement,
> whereas regular lubricant will grind out 6,000km at best. That’s the cost of
> three new chains.

### 1st time prep: How to strip the factory grease off a chain?

The chain must be stripped off any grease residue -- only then will wax attach
to the metal and work as lubricant, see this nice video [(OZC2)][32b].  The
best way to render the metal sterile clean is by the _container method_, see
this [Q&A](#1st-time-prep-how-to-clean-a-chain-container-method) below.

Alternatively, one can skip this step and simply buy a _prewaxed_ chain -- no
need for cleaning chemicals, see this nice video [(CT4)][44b].  However,
prewaxed chains seem to be quite expensive, for example, [this][93] chain at
$100 (2021).  This is outside _my_ price range of $13..$20 per chain for an
internal-gear hub bike.

_Question:_ Are there vendors that offer bare metal chains without any factory
lubrication?  Product pages don't appear to say.  The vendors would save the
cost of adding grease, and customers for removing it.  Or is the reason why
chains are always greased (or waxed) to prevent corrosion during shipping and
storage?

### 1st time prep: Should I start with a new chain?

[(OZC3)][33a] suggests to first run a new chain for the first 400km on factory
grease to break it down a bit (oxidize the grease) -- if one can avoid getting
the chain too dirty.

But my experience matches that of [(mspeedwax)][60c]:

> Time saving tip: Start with a new chain, they are much easier to clean for
> waxing!

_Tip:_ Pass on _expensive_ chains for waxing [(CT6)][46e].

> Some chains with low-friction coatings seem to repel the wax, leading to
> poor longevity of the wax coating.

_Tip:_ Buy a set of 2..5 chains and clean them in one batch for waxing;
almost the same effort as cleaning just one chain, see
[Q&A](#summary-a-clean-chain---always-and-easy).

### 1st time prep: How to clean a chain? (container method)

The cleaning procedure consist of 4 steps:
1. Remove the bulk of the grease using an _aggressive, water-soluble_ solvent;
1. Remove the stubborn grease using a _petroleum-based_ solvent;
1. Remove any residues of step 2) by using a _volatile_ solvent;
1. Let any residues of step 3) evaporate or dry off.

_Tips:_ Use gloves.  Thread the chain ends on a wire and place in a bucket.
Alternatively, drop the chain into a plastic jar with an airtight lid.  Add
solvent, agitate or shake, let soak, repeat, take out, rinse, wipe clean.

_Important:_ Repeat steps 2) and 3) until the solvent stays chrystal clear.

_TLDR:_ Watch this nice ~4 minute video [(mspeedwax)][61] or see article
[(mspeedwax)][60c].

_Details:_ See pdf [(zfc)][85b] (preferred over newer version [(zfc)][85c]),
or articles [(zfc)][83], [(zfc)][84], [(zfc)][81], or video

### 1st time prep: What solvents to use? (container method)

Here's what I use...

![Cleaning for Waxing][wax1]

...but starting with this:

![Degreasing for Waxing][wax3]

As __aggressive, water-soluble solvent:__ [Cafiza powder][94] (sodium
percarbonate + tetrasodium pyrophosphate + alcohols + sodium hydroxide).

Cafiza is a fantastic solvent that is widely used to clean espresso machines
from the oily, tar-like coffee residues that build up over weeks and months.
I've found it also effective in dissolving much of the bicycle chain's factory
grease.

For cleaning a batch of 2..5 new chains, I _used_ to run 4 washing cycles of
~15 minutes:
- Thread the chains (and master link) on a wire and place them in a ~5l (qt)
  bucket,
- add 40g/30g/20g/10g of Cafiza, respectively, with ~1.5 l boiling water,
- let the chains soak, agitate them, fish them out and fold them back in,
- then thoroughly rinse them with warm water.

_Caution:_ 4 Cafiza cycles seems to corrode the chains (small spots of rust);
although, there's still grease coming out the chains.

_Tip:_ Only apply 2 Cafiza cycles and keep each under 10 minutes!

As __petroleum-based solvent:__ [Mineral spirits][20a] (petroleum spirits,
white spirit, mineral turpentine).

[(OZC2)][32f] recommends [petrol][20b] (gasoline) instead of mineral spirits
as effective and less expensive.  It leaves an oil film [(zfc)][85b], which
will be removed by the next solvent.  Use of [Paint thinner][20c] is less
effective than mineral spirits, in my experience.

Let soak for ~15 minutes -- or hours for new, stubborn factory grease.

_Tip:_ The petroleum-based solvent can be decanted, filtered, and re-used a
few times.

_Tip:_ Use 2..4 cycles of ~10 minutes: first with recycled, then with clean
spirits.

Optional __aqueous solvent:__ [Degreaser][42] (chain cleaner, all-purpose
cleaner)

Some experts keep the petroleum-based solvent phase rather short, and add this
step of a water-based degreaser [(OZC2)][32f].  The chain is soaked for ~30
minutes in concentrated degreaser and then rinsed with water.  Use a
_bio-degradable_ degreaser, such as [(PT)][50a].

_Tip:_ I just rinse the chains with warm water and a splash of dish soap.

As __volatile solvent:__ [Denatured alcohol][21a] (ethanol, methylated
spirits) and [isopropyl alcohol][21b] (rubbing alcohol).

I add a final wash of [isopropyl alcohol][21b], which evaporates even quicker
than ethanol.

_Tip:_ The alcohol can be decanted, filtered, and re-used a few times.

_Caution:_ Both ethanol and isopropyl alcohol seem to corrode the chains.

_Tip:_ Use 4 cycles of ~5 minutes each: first with recycled, then with clean
ethanol or isopropyl alcohol.

_Problem:_ Some states banned the selling of denatured alcohol. :facepalm:

### 1st time prep: Why not to use an ultrasonic cleaner?

The post [(5)][0e] discussed the _ultrasonic cleaner method_ for oil-based
lubricants.  Alas, this method alone is not sufficient for 1st time wax prep
[(mspeedwax)][60d]:

> [...] Instead, choose water based (aqueous) cleaning solutions that are non
> flammable.  When combined with heat these solutions clean extremely well
> with one exception: they aren't 100% effective in removing new, unoxidized
> grease from a new chain.  If an aqueous solution is used in this situation a
> light film of grease will remain on the chain.

Therefore, either have sonic cleaning followed with an extra ethanol bath
[(OZC2)][32e] or use the container method to begin with.

For waxed chains, their subsequent cleaning is simpler than for oil-lubricated
ones.  This also speaks against the extra costs for an ultrasonic cleaner,
such as [(mspeedwax)][62b].

### Waxing: Buy a chain wax or mix your own?

Chain wax consist of [paraffin wax][22a] (petroleum wax, candle wax) with
added lubricants, such as [PTFE][22b] (teflon), see video [(OZC4)][34a].

Examples of ready-to-use chain waxes are [(speedmaster)][70b],
[(mspeedwax)][62a], or [(hot-tub)][91].  For product comparisons (with an eye
on racing bikes), see [(CT6)][46d], [(zfc)][82], [(zfc)][85a].  Price range of
premixed chain waxes (2021): $18..20 per 1 lb.

Alternatively, it's easy to buy and mix the ingredients, see video
[(OZC4)][34c]: Melt _10 parts_ refined paraffin wax (pellets or blocks),
stir in _1 part_ PTFE powder (1.6 micron).

As of 2021, that recipe is _not_ cheaper than premixed chain wax products:
~$38 per 2 lb. [Memo: $20 of 100g PTFE powder (3.5 oz, 1.6 micron, brands
HMME, CCS) + $18 for 2 lb paraffin wax pellets or blocks (or unscented,
uncolored, paraffin candles [(OZC4)][34d]).]

Alternative recipe by [(CT6)][46d]: 500g paraffin wax, 5g PTFE, 1g MoS2
(molybdenum disulfide).

### Waxing: Use a slow cooker or hot water bath?

Finally: Waxing a chain is super easy -- with a small [slow cooker][23a]
(alternative below).

It's worth the expense: $14 for a [2 qt crock-pot][90] with low-high
temperature control (2021).  Note: [rice cookers][23b] are different and
should not be used!  At 'high', the wax takes about 30 minutes to melt; it
practically cannot overheat (some still recommend 'low', though).

After use, the solidified wax remains in the pot, and the lid keeps out dust:

![Wax Slow Cooker][wax2]

Bottom right: bottle of _drip wax_ (emulsion) to "top up" the wax in a chain,
see this [Q&A](#rewaxing-drip-wax-or-wax-bath).

Without a slow cooker, the wax can be melted in a warm oven at 90-100 Celsius;
a safer method is to use a [double boiler][23c] (water bath); see video
[(smwax)][72].  Note: Wax is flammable at higher temperatures.

### Waxing: Just submerse the chain and agitate, right?

Yes, it's that simple: submerse the chain in the molten wax, agitate it, let
soak, let drip off, break the stiff links.  Watch this nice 2 minute video by
[(CT4)][44c].

These videos and articles give a number of tips [(mspeedwax)][63],
[(OZC4)][34e], [(OZC3)][33b], [(mspeedwax)][60e], [(CT6)][46b].  __My routine
consists of: 1..3__
1. Place the chain on the hard wax already and let it melt in.
3. Soak the chain for only 1/2 hr instead of 1 hr (slow cooker at 'high').
4. Turn off cooker but leave the chain in the wax, wait for a skin to form
   (~50 Celsius); this reduces wax run off.
5. Don't wipe the chain with a paper towel after taking it out of the wax;
   this leaves a wax coating on the outside (e.g., for mountain bikes or wet
   climate).
6. Drop the hot chain into cold water to prevent the wax from running out.
7. Using a brush, push excess wax out of the space between the rollers.

### Rewaxing: How soon? (wax mileage)

A chain needs to be rewaxed if it...
- starts squeaking,
- dries up and friction can be felt under load,
- has accumulated dirt, in which case it must also be cleaned.

Every 250-300 km by estimate of [(OZC3)][33c], [(OZC4)][34f].

Every 100-400 mi by estimate of [(smwax)][74]:

> As you become familiar with your riding terrain / condition, you will soon
> know what's the approx. mileages before it needed for the next re-wax.  A
> good rule of thumb is 400+ miles on a dry flat road.  The mileage claims
> will be lessened if there are elevation, dusty road or even rain.  Off road
> would be approx. 100-200+ miles.

Every 200-300 km by recommendation of [(CT6)][46a]:

> “If you re-wax every 200 km, wear rates will remain untraceable for a long
> time,” says Kerin.  “Re-waxing every 300 km, the average for a top chain
> like YBN SLA is 15,000 km to the recommended 0.5% wear mark.  Frequently
> pushing to 400 km plus, this drops to 8000 to 10,000 km.”

### Rewaxing: What causes poor wax mileage?

If rewaxing becomes necessary much sooner than the expected ~250 km, it is a
sign that the wax didn't stick to the chain's surface.  The reasons might be:
- Residues of prior oil or grease [(osz6)][36a]: The chain must be cleaned
  again by the container method for removing factory grease.
- Wax "run off", the chain was removed when the wax was very hot
  [(smwax)][74]: Rewax and leave the chain in the wax until it has cooled
  down and starts forming a skin.

### Rewaxing: How to clean the chain?

Riding conditions make a difference how much dirt has accumulated on a waxed
chain, compare video [(smwax)][73a] (x00 mi) and [(OZC3)][33c] (300 km).

It may suffice to wipe the chain clean.  Better, though, to take the chain off
the bike, place it in a colendar or a bucket, and rinse it with boiling water.
This washes off contaminated wax on the outside but keeps a fair amount of
"good" wax inside the chain [(CT6)][46c], [(OZC2)][32g].

If the chain requires deeper cleaning, it can be boiled for a minute in water
[(smwax)][74].  Or one uses a _chain scrubber_ with hot, soapy water
[(OZC3)][33c].  Otherwise, the chain must be cleaned by the container method
for removing the factory grease.

Let the chain dry before rewaxing, which is quick if the chain is still hot
from the boiling water.  Or sling the chain around, use a hair dryer, or let
the chain hang for a while.

### Rewaxing: Drip wax or wax bath?

Occasionally, the chain just needs quick "wax top up".  For this, "drip wax"
or "bottle wax" (i.e., _liquid wax emulsion_ from a drip bottle) is perfect.

Product examples are [(smwax)][70a] or [(squirt chain lube)][92].  One can
easily mix bottle wax: melt 1 part chain wax in 7 parts [isopropyl
alcohol][21b] at ~70 Celsius, see video [(osz6)][36b].

Shake the wax emulsion if the ingredients have separated, or warm it up if the
wax has solidified.  Apply the liquid wax onto the chain, let the alcohol
evaporate, see videos [(osz6)][36c], [(smwax)][71].

The seemingly preferred method, though, is to rewax a chain in a hot wax
bath.

### Rewaxing: When to replace the wax in a pot?

The hot wax bath washes out dirt and grime in the chain, which sink to the
bottom.  Those residues can be scraped off when the solidified wax is taken
out of the pot (just heat it up a bit).

Estimates for the lifespan of the wax range from 10+ uses [(smwax)][74] to 30
uses [(CT6)][46d] until the wax becomes too contaminate.  Obviously, dirty
chains (and those treated with drip-on waxes) shorten the lifespan; see this
video how the used wax has turned grey [(OZC3)][33d].

### Summary: A clean chain - always and easy!

My experience with wax, as stated above: _The easiest, fastest, and cleanest
chain lube._

The other takeaway: Buy ~3 chains per bicycle [(CT6)][46b].

> Using multiple chains in rotation is an old trick that works for anyone,
> regardless of whether you use wax or not.  However, its application to
> immersive waxing is most obvious.  In this case, you can prep and wax your
> chains in bulk, greatly reducing the frequency and labour of the process.

### TODO [added 2024-10]:

Try out "outside the box suggestion" in [(SB0)][10a]: "My two cents on chain
lube: hot waxing with wax toilet ring seals. A soft, low temp, almost greasy
wax available at home centers. Doesn't seem to clean off with citrus cleaner,
but paint thinner works, and WD40 really works." Use a new one. -- John Allen

[drivetrain]: /img/2021-08-17-bicycle-chain-tension02.jpg
[wax1]: /img/2021-08-17-bicycle-chain-wax01.jpg
[wax2]: /img/2021-08-17-bicycle-chain-wax02.jpg
[wax3]: /img/2021-08-17-bicycle-chain-wax03.jpg

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[10a]: https://www.sheldonbrown.com/chains.html#wax

[20a]: https://en.wikipedia.org/wiki/White_spirit
[20b]: https://en.wikipedia.org/wiki/Gasoline
[20c]: https://en.wikipedia.org/wiki/Paint_thinner

[21a]: https://en.wikipedia.org/wiki/Denatured_alcohol
[21b]: https://en.wikipedia.org/wiki/Isopropyl_alcohol

[22a]: https://en.wikipedia.org/wiki/Paraffin_wax
[22b]: https://en.wikipedia.org/wiki/Polytetrafluoroethylene

[23a]: https://en.wikipedia.org/wiki/Slow_cooker
[23b]: https://en.wikipedia.org/wiki/Rice_cooker
[23c]: https://en.wikipedia.org/wiki/Bain-marie

[32b]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=296s
[32e]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=574s
[32f]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=682s
[32g]: https://www.youtube.com/watch?v=sYxzHClWfQU&t=927s

[33a]: https://www.youtube.com/watch?v=fBKOW43p7ME&t=32s
[33b]: https://www.youtube.com/watch?v=fBKOW43p7ME&t=100s
[33c]: https://www.youtube.com/watch?v=fBKOW43p7ME&t=314s
[33d]: https://www.youtube.com/watch?v=fBKOW43p7ME&t=351s

[34a]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=99s
[34b]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=140s
[34c]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=392s
[34d]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=691s
[34e]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=821s
[34f]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=1050s
[34g]: https://www.youtube.com/watch?v=HHr9znwpwmQ&t=1388s

[35a]: https://www.youtube.com/watch?v=D12BFIXZCes&t=7s
[35b]: https://www.youtube.com/watch?v=D12BFIXZCes&t=546s

[36a]: https://www.youtube.com/watch?v=-oyNX6-CCMw&t=41s
[36b]: https://www.youtube.com/watch?v=-oyNX6-CCMw&t=85s
[36c]: https://www.youtube.com/watch?v=-oyNX6-CCMw&t=506s

[43]: https://cyclingtips.com/2018/03/fast-chain-lube-that-saves-you-money/

[44a]: https://www.youtube.com/watch?v=2rcWGVLEy2Q&t=23s
[44b]: https://www.youtube.com/watch?v=2rcWGVLEy2Q&t=67s
[44c]: https://www.youtube.com/watch?v=2rcWGVLEy2Q&t=248s

[45]: https://cyclingtips.com/2020/05/how-many-watts-does-a-dirty-chain-steal/

[46a]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#basics
[46b]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#waxing
[46c]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#post_wax
[46d]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#best_wax_products
[46e]: https://cyclingtips.com/2020/08/how-to-wax-a-chain-an-endless-faq/#chain_prep

[60a]: https://moltenspeedwax.com/pages/about-us
[60b]: https://moltenspeedwax.com/pages/why-wax
[60c]: https://moltenspeedwax.com/pages/clean-your-chain
[60d]: https://moltenspeedwax.com/pages/ultrasonically-cleaning-your-chain
[60e]: https://moltenspeedwax.com/pages/waxing-your-chain

[61]: https://www.youtube.com/watch?v=lI0tZqIpguk

[62a]: https://moltenspeedwax.com/collections/molten-speed-wax
[62b]: https://moltenspeedwax.com/collections/tools-miscellaneous/products/stainless-steel-ultrasonic-cleaner

[63]: https://www.youtube.com/watch?v=98thWZIYezw

[50]: https://www.parktool.com/product/bio-chainbrite-cb-4

[70a]: https://speedmasterwax.com/products/liquid-wax-with-ptfe-bike-dry-chain-lube-from-speedmaster-wax
[70b]: https://speedmasterwax.com/products/speedmaster-wax-bicycle-chain-lube

[71]: https://www.youtube.com/watch?v=7RMr3nBt8Mk

[72]: https://www.youtube.com/watch?v=v091JGlb-rw

[73a]: https://www.youtube.com/watch?v=QLZYlRgaEvA&t=4s

[74]: https://speedmasterwax.com/pages/faq-how-to-apply-and-rewax-your-bike-chain-with-speedmaster-wax

[80]: https://www.bicyclingaustralia.com.au/news/a-fraction-too-much-friction-how-to-wax-your-chain

[81]: https://zerofrictioncycling.com.au/faq/

[82]: https://zerofrictioncycling.com.au/lubetesting/

[83]: https://zerofrictioncycling.com.au/wax-instructions/

[84]: https://zerofrictioncycling.com.au/wax-at-home/

[85a]: https://zerofrictioncycling.com.au/wp-content/uploads/2019/05/Key-Learnings-from-Lubricant-Testing-Round-1.pdf
[85b]: https://zerofrictioncycling.com.au/wp-content/uploads/2018/08/waxing-how-to-zen-master-guide-v2.pdf
[85c]: https://zerofrictioncycling.com.au/wp-content/uploads/2020/09/waxing-how-to-zen-master-guide-v4.pdf

[90]: https://www.crock-pot.com/slow-cookers/3-quart-under/

[91]: https://www.runawaybike.com/collections/frontpage/products/hot-tub

[92]: https://www.squirtcyclingproducts.com/portfolio_page/squirtchainlube/

[93]: https://wendperformance.com/buy-online/wend-factory-waxed-bike-chain

[94]: https://urnex.com/cafiza-powder
