---
title: GitLab's built-in templates for Pages are unpersuasive
date: 2021-08-04
tags: ["website", "GitLab Pages", "Static Site Generator", "HTML", "Jekyll", "Gatsby", "Hexo", "GitBook", "Hugo", "Beautiful Hugo Blog Template"]
---

The previous blogs showed the steps to set up this blog website with [GitLab
Pages][1] and the [Hugo Blog Template][14] -- also that this template required
a few fixes and seems abandoned as GitLab project.

So, how do the __other built-in templates__ for _GitLab Pages_ stack up to
_Hugo Blog_?
... <!--more-->

![builtin]

### Summary

The _Hugo Blog_ template gives the best start, despite its issues. The
other built-in website templates for _GitLab Pages_ are unconvincing.

Out-of-the-box testing: built-in templates using other [Static Site
Generators (SSG)][6]

| GitLab template: | [Plain HTML][52] | [Jekyll][51] | [Gatsby][53] | [Hexo][54] | [GitBook][55] |
|---|:---:|:---:|:---:|:---:|:---:|
| SSG | -- | [jekyll SSG][11] | [gatsbyjs SSG][56] | [hexo SSG][58] | [gitbook SSG][59] |
| Engine | -- | [Ruby][12] | [Node.js][57] | [Node.js][57] | [Node.js][57] |
| Local deploy | not tried | _gem_ issues | _npm_ issues | _npm_ issues | _npm_ issues |
| GitLab deploy | ok | ok | issues | ok | -- |
| Appearance | [placeholder page][html1] | [poor, broken][jekyll2] | [error, 404][gatsby4] | [basic page][hexo2] | -- |
| Blog support | no | yes | no | yes (CLI) | no |

Notes: I made no effort to analyze the `npm` and `gem` install issues (the
package managers for _Node.js_ and _Ruby_).  I find their complex usage (CLI,
paths), versioning mechanisms (lock), and use of additional tools (e.g.,
_bundle_, _rbenv_) not helpful in trouble-shooting problems.

See below for screenshots of the templates' deployed pages or install issues.

### GitLab template: plain html

GitLab-deployed page:

![html1]

### GitLab template: jekyll

GitLab-deployed page:

![jekyll2]

Local deploy issues:

![jekyll1]

### GitLab template: gatsby

GitLab-deployed page:

![gatsby4]

Local deploy issues:

![gatsby1]

GitLab deploy took a while:

![gatsby2]

Multitude of config files:

![gatsby3]

### GitLab template: hexo

GitLab-deployed page:

![hexo2]

Local deploy issues:

![hexo1]

### GitLab template: gitbook

[GitBook's][59] focus is not on a blog but collaborative website.  Yet, would
have liked to see the look of the GitLab template's deployed page.

No predefined CI/CD job, despite being listed as _Pages_ template:

![gitbook2]

Confirmed, no predefined _.gitlab-ci.yml_ file:

![gitbook1]

[builtin]: /img/2021-08-04-gitlab-pages-builtin-templates.jpg
[gatsby1]: /img/2021-08-04-gitlab-pages-gatsby-1317.jpg
[gatsby2]: /img/2021-08-04-gitlab-pages-gatsby-1340.jpg
[gatsby3]: /img/2021-08-04-gitlab-pages-gatsby-1342.jpg
[gatsby4]: /img/2021-08-04-gitlab-pages-gatsby-1343.jpg
[gitbook1]: /img/2021-08-04-gitlab-pages-gitbook-1428.jpg
[gitbook2]: /img/2021-08-04-gitlab-pages-gitbook-1429.jpg
[hexo1]: /img/2021-08-04-gitlab-pages-hexo-1402.jpg
[hexo2]: /img/2021-08-04-gitlab-pages-hexo-1409.jpg
[html1]: /img/2021-08-04-gitlab-pages-html-1333.jpg
[jekyll1]: /img/2021-08-04-gitlab-pages-jekyll-1727.jpg
[jekyll2]: /img/2021-08-04-gitlab-pages-jekyll-1739.jpg


[1]: https://docs.gitlab.com/ee/user/project/pages/

[6]: https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/

[11]: https://jekyllrb.com/

[12]: https://www.ruby-lang.org/en/

[14]: https://pages.gitlab.io/hugo/

[51]: https://gitlab.com/pages/jekyll

[52]: https://gitlab.com/pages/plain-html

[53]: https://gitlab.com/pages/gatsby

[54]: https://gitlab.com/pages/hexo

[55]: https://gitlab.com/pages/gitbook

[56]: https://www.gatsbyjs.com

[57]: https://nodejs.org/en/

[58]: https://hexo.io

[59]: https://www.gitbook.com

[60]: https://gitlab.com/themes-templates

[61]: https://gitlab.com/pages
