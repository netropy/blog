---
title: About bicycle chains (1)
subtitle: DIY chain service made easy
date: 2021-08-17
tags: ["DIY", "bicycle", "bicycle maintenance", "bicycle chain"]
---

"Chain Service" for my bikes used to be just this: When the chain looked too
dry, I'd put some lubrication on it; when it looked too dirty, I'd wipe it
clean with a rag...  Until after a few years, the chain started slipping, and
the local bike shop informed me of the costs for a new [drivetrain][10c]:
[chain][10d], [cassette][10e], and [chainrings][10f], plus costs for labor
:anguished:

Only later did I learn that key to a long-lasting, smooth-shifting drivetrain
is to always ride with a _clean chain._  This mini blog series details how to
do chain service, summarizing information from expert sites.

Lessons learned:
1. Chain maintenance _can_ be clean, quick, and simple.
1. The most effective and least messy way to clean a chain is to take it off
   the bike.
1. Chains with a _master link_ are taken off or mounted back on a bike in
   seconds.
1. _Chain wax_ is a great lubricant, and cleansing a waxed chain comes down to
   rinsing in hot water.
1. Best to rotate through 3..4 sized chains per bike and then cleanse and
   rewax or relubricate them in a batch.

This post starts with the _basics_ about chains: choosing the width and
connector type
... <!--more-->

Topics: \
[Resources: Chain Maintenance](#resources-chain-maintenance) \
[Bicycle chain technology](#bicycle-chain-technology) \
[Chain widths](#chain-widths) \
[Choosing a chain width for a derailleur drivetrain](#choosing-a-chain-width-for-a-derailleur-drivetrain) \
[Choosing a chain width for a non-derailleur drivetrain](#choosing-a-chain-width-for-a-non-derailleur-drivetrain) \
[Choosing the chain connector type](#choosing-the-chain-connector-type) \
[Chain choice for the Alfine IGH drivetrain](#chain-choice-for-the-alfine-igh-drivetrain)

Disclaimer: some product links with comments below (not sponsored by anyone).

### Resources: Chain Maintenance

This blog mini series sums up information from these highly recommended expert
sites:

| ref | URL|
|---|---|
| (SB)| https://www.sheldonbrown.com/chains.html |
| (OZC) | https://www.youtube.com/c/stevenleffanue/search?query=chain |
| (CT) | https://cyclingtips.com/tag/bicycle-chains/ [__update 2024:__ defunct] |
| (BG) | https://bike.bikegremlin.com/category/bicycle-drivetrain/chain/ |
| (PT) | https://www.parktool.com/blog/repair-help?query=&area%5B%5D=50 |
| (PT) | https://www.youtube.com/playlist?list=PLGCTGpvdT04RFvpef1qIJSQygRL8sv0eh |

_Notation:_ recurring links like \
\- ... [(PT1)][52a] ... [(PT1)][52b], jump to different timestamps/chapters in
the same video/document; \
\- ... [(PT)][62a] ... [(PT)][62b], may link to different resources by the
same author or website.

### Bicycle chain technology

A bit of technological background & terminology (worth knowing, but not
essential).

Bicycle chains are (beautiful) _old tech:_ they enabled the [safety
bicycle][20b] in the _1880s!_  Still, this area of technology has been seeing
innovation [(SB1)][11a]:

> An old-style bicycle chain has ten parts per link. The typical 57-link chain
> used on the average multi-speed bike had 570 parts, more than the whole rest
> of the bicycle put together...
> The major revolution in chain design has been the introduction of the
> bushingless chain... Bushingless chains have only eight parts per link. (*)

\* In many technical docs, a _link_ is defined as the smallest unit one can
remove from a chain that still leaves the chain reconnectable; hence, a link
consists of 2 connectors or _half-links_, one with outer and one with inner
plates.  Commonly, though, and henceforth in this blog series, the term _link_
is used to refer to a single connector, which makes a bicycle chain to have
114..130 links, each of them of length 1⁄2" or 12.7mm [(wiki1)][21].

The 8 parts that make up 2 half-links [(PT5)][50a]: 2 outer plates, 2 inner
plates, 2 pins, 2 rollers.

See this exploded view from [(CT2)][42] (retrieved image, defunct website):

![Exploded view of a single chain link by cyclingtips.com][chainlink]

Noticable is also the product diversification over recent decades
[(PT5)][50c]:

> Drivetrain manufacturers design their chains to work as a system with the
> derailleurs, rear sprockets, and shift levers.  Chains can vary in side
> plate shape, sizing, and height.  Differences can cause variations in
> shifting performance between brands and models.  Additionally, chains will
> vary in the quality of steel used.

### Chain widths

Chains differ by their
- _internal width:_ roller widths, distance between the inside plates;
- _external width:_ length of the rivet pins, greatest outside width.

Internal widths fall into 2 (of 4) groups, external widths into 5 (of many)
groups -- for total of 6 _relevant_ groups of chains [(wiki1)][21],
[(wiki)][20d], [(PT5)][50].  Starting with the widest:

| name | int. width | ext. width | #sprockets (cogs) |
|:---|:---:|:---:|:---|
| 1⁄8 inch or "track" chain | 1⁄8", ~3.2mm | ~9mm | [1-speed][10i], [internal-gear][10h] |
| 3⁄32 inch chain | 3⁄32", ~2.4mm | 7.0-7.3mm | 5-8, [1-speed][10i], [internal-gear][10h] |
| 9 speed chain | 3⁄32", ~2.4mm |  6.5-7.0mm | ~9 |
| 10 speed chain | 3⁄32", ~2.4mm |  5.88-7.0mm | ~10 |
| 11 speed chain | 3⁄32", ~2.4mm |  5.5-5.62mm | ~11 |
| 12 speed chain | 3⁄32", ~2.4mm |  5.3mm | ~12 |

We see that first the internal and then the external widths were reduced to
accommodate more [sprockets][10g].

__How did we get here?__

[(SB2)][13a] tells the 1st part:

> Chain Early derailer systems with only 2 or 3 rear sprockets used "1/8 inch"
> or "track" chain -- still used on some derailerless bicycles.  Later, and up
> through the the 1970s, "3/32 inch" chain allowed more, thinner sprockets.
> These chains had protruding rivet pins that snagged on the sides of sprocket
> teeth, aiding shifting.

[(CT)][45] tells the 2nd part:

> The introduction of eight-speed systems depended on an increase in rear hub
> width from 126mm to 130mm to accommodate the extra sprocket.  After that,
> the width and spacing of the sprockets was reduced to allow more sprockets
> to be added.  Chains were narrowed, too, slimming down to 5.50-5.90mm where
> once they were 7.80mm.

### Choosing a chain width for a derailleur drivetrain

_Summary:_ For a [derailleur][10j] system, select the chain by the number of
speeds ([cogs][10k]).  Optionally, pick one size narrower for lesser weight.
Optionally, pick the same manufacturer as the [group][10l] ([groupset][10m]).

__Choose a chain one size narrower?__

[(SB2)][13a] states that using one chain size narrower than standard should
not be a problem, although, shifting may not be quite as smooth; 10 or more
speeds don't last as long, and master links for them may be one-time use.

[(PT5)][50c] cautions that the chain width must also be compatible with the
front chainrings: The spacing between front rings may be too wide for a narrow
10 or 11 speed chain, it may fall between the rings during a shift.

[(OZC1)][30d] disputes that narrower widths means weaker chains, as this fails
to take modern metal hardening technology into account.

[(CT)][43] claims that choosing one size narrower may even improve shifting
and durability, as there's less contact between the chain and the edges of
nearby cogs.

__Mix chains and groups from different manufacturers?__

[(CT)][43] points out opposing statements from drivetrain manufacturers and
aftermarket chain makers, and affirms that for 8-11 speed drivetrains, mixing
should not be a problem as long as one stays within the same speed.

[(PT5)][50c] cautions that drivetrain manufacturers design their chains to
work as a system with the derailleurs, sprockets, and shift levers; chains
can vary in side plate shape, sizing, height, or the quality of steel.

### Choosing a chain width for a non-derailleur drivetrain

_Summary:_ For [internal-gear][10h] or [single-speed][10i] hub drivetrains,
match the chain with the width of the front and rear sprocket, that is either
1⁄8" or 3⁄32" (8-speed).  Optionally, use a narrower derailleur chain on a
3⁄32" sprocket for lesser weight.  If you must, run a wide 1⁄8" track chain on
a 3⁄32" sprocket.

__Run a derailleur chain on 1⁄8" sprockets?__

As _1⁄8" = 4⁄32" > 3⁄32"_, or _3.2mm > 2.4mm_ :wink:, a derailleur chain is
_1⁄32"_ or _1.2mm_ too narrow for a single-speed sprocket.

[(SB2)][13a] confirms: Only 1⁄8" chain will run on 1⁄8" sprockets.

[(PT5)][50b] points out that some freestyle bikes use a wider sprocket and a
wider 3⁄16" chain for longer chain life when "sliding down a rail or other
long fixture on the chain."

__Run a narrower derailleur chain on 3⁄32" sprockets?__

Should not be a problem as the internal width stays the same and there's no
shifting between sprockets.

[(SB2)][13a] cautions that narrower chains are usually more expensive and,
with 10 or more speeds, don't last as long.

### Choosing the chain connector type

_Summary:_ Master links are a game changer -- quick to open and close,
reliable, mostly reusable (10 speeds or less).  See post [(3)][0c] for
photos.

An important feature of chains is how they are _broken_, so that a chain can
be taken off the bike, and _assembled_ again, namely, how the chain ends are
reconnected.  Chains either use
- a [master link][20c], also branded as _connex link_, _quick link_, _power
  link_, _power lock_, _missing link_ [(SB)][10a], which is a special link
  with locking side plates, typically [un-]locked with a _chain plier_; or
- a [connecting rivet][51a], which is a special [rivet pin][10b] driven into a
  link with a _chain tool_ and then having its _pilot tip_ broken off.

See this nice video segment showing the difference between the two connector
types [(PT3)][57a].  See post [(2)][0b] for photos.

__Why don't all chain vendors switch to master links?__

This article compares master links from different manufacturers and comments
on the market's direction [(CT)][41]:

> For years KMC, YBN, SRAM and Wippermann have used such links, while Shimano
> and Campagnolo held out and stuck with special replacement pins.  However,
> even Shimano is now changing its tune and its latest 11 and 12 speed chains
> are available with master links.

This article refers to Shimano's [SM-CN900][90b] directional, non-reusable
[quick link][90c] for 11-12 speed chains.  However, Shimano once must have
offered a quick link for 6-8 speeds: [SM-UG51][91b].  Apparently, Shimano
discontinued this link (only this pdf accessible, no product information on
their website).

__Mix chains and master links from different manufacturers?__

[(CT)][41a] looked into this and confirms: For 8-9 speeds, the [KMC][94b],
[SRAM][96] or [Wippermann][95] links should work with all chains; for 10-12
speeds, there is basic compatibility with a few fine points and disclaimers.

For 1-speed (1⁄8") chains, there's a large variety of connectors from
[KMC][94b], [Wippermann][95], and other vendors.

### Chain choice for the Alfine IGH drivetrain

For our [touring bikes][upgrade1] with a [Shimano Alfine][90a]
[internal-gear hub (IGH)][12], we use an 3⁄32" (8-speed derailleur)
[SRAM PC-870][92a] master link chain. [__update 2024:__ Pleased with this
chain, good durability.]

The chain ships with this [PowerLink][93], also available as spare.  I plan to
try out the cheaper models [PC-850][92b] or [PC-830][92c] (no nickel finish)
to see how they work with _wax_, see post [(6)][0f]. [__update 2024:__ The
PC-850 chains did not seem to last as long as the PC-870; the PC-830 chains
even less so, albeit at 1⁄2 the price of the PC-870.]

![this][drivetrain]

The decision between a _1⁄8"_ and _3⁄32"_ chain was not clear for a while,
since the Shimano product documentation on the Alfine series [hub][91a],
[chainwheel][90e], and [sprocket][90d] are silent about chain or sprocket
widths.

In bicycle fora, Alfine owners reported using any of _1⁄8"_, _3⁄32"_, _9-_, or
_10-speed_ chains.  But on which Alfine/Nexus sprockets?  And what lifetime
did they get out of 9- or 10-speed chains?

A small hint came from an [Alfine][90g] product website, which lists as chain:
[CN-HG93][90f], "SHIMANO ALFINE - 9-Speed - Super Narrow - HYPERGLIDE".  Hmm,
why "hyperglide" when there's only a single rear sprocket?  Why aren't other
chains for 6-9 speeds listed?  At least, 9-speed tells us that the sprocket is
3⁄32".  So, an 8-speed chain will work well, and at longer durability. :smile:

[drivetrain]: /img/2021-08-09-bicycle-drivetrain.jpg
[chainlink]: /img/2021-08-17.bicycle-chain-exploded-view-single-link.webp.jpg

[0a]: /post/2021-08-17-bicycle-chain-maintenance1/
[0b]: /post/2021-08-18-bicycle-chain-maintenance2/
[0c]: /post/2021-08-19-bicycle-chain-maintenance3/
[0d]: /post/2021-08-20-bicycle-chain-maintenance4/
[0e]: /post/2021-08-21-bicycle-chain-maintenance5/
[0f]: /post/2021-08-22-bicycle-chain-maintenance6/

[upgrade1]: /post/2021-08-09-bicycle-upgrade1/

[10a]: https://www.sheldonbrown.com/gloss_m.html#masterlink
[10b]: https://www.sheldonbrown.com/gloss_ri-z.html#rivet
[10c]: https://www.sheldonbrown.com/gloss_dr-z.html#drivetrain
[10d]: https://www.sheldonbrown.com/gloss_ch.html#chain
[10e]: https://www.sheldonbrown.com/gloss_ch.html#chainring
[10f]: https://www.sheldonbrown.com/gloss_ca-g.html#cassette
[10g]: https://www.sheldonbrown.com/gloss_sp-ss.html#sprocket
[10h]: https://www.sheldonbrown.com/gloss_i-k.html#internal
[10i]: https://www.sheldonbrown.com/gloss_sa-o.html#singlespeed
[10j]: https://www.sheldonbrown.com/gloss_da-o.html#derailer
[10k]: https://www.sheldonbrown.com/gloss_cn-z.html#cog
[10l]: https://www.sheldonbrown.com/gloss_g.html#group
[10m]: https://www.sheldonbrown.com/gloss_g.html#groupset

[11a]: https://www.sheldonbrown.com/chains.html

[12]: https://www.sheldonbrown.com/internal-gears.html

[13a]: https://www.sheldonbrown.com/speeds.html#chains

[20b]: https://en.wikipedia.org/wiki/Safety_bicycle#History
[20c]: https://en.wikipedia.org/wiki/Master_link
[20d]: https://en.wikipedia.org/wiki/Bicycle_chain

[21]: https://en.wikibooks.org/wiki/Bicycles/Maintenance_and_Repair/Chains/Chain_sizes

[30d]: https://www.youtube.com/watch?v=CZz2gdMCp2A&t=955s

[41]: https://cyclingtips.com/2019/01/chain-quick-links-guide-to-easy-connection/
[41a]: https://cyclingtips.com/2019/01/chain-quick-links-guide-to-easy-connection/#mixing-matching

[42]: https://cyclingtips.com/2019/08/bicycle-chain-wear-and-checking-for-it/

[43]: https://cyclingtips.com/2019/12/the-best-bicycle-chain-durability-and-efficiency-tested/#8-9-10-11-12-speed

[45]: https://cyclingtips.com/2017/12/mixing-road-groupsets-what-works-together-and-what-doesnt-2/

[50]: https://www.parktool.com/blog/repair-help/chain-compatibility
[50a]: https://www.parktool.com/blog/repair-help/chain-compatibility#article-section-1
[50b]: https://www.parktool.com/blog/repair-help/chain-compatibility#article-section-2
[50c]: https://www.parktool.com/blog/repair-help/chain-compatibility#article-section-3

[51a]: https://www.parktool.com/blog/repair-help/chain-replacement-derailleur-bikes#article-section-5

[52a]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=126s
[52b]: https://www.youtube.com/watch?v=VdUQKVMPF5I&t=324s

[55a]: https://www.youtube.com/watch?v=88tDcVvS7mU&t=57s

[57a]: https://www.youtube.com/watch?v=O0YibMDWBAw&t=25s

[62a]: https://www.parktool.com/product/chain-checker-cc-2
[62b]: https://www.parktool.com/product/mini-chain-brute-chain-tool-ct-5

[90a]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/SG-S7001-8.html
[90b]: https://bike.shimano.com/en-EU/product/component/duraace-r9100/SM-CN900-11.html
[90c]: https://bike.shimano.com/en-EU/technologies/component/details/quick-link.html
[90d]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/CS-S500.html
[90e]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/FC-S501.html
[90f]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700/CN-HG93.html
[90g]: https://bike.shimano.com/en-EU/product/component/alfine-s7000s700.html

[91a]: https://si.shimano.com/#/en/DM/SG0004
[91b]: https://si.shimano.com/pdfs/si/SI-0034B-001-ENG_FRE.pdf

[92a]: https://www.sram.com/en/sram/models/cn-870-a1
[92b]: https://www.sram.com/en/sram/models/cn-850-a1
[92c]: https://www.sram.com/en/sram/models/cn-830-a1

[93]: https://www.sram.com/en/sram/models/cn-plnk-8spd-a1

[94a]: https://www.kmcchain.com/en/series/chain-connector-single-speed
[94b]: https://www.kmcchain.com/en/series/chain-connector-12s-11s-10s-9s-8s-7s-6s-speed

[95]: https://www.connexchain.com/en/product/connex-link.html

[96]: https://www.sram.com/en/search?t=powerlink
