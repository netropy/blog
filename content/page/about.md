---
title: About me
subtitle: Software Developer
comments: false
---

Scala 3 + 2.x, Java 11, C++14, C11; Rust, Go, Python, Clojure; Math, German,
English.

My favourite metric: `(Time, Value, Form)`

I'm not on social media.
