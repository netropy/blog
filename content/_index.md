## My Code Repositories

- [polyglot](https://gitlab.com/netropy/polyglot) - Languages, Code
  Experiments, Exercises, Refcards, Notes
- [fpintro](https://github.com/netropy/fpintro) - Self-directed tutorial
  (Clojure, Scala) with a high-school student
