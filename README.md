# Musings

Blog site based on the
[Beautiful Hugo Blog Template](https://gitlab.com/pages/hugo/)
for
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

---

[Musings](https://netropy.gitlab.io/blog/) © 2021 by
[Martin Zaun](https://netropy.gitlab.io/) is licensed under
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1)
